<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <source>&amp;About</source>
        <translation type="vanished">&amp;关于</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="35"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="49"/>
        <location filename="../oscar/aboutdialog.cpp" line="128"/>
        <source>Release Notes</source>
        <translation>版本说明</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="63"/>
        <source>Credits</source>
        <translation>鸣谢</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="77"/>
        <source>GPL License</source>
        <translation>GPL 许可证</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="238"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="36"/>
        <source>Show data folder</source>
        <translation>显示数据文件夹</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="87"/>
        <source>Sorry, could not locate About file.</source>
        <translation>抱歉，无法找到关于文件。</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="102"/>
        <source>Sorry, could not locate Credits file.</source>
        <translation>抱歉，无法找到鸣谢文件。</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="132"/>
        <source>Important:</source>
        <translation>重要提示：</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="146"/>
        <source>To see if the license text is available in your language, see %1.</source>
        <translation>若要查看你的语言的许可证书是否可用，请参阅 %1。</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="117"/>
        <source>Sorry, could not locate Release Notes.</source>
        <translation>抱歉，无法找到版本说明。</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="133"/>
        <source>As this is a pre-release version, it is recommended that you &lt;b&gt;back up your data folder manually&lt;/b&gt; before proceeding, because attempting to roll back later may break things.</source>
        <translation>由于这是一个预发布版本，建议您在继续之前&lt;b&gt;手动备份数据文件夹&lt;/b&gt;，因为稍后尝试回滚可能会导致错误。</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="40"/>
        <source>About OSCAR %1</source>
        <translation>关于 OSCAR %1</translation>
    </message>
</context>
<context>
    <name>CMS50F37Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="890"/>
        <source>Could not find the oximeter file:</source>
        <translation>无法找到血氧计文件：</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="896"/>
        <source>Could not open the oximeter file:</source>
        <translation>无法打开血氧计文件：</translation>
    </message>
</context>
<context>
    <name>CMS50Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="492"/>
        <source>Could not get data transmission from oximeter.</source>
        <translation>无法传输血氧计数据。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="492"/>
        <source>Please ensure you select &apos;upload&apos; from the oximeter devices menu.</source>
        <translation>请确认已在血氧计菜单中选择&apos;上传&apos;操作。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="560"/>
        <source>Could not find the oximeter file:</source>
        <translation>无法找到血氧计文件：</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="566"/>
        <source>Could not open the oximeter file:</source>
        <translation>无法打开血氧计文件：</translation>
    </message>
</context>
<context>
    <name>CheckUpdates</name>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="250"/>
        <source>Checking for newer OSCAR versions</source>
        <translation>正在检查更新版本的 OSCAR</translation>
    </message>
</context>
<context>
    <name>Daily</name>
    <message>
        <location filename="../oscar/daily.ui" line="1062"/>
        <source>B</source>
        <translation>粗体</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1074"/>
        <source>u</source>
        <translation>下划线</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1126"/>
        <source>Big</source>
        <translation>大</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1084"/>
        <source>Color</source>
        <translation>颜色</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="968"/>
        <location filename="../oscar/daily.ui" line="1411"/>
        <source>Notes</source>
        <translation>备注</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1050"/>
        <source> i </source>
        <translation>斜体</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1106"/>
        <location filename="../oscar/daily.ui" line="1116"/>
        <source>Small</source>
        <translation>小</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1022"/>
        <source>Journal</source>
        <translation>日志</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1033"/>
        <source>Position Sensor Sessions</source>
        <translation>位置传感器会话</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1383"/>
        <source>Add Bookmark</source>
        <translation>添加书签</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1419"/>
        <source>Remove Bookmark</source>
        <translation>删除书签</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2060"/>
        <source>Pick a Colour</source>
        <translation>选择一种颜色</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1786"/>
        <source>Complain to your Equipment Provider!</source>
        <translation>向设备供应商投诉！</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1004"/>
        <source>Session Information</source>
        <translation>会话信息</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1775"/>
        <source>Sessions all off!</source>
        <translation>所有会话关闭！</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="773"/>
        <source>%1 event</source>
        <translation>%1 事件</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="674"/>
        <source>Go to the most recent day with data records</source>
        <translation>跳转到最近一天的数据记录</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1184"/>
        <source>Feelings</source>
        <translation>感觉</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1187"/>
        <location filename="../oscar/daily.ui" line="1274"/>
        <source>Feelings have a range 1 - 10. 1 is the worst feeling and 10 is the Best.</source>
        <translation>感觉范围是 1 - 10。1 代表感觉最差，10 代表感觉最好。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1205"/>
        <source>Poor</source>
        <translation>差</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1346"/>
        <source>B.M.I.</source>
        <translation>体重指数</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1030"/>
        <source>Sleep Stage Sessions</source>
        <translation>睡眠阶段会话</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1248"/>
        <source>Oximeter Information</source>
        <translation>血氧计信息</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="861"/>
        <source>Events</source>
        <translation>事件</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1024"/>
        <source>CPAP Sessions</source>
        <translation>CPAP 会话</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1121"/>
        <source>Medium</source>
        <translation>中</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1406"/>
        <source>Starts</source>
        <translation>开始</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1254"/>
        <source>Weight</source>
        <translation>体重</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1362"/>
        <source>Bookmarks</source>
        <translation>书签</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="774"/>
        <source>%1 events</source>
        <translation>%1 事件</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="297"/>
        <source>events</source>
        <translation>事件</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1496"/>
        <source>Event Breakdown</source>
        <translation>事件分类</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1253"/>
        <source>SpO2 Desaturations</source>
        <translation>血氧饱和度下降</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1238"/>
        <source>Awesome</source>
        <translation>极好</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1254"/>
        <source>Pulse Change events</source>
        <translation>脉搏变化事件</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1255"/>
        <source>SpO2 Baseline Used</source>
        <translation>使用的血氧饱和度基线</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1781"/>
        <source>Zero hours??</source>
        <translation>零小时??</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="506"/>
        <source>Go to the previous day</source>
        <translation>跳转到前一天</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2397"/>
        <source>Bookmark at %1</source>
        <translation>在 %1 添加书签</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1321"/>
        <source>Statistics</source>
        <translation>统计</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="297"/>
        <source>Breakdown</source>
        <translation>分类</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1038"/>
        <source>Unknown Session</source>
        <translation>未知会话</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1699"/>
        <source>This CPAP device does NOT record detailed data</source>
        <translation>此 CPAP 设备不记录详细数据</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1777"/>
        <source>Sessions exist for this day but are switched off.</source>
        <translation>此日期存在会话，但已关闭。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1047"/>
        <source>Duration</source>
        <translation>时长</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="923"/>
        <source>View Size</source>
        <translation>查看大小</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1780"/>
        <source>Impossibly short session</source>
        <translation>不可能的短会话</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="655"/>
        <source>No %1 events are recorded this day</source>
        <translation>当天无 %1 事件记录</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="551"/>
        <source>Show or hide the calendar</source>
        <translation>显示或隐藏日历</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="614"/>
        <source>Go to the next day</source>
        <translation>跳转到下一天</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="817"/>
        <source>Session Start Times</source>
        <translation>会话开始时间</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="818"/>
        <source>Session End Times</source>
        <translation>会话结束时间</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1421"/>
        <source>Time over leak redline</source>
        <translation>漏气时间超过红线</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="309"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="310"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="570"/>
        <source>Clinical Mode</source>
        <translation>临床模式</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1411"/>
        <source>Total time in apnea</source>
        <translation>呼吸暂停总时间</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1427"/>
        <source>Total ramp time</source>
        <translation>总升压时间</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1431"/>
        <source>Time outside of ramp</source>
        <translation>升压之外的时间</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1804"/>
        <source>&quot;Nothing&apos;s here!&quot;</source>
        <translation>&quot;这里什么都没有！&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1027"/>
        <source>Oximetry Sessions</source>
        <translation>血氧监测会话</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1275"/>
        <source>Model %1 - %2</source>
        <translation>型号 %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1401"/>
        <source>This day just contains summary data, only limited information is available.</source>
        <translation>这一天仅包含摘要数据，仅有限的信息可用。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1217"/>
        <source>I&apos;m feeling ...</source>
        <translation>我感觉...</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1427"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1515"/>
        <source>Hint</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1522"/>
        <source>Graphing Help</source>
        <translation>图表帮助</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1529"/>
        <source>Layout</source>
        <translation>布局</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1536"/>
        <source>Save and Restore Graph Layout Settings</source>
        <translation>保存和恢复图表布局设置</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1549"/>
        <source>Show/hide available graphs.</source>
        <translation>显示/隐藏可用图表。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="189"/>
        <source>Details</source>
        <translation>详情</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="369"/>
        <source>Time at Pressure</source>
        <translation>压力时间</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="570"/>
        <source> Disabling Sessions requires Permissive Mode be set in OSCAR Preferences in the Clinical tab.</source>
        <translation>禁用会话需要在 OSCAR 首选项的临床选项卡中设置允许模式。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1067"/>
        <source>Click to %1 this session.</source>
        <translation>点击以 %1 这个会话。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1067"/>
        <source>disable</source>
        <translation>禁用</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1067"/>
        <source>enable</source>
        <translation>启用</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1083"/>
        <source>%1 Session #%2</source>
        <translation>%1 会话 #%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1084"/>
        <source>%1h %2m %3s</source>
        <translation>%1小时 %2分钟 %3秒</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1115"/>
        <source>Device Settings</source>
        <translation>设备设置</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1280"/>
        <source>PAP Mode: %1</source>
        <translation>PAP 模式：%1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1472"/>
        <source>Start</source>
        <translation>开始</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1472"/>
        <source>End</source>
        <translation>结束</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1509"/>
        <source>Unable to display Pie Chart on this system</source>
        <translation>无法在此系统上显示饼图</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1807"/>
        <source>No data is available for this day.</source>
        <translation>当天无可用数据。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2483"/>
        <source>No Value Selected</source>
        <translation>未选择值</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2485"/>
        <source>Value</source>
        <translation>值</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1257"/>
        <location filename="../oscar/daily.ui" line="1264"/>
        <source>If height is greater than zero in Preferences Dialog, setting weight here will show Body Mass Index (BMI) value</source>
        <translation>如果在首选项对话框中身高大于零，在此处设置体重将显示身体质量指数 (BMI) 值</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1118"/>
        <source>&lt;b&gt;Please Note:&lt;/b&gt; All settings shown below are based on assumptions that nothing has changed since previous days.</source>
        <translation>&lt;b&gt;请注意：&lt;/b&gt; 以下显示的所有设置均基于假设即自前几天以来未发生任何变化。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1784"/>
        <source>no data :(</source>
        <translation>无数据 :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1785"/>
        <source>Sorry, this device only provides compliance data.</source>
        <translation>抱歉，此设备仅提供依从性数据。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2354"/>
        <source>This bookmark is in a currently disabled area.</source>
        <translation>此书签位于当前禁用的区域中。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1284"/>
        <source>(Mode and Pressure settings missing; yesterday&apos;s shown.)</source>
        <translation>（模式和压力设置缺失；显示昨天的设置。）</translation>
    </message>
    <message>
        <location filename="../oscar/daily.h" line="148"/>
        <source>Hide All Events</source>
        <translation>隐藏所有事件</translation>
    </message>
    <message>
        <location filename="../oscar/daily.h" line="149"/>
        <source>Show All Events</source>
        <translation>显示所有事件</translation>
    </message>
    <message>
        <location filename="../oscar/daily.h" line="150"/>
        <source>Hide All Graphs</source>
        <translation>隐藏所有图表</translation>
    </message>
    <message>
        <location filename="../oscar/daily.h" line="151"/>
        <source>Show All Graphs</source>
        <translation>显示所有图表</translation>
    </message>
</context>
<context>
    <name>DailySearchTab</name>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="273"/>
        <source>Clear</source>
        <translation>清除</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1198"/>
        <location filename="../oscar/dailySearchTab.cpp" line="1203"/>
        <source>Bookmark
Jumps to Date&apos;s Bookmark</source>
        <translation>书签
跳转到日期的书签</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1445"/>
        <location filename="../oscar/dailySearchTab.cpp" line="1652"/>
        <source>Start Search</source>
        <translation>开始搜索</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="327"/>
        <source>DATE
Jumps to Date</source>
        <translation>日期
跳转到日期</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="272"/>
        <source>Match</source>
        <translation>匹配</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="496"/>
        <source>Notes</source>
        <translation>备注</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="497"/>
        <source>Notes containing</source>
        <translation>包含备注</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="498"/>
        <source>Bookmarks</source>
        <translation>书签</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="499"/>
        <source>Bookmarks containing</source>
        <translation>包含书签</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="500"/>
        <source>AHI </source>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="501"/>
        <source>Daily Duration</source>
        <translation>每日时长</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="502"/>
        <source>Session Duration</source>
        <translation>会话时长</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="503"/>
        <source>Days Skipped</source>
        <translation>跳过的天数</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="504"/>
        <source>Apnea Length</source>
        <translation>呼吸暂停时长</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="507"/>
        <source>Disabled Sessions</source>
        <translation>禁用的会话</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="509"/>
        <source>Number of Sessions</source>
        <translation>会话数</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1334"/>
        <source>Click HERE to close Help</source>
        <translation>点击此处关闭帮助</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1339"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1187"/>
        <source>Number Disabled Session
Jumps to Date&apos;s Details </source>
        <translation>禁用会话次数
跳转到日期详情</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1193"/>
        <location filename="../oscar/dailySearchTab.cpp" line="1209"/>
        <source>Note
Jumps to Date&apos;s Notes</source>
        <translation>备注
跳转到日期备注</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1215"/>
        <source>AHI
Jumps to Date&apos;s Details</source>
        <translation>AHI
跳转到日期详情</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1218"/>
        <source> EventsPerHour</source>
        <translation> 每小时事件数</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1228"/>
        <source>Set of Apnea:Length
Jumps to Date&apos;s Events</source>
        <translation>呼吸暂停:长度
跳转到日期事件</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1234"/>
        <location filename="../oscar/dailySearchTab.cpp" line="1385"/>
        <source> Seconds</source>
        <translation> 秒</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1237"/>
        <source>Session Duration
Jumps to Date&apos;s Details</source>
        <translation>会话时长
跳转到日期详情</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1241"/>
        <location filename="../oscar/dailySearchTab.cpp" line="1390"/>
        <source> Minutes</source>
        <translation> 分钟</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1246"/>
        <source>Number of Sessions
Jumps to Date&apos;s Details</source>
        <translation>会话次数
跳转到日期详情</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1251"/>
        <source> Sessions</source>
        <translation> 会话</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1254"/>
        <source>Daily Duration
Jumps to Date&apos;s Details</source>
        <translation>每日时长
跳转到日期详情</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1260"/>
        <location filename="../oscar/dailySearchTab.cpp" line="1380"/>
        <source> Hours</source>
        <translation> 小时</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1264"/>
        <source>Number of events
Jumps to Date&apos;s Events</source>
        <translation>事件次数
跳转到日期事件</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1268"/>
        <source> Events</source>
        <translation> 事件</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="578"/>
        <source>Continue Search</source>
        <translation>继续搜索</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="522"/>
        <source>All Apnea</source>
        <translation>所有呼吸暂停</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="586"/>
        <source>End of Search</source>
        <translation>搜索结束</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="594"/>
        <source>No Matches</source>
        <translation>无匹配项</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1276"/>
        <source>add another match?</source>
        <translation>添加另一个匹配项？</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1580"/>
        <source> Skip:%1</source>
        <translation> 跳过:%1</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1581"/>
        <source>%1/%2%3 days</source>
        <translation>%1/%2%3 天</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1584"/>
        <source>Found %1 </source>
        <translation>找到 %1 </translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1602"/>
        <source>File errors:%1</source>
        <translation>文件错误：%1</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1691"/>
        <source>Finds days that match specified criteria.</source>
        <translation>查找符合指定条件的日期。</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1692"/>
        <source>  Searches from last day to first day.</source>
        <translation>  从最后一天到第一天进行搜索。</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1693"/>
        <source>  Skips Days with no graphing data.</source>
        <translation>  跳过没有图表数据的日期。</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1695"/>
        <source>First click on Match Button then select topic.</source>
        <translation>首先点击“匹配”按钮，然后选择主题。</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1696"/>
        <source>  Then click on the operation to modify it.</source>
        <translation>  然后点击操作以修改它。</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1697"/>
        <source>  or update the value</source>
        <translation>  或更新值</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1699"/>
        <source>Topics without operations will automatically start.</source>
        <translation>没有操作的主题将自动开始。</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1701"/>
        <source>Compare Operations: numeric or character. </source>
        <translation>比较操作：数字或字符。 </translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1702"/>
        <source>Numeric Operations: </source>
        <translation>数字操作： </translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1703"/>
        <source>  Character Operations: </source>
        <translation>  字符操作： </translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1705"/>
        <source>Summary Line</source>
        <translation>摘要栏</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1706"/>
        <source>  Left:Summary - Number of Day searched</source>
        <translation>  左：摘要 - 搜索的日期数</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1707"/>
        <source>  Center:Number of Items Found</source>
        <translation>  中：找到的项目数</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1708"/>
        <source>  Right:Minimum/Maximum for item searched</source>
        <translation>  右：搜索项目的最小/最大值</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1710"/>
        <source>Result Table</source>
        <translation>结果表</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1711"/>
        <source>  Column One: Date of match. Click selects date.</source>
        <translation>  第一列：匹配日期。点击选择日期。</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1712"/>
        <source>  Column two: Information. Click selects date.</source>
        <translation>  第二列：信息。点击选择日期。</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1713"/>
        <source>    Then Jumps the appropriate tab.</source>
        <translation>    然后跳转到相应的选项卡。</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1715"/>
        <source>Wildcard Pattern Matching:</source>
        <translation>通配符模式匹配：</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1716"/>
        <source>  Wildcards use 3 characters:</source>
        <translation>  通配符使用3个字符：</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1717"/>
        <source>  Asterisk</source>
        <translation>  星号</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1718"/>
        <source>  Question Mark</source>
        <translation>  问号</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1719"/>
        <source>  Backslash.</source>
        <translation>  反斜杠。</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1720"/>
        <source>  Asterisk matches any number of characters.</source>
        <translation>  星号匹配任意数量的字符。</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1721"/>
        <source>  Question Mark matches a single character.</source>
        <translation>  问号匹配单个字符。</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1722"/>
        <source>  Backslash matches next character.</source>
        <translation>  反斜杠匹配下一个字符。</translation>
    </message>
</context>
<context>
    <name>DateErrorDisplay</name>
    <message>
        <location filename="../oscar/overview.cpp" line="814"/>
        <source>ERROR
The start date MUST be before the end date</source>
        <translation>错误
开始日期必须早于结束日期</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="817"/>
        <source>The entered start date %1 is after the end date %2</source>
        <translation>输入的开始日期 %1 晚于结束日期 %2</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="818"/>
        <source>
Hint: Change the end date first</source>
        <translation>
提示：请先更改结束日期</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="820"/>
        <source>The entered end date %1 </source>
        <translation>输入的结束日期 %1</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="821"/>
        <source>is before the start date %1</source>
        <translation>早于开始日期 %1</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="822"/>
        <source>
Hint: Change the start date first</source>
        <translation>
提示：请先更改开始日期</translation>
    </message>
</context>
<context>
    <name>ExportCSV</name>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>AHI</source>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>End</source>
        <translation>结束</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="154"/>
        <source>End:</source>
        <translation>结束：</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="192"/>
        <source>Quick Range:</source>
        <translation>快速范围：</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="60"/>
        <source>Daily</source>
        <translation>每日</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Event</source>
        <translation>事件</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Start</source>
        <translation>开始</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="210"/>
        <location filename="../oscar/exportcsv.cpp" line="129"/>
        <source>Last Fortnight</source>
        <translation>过去两周</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="61"/>
        <location filename="../oscar/exportcsv.cpp" line="123"/>
        <source>Most Recent Day</source>
        <translation>最近一天</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="214"/>
        <source> Count</source>
        <translation> 计数</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="85"/>
        <source>Filename:</source>
        <translation>文件名：</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="88"/>
        <source>Select file to export to</source>
        <translation>选择要导出的文件</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="37"/>
        <source>Resolution:</source>
        <translation>分辨率：</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="107"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="24"/>
        <source>Dates:</source>
        <translation>日期：</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="235"/>
        <location filename="../oscar/exportcsv.cpp" line="109"/>
        <source>Custom</source>
        <translation>自定义</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="114"/>
        <source>Export</source>
        <translation>导出</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="131"/>
        <source>Start:</source>
        <translation>开始：</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Data/Duration</source>
        <translation>数据/时长</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="89"/>
        <source>CSV Files (*.csv)</source>
        <translation>CSV 文件 (*.csv)</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="215"/>
        <location filename="../oscar/exportcsv.cpp" line="132"/>
        <source>Last Month</source>
        <translation>上个月</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="220"/>
        <location filename="../oscar/exportcsv.cpp" line="135"/>
        <source>Last 6 Months</source>
        <translation>过去六个月</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>Total Time</source>
        <translation>总时间</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>DateTime</source>
        <translation>日期时间</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <source>Session Count</source>
        <translation>会话次数</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Session</source>
        <translation>会话</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="230"/>
        <location filename="../oscar/exportcsv.cpp" line="120"/>
        <source>Everything</source>
        <translation>全部</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="205"/>
        <location filename="../oscar/exportcsv.cpp" line="126"/>
        <source>Last Week</source>
        <translation>上周</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="225"/>
        <location filename="../oscar/exportcsv.cpp" line="138"/>
        <source>Last Year</source>
        <translation>去年</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="14"/>
        <source>Export as CSV</source>
        <translation>导出为 CSV</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="79"/>
        <source>Sessions_</source>
        <translation>会话_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="46"/>
        <source>Details</source>
        <translation>详情</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="81"/>
        <source>Summary_</source>
        <translation>摘要_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="77"/>
        <source>Details_</source>
        <translation>详情_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="53"/>
        <source>Sessions</source>
        <translation>会话</translation>
    </message>
</context>
<context>
    <name>FPIconLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="242"/>
        <source>Import Error</source>
        <translation>导入错误</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="243"/>
        <source>This device Record cannot be imported in this profile.</source>
        <translation>无法将此设备记录导入此配置文件。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="243"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>当天记录与现有内容重叠。</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../oscar/help.ui" line="196"/>
        <source>Search Topic:</source>
        <translation>搜索主题：</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="111"/>
        <source>Contents</source>
        <translation>目录</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="112"/>
        <source>Index</source>
        <translation>索引</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="116"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="91"/>
        <source>Hide this message</source>
        <translation>隐藏此消息</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="59"/>
        <source>Help Files are not yet available for %1 and will display in %2.</source>
        <translation>帮助文件尚不可用于 %1，并将在 %2 中显示。</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="85"/>
        <source>HelpEngine did not set up correctly</source>
        <translation>帮助引擎未正确设置</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="100"/>
        <source>HelpEngine could not register documentation correctly.</source>
        <translation>帮助引擎无法正确注册文档。</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="240"/>
        <source>No</source>
        <translation>没有</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="241"/>
        <source>%1 result(s) for &quot;%2&quot;</source>
        <translation>%1 个&quot;%2&quot;的结果 </translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="242"/>
        <source>clear</source>
        <translation>清除</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="69"/>
        <source>Help files do not appear to be present.</source>
        <translation>帮助文件似乎不存在。</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="126"/>
        <source>No documentation available</source>
        <translation>无可用文档</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="214"/>
        <source>Please wait a bit.. Indexing still in progress</source>
        <translation>请稍等...索引仍在进行中</translation>
    </message>
</context>
<context>
    <name>MD300W1Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="168"/>
        <source>Could not find the oximeter file:</source>
        <translation>无法找到血氧计文件：</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="174"/>
        <source>Could not open the oximeter file:</source>
        <translation>无法打开血氧计文件：</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1247"/>
        <location filename="../oscar/mainwindow.ui" line="2450"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <source>&amp;Data</source>
        <translation type="vanished">&amp;数据</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="vanished">&amp;文档</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="vanished">&amp;帮助</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation type="vanished">&amp;查看</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="119"/>
        <source>E&amp;xit</source>
        <translation>&amp;退出</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1059"/>
        <source>Daily</source>
        <translation>每日</translation>
    </message>
    <message>
        <source>Import &amp;ZEO Data</source>
        <translation type="vanished">导入 &amp;ZEO 数据</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2231"/>
        <source>MSeries Import complete</source>
        <translation>M 系列导入完成</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1460"/>
        <source>There was an error saving screenshot to file &quot;%1&quot;</source>
        <translation>保存截图到文件 &quot;%1&quot; 时出错</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1140"/>
        <source>Importing Data</source>
        <translation>正在导入数据</translation>
    </message>
    <message>
        <source>Online Users &amp;Guide</source>
        <translation type="vanished">在线用户&amp;指南</translation>
    </message>
    <message>
        <source>View &amp;Welcome</source>
        <translation type="vanished">查看&amp;欢迎</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2227"/>
        <source>There was a problem opening MSeries block File: </source>
        <translation>打开 M 系列文件时出错： </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="116"/>
        <source>&amp;About</source>
        <translation>&amp;关于</translation>
    </message>
    <message>
        <source>View &amp;Daily</source>
        <translation type="vanished">查看&amp;每日</translation>
    </message>
    <message>
        <source>View &amp;Overview</source>
        <translation type="vanished">查看&amp;概览</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1336"/>
        <source>Access to Preferences has been blocked until recalculation completes.</source>
        <translation>在重新计算完成之前，访问首选项已被阻止。</translation>
    </message>
    <message>
        <source>Import RemStar &amp;MSeries Data</source>
        <translation type="vanished">导入 RemStar &amp;M 系列数据</translation>
    </message>
    <message>
        <source>Change &amp;User</source>
        <translation type="vanished">更改 &amp;用户</translation>
    </message>
    <message>
        <source>View S&amp;tatistics</source>
        <translation type="vanished">查看 &amp;统计</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="584"/>
        <source>Monthly</source>
        <translation>每月</translation>
    </message>
    <message>
        <source>Change &amp;Language</source>
        <translation type="vanished">更改 &amp;语言</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1197"/>
        <source>Import</source>
        <translation>导入</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="781"/>
        <location filename="../oscar/mainwindow.cpp" line="1859"/>
        <source>Please wait, importing from backup folder(s)...</source>
        <translation>请稍等，正在从备份文件夹导入...</translation>
    </message>
    <message>
        <source>Right &amp;Sidebar</source>
        <translation type="vanished">右侧 &amp;侧边栏</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2715"/>
        <location filename="../oscar/mainwindow.ui" line="2718"/>
        <source>View Statistics</source>
        <translation>查看统计信息</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1024"/>
        <source>CPAP Data Located</source>
        <translation>已找到 CPAP 数据</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="972"/>
        <source>Access to Import has been blocked while recalculations are in progress.</source>
        <translation>重新计算过程中，访问导入已被阻止。</translation>
    </message>
    <message>
        <source>Sleep Disorder Terms &amp;Glossary</source>
        <translation type="vanished">睡眠障碍术语 &amp;词汇表</translation>
    </message>
    <message>
        <source>Use &amp;AntiAliasing</source>
        <translation type="vanished">使用 &amp;抗锯齿</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="561"/>
        <source>Report Mode</source>
        <translation>报告模式</translation>
    </message>
    <message>
        <source>&amp;Profiles</source>
        <translation type="vanished">&amp;配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="571"/>
        <source>Standard</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="507"/>
        <location filename="../oscar/mainwindow.ui" line="1009"/>
        <source>Statistics</source>
        <translation>统计</translation>
    </message>
    <message>
        <source>&amp;Statistics</source>
        <translation type="vanished">&amp;统计</translation>
    </message>
    <message>
        <source>&amp;Advanced</source>
        <translation type="vanished">&amp;高级</translation>
    </message>
    <message>
        <source>Print &amp;Report</source>
        <translation type="vanished">打印 &amp;报告</translation>
    </message>
    <message>
        <source>Take &amp;Screenshot</source>
        <translation type="vanished">截取 &amp;屏幕截图</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1103"/>
        <source>Overview</source>
        <translation>概览</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2636"/>
        <source>Show Debug Pane</source>
        <translation>显示调试面板</translation>
    </message>
    <message>
        <source>&amp;Edit Profile</source>
        <translation type="vanished">&amp;编辑配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1059"/>
        <source>Import Reminder</source>
        <translation>导入提示</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="536"/>
        <location filename="../oscar/mainwindow.cpp" line="2183"/>
        <source>Welcome</source>
        <translation>欢迎使用</translation>
    </message>
    <message>
        <source>Import &amp;Somnopose Data</source>
        <translation type="vanished">导入&amp;睡眠姿势数据</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1462"/>
        <source>Screenshot saved to file &quot;%1&quot;</source>
        <translation>截屏已保存到文件 &quot;%1&quot;</translation>
    </message>
    <message>
        <source>&amp;Preferences</source>
        <translation type="vanished">&amp;首选项</translation>
    </message>
    <message>
        <source>&amp;Frequently Asked Questions</source>
        <translation type="vanished">&amp;常见问题</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1147"/>
        <location filename="../oscar/mainwindow.ui" line="2937"/>
        <source>Oximetry</source>
        <translation>血氧测定</translation>
    </message>
    <message>
        <source>Change &amp;Data Folder</source>
        <translation type="vanished">更改&amp;数据文件夹</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="594"/>
        <source>Date Range</source>
        <translation>日期范围</translation>
    </message>
    <message>
        <source>O&amp;ximetry Wizard</source>
        <translation type="vanished">&amp;血氧仪安装向导</translation>
    </message>
    <message>
        <source>Purge &amp;Current Selected Day</source>
        <translation type="vanished">清除&amp;当前所选日期的数据</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2769"/>
        <source>Current Days</source>
        <translation>当前天数</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="717"/>
        <source>Import Problem</source>
        <translation>导入问题</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="717"/>
        <source>Couldn&apos;t find any valid Device Data at

%1</source>
        <translation>在 %1 中找不到任何有效设备数据。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="889"/>
        <source>Choose a folder</source>
        <translation>选择一个文件夹</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1119"/>
        <source>No supported data was found</source>
        <translation>未找到支持的数据</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1834"/>
        <source>Are you sure you want to rebuild all CPAP data for the following device:

</source>
        <translation>确定要重建以下设备的所有CPAP数据吗？

 </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1844"/>
        <source>For some reason, OSCAR does not have any backups for the following device:</source>
        <translation>由于某种原因，OSCAR没有以下设备的任何备份：</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1863"/>
        <source>Would you like to import from your own backups now? (you will have no data visible for this device until you do)</source>
        <translation>您现在要从您自己的备份中导入吗？（您将看不到该设备的任何数据，直到您完成导入）</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1914"/>
        <source>OSCAR does not have any backups for this device!</source>
        <translation>OSCAR没有此设备的任何备份！</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1915"/>
        <source>Unless you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your data for this device&lt;/i&gt;, &lt;font size=+2&gt;you will lose this device&apos;s data &lt;b&gt;permanently&lt;/b&gt;!&lt;/font&gt;</source>
        <translation>除非您已为该设备的所有数据创建了&lt;i&gt;您&lt;b&gt;自己&lt;/b&gt;的备份&lt;/i&gt;，&lt;font size=+2&gt;否则您将&lt;b&gt;永久&lt;/b&gt;丢失该设备的数据！&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1921"/>
        <source>You are about to &lt;font size=+2&gt;obliterate&lt;/font&gt; OSCAR&apos;s device database for the following device:&lt;/p&gt;</source>
        <translation>您将要&lt;font size=+2&gt;删除&lt;/font&gt;OSCAR的以下设备的数据库：&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1979"/>
        <source>A file permission error caused the purge process to fail; you will have to delete the following folder manually:</source>
        <translation>文件权限错误导致清除过程失败；您需要手动删除以下文件夹：</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2526"/>
        <source>Are you sure you want to delete oximetry data for %1</source>
        <translation>确定清除%1内的血氧仪数据吗</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2528"/>
        <source>&lt;b&gt;Please be aware you can not undo this operation!&lt;/b&gt;</source>
        <translation>&lt;b&gt;请注意，您无法撤消此操作！&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2558"/>
        <source>Select the day with valid oximetry data in daily view first.</source>
        <translation>请先在每日视图中选择有效血氧仪数据的日期。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2516"/>
        <source>Rebuild CPAP Data</source>
        <translation>重建CPAP数据</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2557"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <source>&amp;Automatic Oximetry Cleanup</source>
        <translation type="vanished">&amp;血氧仪数据自动清理</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="882"/>
        <source>Please insert your CPAP data card...</source>
        <translation>请插入CPAP数据卡...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1846"/>
        <source>Provided you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your CPAP data&lt;/i&gt;, you can still complete this operation, but you will have to restore from your backups manually.</source>
        <translation>如果已经为所有CPAP数据进行了&lt;i&gt;您&lt;b&gt;自己&lt;/b&gt;的备份&lt;/i&gt;，仍然可以完成此操作，但必须手动从备份中还原。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1847"/>
        <source>Are you really sure you want to do this?</source>
        <translation>确定进行此操作?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1862"/>
        <source>Because there are no internal backups to rebuild from, you will have to restore from your own.</source>
        <translation>由于没有可用的内部备份可供重建使用，请自行从备份中还原。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1017"/>
        <source>A %1 file structure for a %2 was located at:</source>
        <translation>在以下位置找到一个用于 %2 的 %1 文件结构：</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1019"/>
        <source>A %1 file structure was located at:</source>
        <translation>%1 文件结构的位置在：</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="984"/>
        <source>Would you like to import from this location?</source>
        <translation>从这里导入吗?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1028"/>
        <source>Specify</source>
        <translation>指定</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="938"/>
        <source>Navigation</source>
        <translation>导航</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1369"/>
        <source>Bookmarks</source>
        <translation>书签</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2337"/>
        <source>Records</source>
        <translation>记录</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2396"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2400"/>
        <source>Export Data</source>
        <translation>导出数据</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2421"/>
        <source>View</source>
        <translation>视图</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2425"/>
        <source>Reset Graphs</source>
        <translation>重设图表</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2480"/>
        <source>Data</source>
        <translation>数据</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2484"/>
        <source>Advanced</source>
        <translation>高级</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2494"/>
        <source>Purge ALL Device Data</source>
        <translation>清除所有设备数据</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2539"/>
        <source>Import CPAP Card Data</source>
<translation>导入CPAP卡数据</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2547"/>
        <source>Preferences</source>
        <translation>首选项</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2562"/>
        <source>View Daily</source>
        <translation>查看日常</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2573"/>
        <source>View Overview</source>
        <translation>查看总览</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2584"/>
        <source>View Welcome</source>
        <translation>查看欢迎</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2609"/>
        <source>Use AntiAliasing</source>
        <translation>使用抗锯齿</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2614"/>
        <source>About OSCAR</source>
        <translation>关于OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2622"/>
        <source>Maximize Toggle</source>
<translation>切换最大化</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2644"/>
        <source>Reset Graph Heights</source>
<translation>重置图表高度</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2652"/>
        <source>Take Screenshot</source>
<translation>截屏</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2660"/>
        <source>Oximetry Wizard</source>
<translation>血氧测量向导</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2668"/>
        <source>Print Report</source>
<translation>打印报告</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2673"/>
        <source>Edit Profile</source>
<translation>编辑个人资料</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2678"/>
        <source>Online Users Guide</source>
<translation>在线用户指南</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2683"/>
        <source>Frequently Asked Questions</source>
<translation>常见问题</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2688"/>
        <source>Automatic Oximetry Cleanup</source>
<translation>血氧仪数据自动清理</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2693"/>
        <source>Change User</source>
<translation>更改用户</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2704"/>
        <source>Right Sidebar</source>
<translation>右侧边栏</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2729"/>
        <source>Import ZEO Data</source>
<translation>导入ZEO数据</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2734"/>
        <source>Import Dreem Data</source>
<translation>导入Dreem数据</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2739"/>
        <source>Import RemStar MSeries Data</source>
<translation>导入RemStar MSeries数据</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2744"/>
        <source>Sleep Disorder Terms Glossary</source>
<translation>睡眠障碍术语词汇表</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2749"/>
        <source>Change Language</source>
<translation>更改语言</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2754"/>
        <source>Change Data Folder</source>
<translation>更改数据文件夹</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2759"/>
        <source>Import Somnopose Data</source>
<translation>导入Somnopose数据</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2764"/>
        <source>Import Viatom/Wellue Data</source>
<translation>导入Viatom/Wellue数据</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2777"/>
        <source>Show Line Cursor</source>
<translation>显示线条光标</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2796"/>
        <source>Daily Sidebar</source>
        <translation>每日侧边栏</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2813"/>
        <source>Daily Calendar</source>
        <translation>日历</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2878"/>
        <source>Show Pie Chart</source>
<translation>显示饼图</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2919"/>
        <source>Check For Updates</source>
<translation>检查更新</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2932"/>
        <source>CPAP</source>
<translation>持续气道正压通气</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2942"/>
        <source>Sleep Stage</source>
<translation>睡眠阶段</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2947"/>
        <source>Position</source>
<translation>位置</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2952"/>
        <source>All except Notes</source>
<translation>除备注外的所有内容</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2957"/>
        <source>All including Notes</source>
<translation>包括备注的所有内容</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="713"/>
        <source>Imported %1 CPAP session(s) from

            %2
        </source>
<translation>从 %2 导入 %1 个CPAP会话</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="713"/>
        <source>Import Success</source>
        <translation>导入成功</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="715"/>
        <source>Already up to date with CPAP data at

%1</source>
        <translation>已更新CPAP数据位于

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="715"/>
        <source>Up to date</source>
        <translation>最新</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1912"/>
        <source>Note as a precaution, the backup folder will be left in place.</source>
        <translation>请注意，备份文件夹将保留在原位。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1924"/>
        <source>Are you &lt;b&gt;absolutely sure&lt;/b&gt; you want to proceed?</source>
        <translation>您 &lt;b&gt;确定&lt;/b&gt; 要继续吗？</translation>
    </message>
    <message>
        <source>Exp&amp;ort Data</source>
        <translation type="vanished">导&amp;出数据</translation>
    </message>
    <message>
        <source>Backup &amp;Journal</source>
        <translation type="vanished">备份&amp;日志</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2618"/>
        <source>%1&apos;s Journal</source>
        <translation>%1&apos;的日志</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2620"/>
        <source>Choose where to save journal</source>
        <translation>选择存储日志的位置</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2620"/>
        <source>XML Files (*.xml)</source>
        <translation>XML 文件 (*.xml)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2832"/>
        <source>Show Performance Information</source>
        <translation>显示性能信息</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2852"/>
        <source>CSV Export Wizard</source>
    <translation>CSV 导出向导</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2857"/>
        <source>Export for Review</source>
    <translation>导出以供查看</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="976"/>
        <source>Import is already running in the background.</source>
    <translation>导入操作已在后台运行。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="965"/>
        <location filename="../oscar/mainwindow.ui" line="2552"/>
        <source>Profiles</source>
        <translation>配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2488"/>
        <source>Purge Oximetry Data</source>
        <translation>清除血氧数据</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="257"/>
        <source>Help Browser</source>
        <translation>帮助浏览器</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="520"/>
        <source>Loading profile &quot;%1&quot;</source>
        <translation>正在加载配置文件 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1331"/>
        <source>Please open a profile first.</source>
        <translation>请先打开一个配置文件。</translation>
    </message>
    <message>
        <source>&amp;About OSCAR</source>
        <translation type="vanished">&amp;关于OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2862"/>
        <source>Report an Issue</source>
        <translation>报告问题</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1529"/>
        <source>The FAQ is not yet implemented</source>
        <translation>常见问题功能尚未实现</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1671"/>
        <location filename="../oscar/mainwindow.cpp" line="1698"/>
        <source>If you can read this, the restart command didn&apos;t work. You will have to do it yourself manually.</source>
        <translation>如果您看到此信息，说明重启命令未生效。您需要手动重启。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2029"/>
        <source>No help is available.</source>
        <translation>没有可用的帮助信息。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2640"/>
        <source>Export review is not yet implemented</source>
        <translation>导出查看功能尚未实现</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2849"/>
        <source>Reporting issues is not yet implemented</source>
        <translation>报告问题功能尚未实现</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1837"/>
        <source>Please note, that this could result in loss of data if OSCAR&apos;s backups have been disabled.</source>
        <translation>请注意，如果禁用了 OSCAR 的备份功能，可能会导致数据丢失。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="968"/>
        <source>No profile has been selected for Import.</source>
        <translation>未选择要导入的配置文件。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1522"/>
        <source>The User&apos;s Guide will open in your default browser</source>
        <translation>用户指南将会在默认浏览器中打开</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2240"/>
        <source>The Glossary will open in your default browser</source>
        <translation>词汇表将会在默认浏览器中打开</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2565"/>
        <source>Show Daily view</source>
        <translation>显示每日视图</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2576"/>
        <source>Show Overview view</source>
        <translation>显示概览视图</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2625"/>
        <source>Maximize window</source>
        <translation>最大化窗口</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2647"/>
        <source>Reset sizes of graphs</source>
        <translation>重置图表大小</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2707"/>
        <source>Show Right Sidebar</source>
        <translation>显示右侧边栏</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2721"/>
        <source>Show Statistics view</source>
        <translation>显示统计视图</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2799"/>
        <source>Show Daily Left Sidebar</source>
        <translation>显示每日左侧边栏</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2816"/>
        <source>Show Daily Calendar</source>
        <translation>显示每日日历</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2824"/>
        <source>Backup Journal</source>
        <translation>备份日志</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2867"/>
        <source>System Information</source>
        <translation>系统信息</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2881"/>
        <source>Show Pie Chart on Daily page</source>
<translation>在每日页面上显示饼图</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1119"/>
        <location filename="../oscar/mainwindow.cpp" line="2858"/>
        <source>OSCAR Information</source>
        <translation>OSCAR 信息</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2454"/>
        <source>Troubleshooting</source>
        <translation>故障排除</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="568"/>
        <source>Show Standard Report</source>
        <translation>显示标准报告</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="581"/>
        <source>Show Monthly Report</source>
        <translation>显示月度报告</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="591"/>
        <source>Show Range Report</source>
        <translation>显示范围报告</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="622"/>
        <source>Select Report Date</source>
        <translation>选择报告日期</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="625"/>
        <source>Report Date</source>
        <translation>报告日期</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2837"/>
        <source>Create zip of CPAP data card</source>
        <translation>创建 CPAP 数据卡的压缩文件</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2847"/>
        <source>Create zip of all OSCAR data</source>
        <translation>创建所有 OSCAR 数据的压缩文件</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="559"/>
        <source>%1 (Profile: %2)</source>
        <translation>%1 (配置文件: %2)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1058"/>
        <source>Please remember to select the root folder or drive letter of your data card, and not a folder inside it.</source>
        <translation>请记住选择数据卡的根文件夹或驱动器号，而不是里面的文件夹。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1450"/>
        <source>Choose where to save screenshot</source>
        <translation>选择保存截图的位置</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1450"/>
        <source>Image files (*.png)</source>
        <translation>图像文件 (*.png)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2605"/>
        <source>You must select and open the profile you wish to modify</source>
        <translation>您必须选择并打开要修改的配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2650"/>
        <source>Would you like to zip this card?</source>
        <translation>是否要压缩此卡？</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2672"/>
        <location filename="../oscar/mainwindow.cpp" line="2743"/>
        <location filename="../oscar/mainwindow.cpp" line="2794"/>
        <source>Choose where to save zip</source>
        <translation>选择保存压缩文件的位置</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2672"/>
        <location filename="../oscar/mainwindow.cpp" line="2743"/>
        <location filename="../oscar/mainwindow.cpp" line="2794"/>
        <source>ZIP files (*.zip)</source>
        <translation>压缩文件 (*.zip)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2719"/>
        <location filename="../oscar/mainwindow.cpp" line="2757"/>
        <location filename="../oscar/mainwindow.cpp" line="2828"/>
        <source>Creating zip...</source>
        <translation>正在创建压缩文件...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2704"/>
        <location filename="../oscar/mainwindow.cpp" line="2812"/>
        <source>Calculating size...</source>
        <translation>计算大小...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2911"/>
        <source>Show Personal Data</source>
        <translation>显示个人数据</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2842"/>
        <source>Create zip of OSCAR diagnostic logs</source>
        <translation>创建 OSCAR 诊断日志的压缩文件</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1401"/>
        <source>Check for updates not implemented</source>
        <translation>检查更新功能尚未实现</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2889"/>
        <source>Standard - CPAP, APAP</source>
        <translation>标准 - CPAP, APAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2892"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Standard graph order, good for CPAP, APAP, Basic BPAP&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
<translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;标准图表顺序，适用于 CPAP, APAP, 基本 BPAP&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2897"/>
        <source>Advanced - BPAP, ASV</source>
        <translation>高级 - BPAP, ASV</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2900"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Advanced graph order, good for BPAP w/BU, ASV, AVAPS, IVAPS&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;高级图表顺序，适用于 BPAP w/BU, ASV, AVAPS, IVAPS&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2499"/>
        <location filename="../oscar/mainwindow.ui" line="2927"/>
        <source>Purge Current Selected Day</source>
        <translation>清除当前选定日的数据</translation>
    </message>
    <message>
        <source>&amp;CPAP</source>
        <translation type="obsolete">&amp;CPAP</translation>
    </message>
    <message>
        <source>&amp;Oximetry</source>
        <translation type="obsolete">&amp;血氧测量</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1074"/>
        <source>Find your CPAP data card</source>
        <translation>找到您的 CPAP 数据卡</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2327"/>
        <location filename="../oscar/mainwindow.cpp" line="2331"/>
        <source>There was a problem opening %1 Data File: %2</source>
        <translation>打开 %1 数据文件时出现问题：%2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2330"/>
        <source>%1 Data Import of %2 file(s) complete</source>
        <translation>%1 数据文件的 %2 文件导入完成</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2332"/>
        <source>%1 Import Partial Success</source>
        <translation>%1 导入部分成功</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2334"/>
        <source>%1 Data Import complete</source>
        <translation>%1 数据导入完成</translation>
    </message>
</context>
<context>
    <name>MinMaxWidget</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2186"/>
        <source>Auto-Fit</source>
        <translation>自适应</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2187"/>
        <source>Defaults</source>
        <translation>默认</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2188"/>
        <source>Override</source>
        <translation>覆盖</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2189"/>
        <source>The Y-Axis scaling mode, &apos;Auto-Fit&apos; for automatic scaling, &apos;Defaults&apos; for settings according to manufacturer, and &apos;Override&apos; to choose your own.</source>
        <translation>Y 轴缩放模式：“自适应”表示自动缩放，“默认”表示使用制造商设置，“覆盖”表示自定义设置。</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2195"/>
        <source>The Minimum Y-Axis value.. Note this can be a negative number if you wish.</source>
        <translation>Y 轴最小值，注意此值可以为负数。</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2196"/>
        <source>The Maximum Y-Axis value.. Must be greater than Minimum to work.</source>
        <translation>Y 轴最大值，必须大于最小值才能正常工作。</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2231"/>
        <source>Scaling Mode</source>
        <translation>缩放模式</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2253"/>
        <source>This button resets the Min and Max to match the Auto-Fit</source>
        <translation>此按钮将最小值和最大值重置为自适应模式</translation>
    </message>
</context>
<context>
    <name>NewProfile</name>
    <message>
        <location filename="../oscar/newprofile.ui" line="660"/>
        <source>ASV</source>
        <translation>适应性支持通气</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="650"/>
        <source>APAP</source>
        <translation>全自动正压通气</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="645"/>
        <source>CPAP</source>
        <translation>持续气道正压通气</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="402"/>
        <source>Male</source>
        <translation>男</translation>
    </message>
    <message>
        <source>&amp;Back</source>
        <translation type="vanished">&amp;上一步</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="316"/>
        <location filename="../oscar/newprofile.cpp" line="325"/>
        <source>&amp;Next</source>
        <translation>&amp;下一步</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="256"/>
        <source>TimeZone</source>
        <translation>时区</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="527"/>
        <location filename="../oscar/newprofile.ui" line="816"/>
        <source>Email</source>
        <translation>电子邮件</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="537"/>
        <location filename="../oscar/newprofile.ui" line="806"/>
        <source>Phone</source>
        <translation>电话</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="129"/>
        <source>Any reports generated are for PERSONAL USE ONLY, and NOT IN ANY WAY fit for compliance or medical diagnostic purposes.</source>
        <translation>所有生成的报告仅限个人使用，不能用于合规或医疗诊断目的。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="139"/>
        <source>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019-2024 The OSCAR Team</source>
        <translation>OSCAR 版权 &amp;copy;2011-2018 Mark Watkins 部分版权 &amp;copy;2019-2024 OSCAR 团队</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="296"/>
        <source>Duplicate or Invalid User Name</source>
        <translation>重复或无效的用户名</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="296"/>
        <source>Please Change User Name</source>
        <translation>请更改用户名</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="491"/>
        <source>&amp;Close this window</source>
        <translation>&amp;关闭此窗口</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="14"/>
        <source>Edit User Profile</source>
        <translation>编辑用户配置</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="582"/>
        <source>CPAP Treatment Information</source>
        <translation>CPAP 治疗信息</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="158"/>
        <source>Password Protect Profile</source>
        <translation>密码保护配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="127"/>
        <source>Accuracy of any data displayed is not and can not be guaranteed.</source>
        <translation>不保证显示数据的准确性。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="370"/>
        <source>D.O.B.</source>
        <translation>出生日期</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="407"/>
        <source>Female</source>
        <translation>女</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="389"/>
        <source>Gender</source>
        <translation>性别</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="423"/>
        <source>Height</source>
        <translation>身高</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="483"/>
        <source>Contact Information</source>
        <translation>联系信息</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="216"/>
        <source>Locale Settings</source>
        <translation>区域设置</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="637"/>
        <source>CPAP Mode</source>
        <translation>CPAP 模式</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="68"/>
        <source>Select Country</source>
        <translation>选择国家</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="118"/>
        <source>This software is being designed to assist you in reviewing the data produced by your CPAP Devices and related equipment.</source>
        <translation>此软件旨在帮助您查看CPAP设备及相关设备生成的数据。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="123"/>
        <source>PLEASE READ CAREFULLY</source>
        <translation>请认真阅读</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="623"/>
        <source>Untreated AHI</source>
        <translation>未治疗时的AHI</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="510"/>
        <location filename="../oscar/newprofile.ui" line="785"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="73"/>
        <source>I agree to all the conditions above.</source>
        <translation>我同意以上所有条款。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="243"/>
        <source>DST Zone</source>
        <translation>夏令时区</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="668"/>
        <source>RX Pressure</source>
        <translation>处方压力</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="960"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="976"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="992"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="185"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="136"/>
        <source>Use of this software is entirely at your own risk.</source>
        <translation>使用此软件完全由您自行承担风险。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="183"/>
        <source>Passwords don&apos;t match</source>
        <translation>密码不匹配</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="347"/>
        <source>First Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="357"/>
        <source>Last Name</source>
        <translation>姓氏</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="288"/>
        <source>Country</source>
        <translation>国家</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="vanished">&amp;取消</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="314"/>
        <source>&amp;Finish</source>
        <translation>&amp;完成</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="655"/>
        <source>Bi-Level</source>
        <translation>双水平</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="222"/>
        <source>Profile Changes</source>
        <translation>配置文件更改</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="323"/>
        <source>Personal Information (for reports)</source>
        <translation>个人信息（用于报告）</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="140"/>
        <source>User Name</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="114"/>
        <source>User Information</source>
        <translation>用户信息</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="199"/>
        <source>...twice...</source>
        <translation>...两次...</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="741"/>
        <source>Doctors Name</source>
        <translation>医生姓名</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="714"/>
        <source>Doctors / Clinic Information</source>
        <translation>医生/诊所信息</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="758"/>
        <source>Practice Name</source>
        <translation>诊所名称</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="609"/>
        <source>Date Diagnosed</source>
        <translation>诊断日期</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="222"/>
        <source>Accept and save this information?</source>
        <translation>接受并保存此信息？</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="768"/>
        <source>Patient ID</source>
        <translation>患者编号</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="173"/>
        <source>Please provide a username for this profile</source>
        <translation>请为此配置文件提供一个用户名</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="54"/>
        <source>about:blank</source>
        <translation>关于：空白</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="367"/>
        <source>It&apos;s totally ok to fib or skip this, but your rough age is needed to enhance accuracy of certain calculations.</source>
        <translation>可以跳过这一步，但提供大概年龄数据可以提高某些计算的准确性。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="386"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biological (birth) gender is sometimes needed to enhance the accuracy of a few calculations, feel free to leave this blank and skip any of them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;出生性别有时需要用于提高某些计算的准确性，可以留空或跳过。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="121"/>
        <source>OSCAR has been released freely under the &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, and comes with no warranty, and without ANY claims to fitness for any purpose.</source>
        <translation>OSCAR 已根据 &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU 公共许可证 v3&lt;/a&gt; 版本免费发布，没有任何担保，也没有任何针对任何目的的适用性声明。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="124"/>
        <source>OSCAR is intended merely as a data viewer, and definitely not a substitute for competent medical guidance from your Doctor.</source>
        <translation>OSCAR 仅作为一个数据查看器，绝不能替代您医生提供的专业医疗指导。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="132"/>
        <source>The authors will not be held liable for &lt;u&gt;anything&lt;/u&gt; related to the use or misuse of this software.</source>
        <translation>作者对与使用或误用本软件相关的&lt;u&gt;任何&lt;/u&gt;事情不承担责任。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="116"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>欢迎使用开源CPAP分析报告</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="465"/>
        <source>Metric</source>
        <translation>公制</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="470"/>
        <source>English</source>
        <translation>英制</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="155"/>
        <source>Very weak password protection and not recommended if security is required.</source>
        <translation>密码保护非常弱，如果需要安全性，不推荐使用。</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <location filename="../oscar/overview.ui" line="158"/>
        <source>End:</source>
        <translation>结束:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="276"/>
        <source>Usage</source>
        <translation>使用情况</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="267"/>
        <source>Respiratory
Disturbance
Index</source>
        <translation>呼吸
紊乱
指数</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="181"/>
        <source>Reset view to selected date range</source>
        <translation>将视图重置为所选日期范围</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="230"/>
        <source>Hint</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="237"/>
        <source>Graphing Help</source>
        <translation>绘图帮助</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="244"/>
        <source>Layout</source>
        <translation>布局</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="251"/>
        <source>Save and Restore Graph Layout Settings</source>
        <translation>保存和恢复图表布局设置</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="258"/>
        <source>Drop down to see list of graphs to switch on/off.</source>
        <translation>下拉以查看要打开/关闭的图表列表。</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="276"/>
        <source>Usage
(hours)</source>
        <translation>使用时间
（小时）</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="102"/>
        <source>Last Three Months</source>
        <translation>最近三个月</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="122"/>
        <source>Custom</source>
        <translation>自定义</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="265"/>
        <source>Graphs</source>
        <translation>图表</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="68"/>
        <source>Range:</source>
        <translation>范围:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="135"/>
        <source>Start:</source>
        <translation>开始:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="92"/>
        <source>Last Month</source>
        <translation>上个月</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="269"/>
        <source>Apnea
Hypopnea
Index</source>
        <translation>呼吸暂停
低通气
指数</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="107"/>
        <source>Last 6 Months</source>
        <translation>最近六个月</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="347"/>
        <source>Body
Mass
Index</source>
        <translation>身体
质量
指数</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="281"/>
        <source>Session Times</source>
        <translation>会话时间</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="87"/>
        <source>Last Two Weeks</source>
        <translation>最近两周</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="117"/>
        <source>Everything</source>
        <translation>所有</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="82"/>
        <source>Last Week</source>
        <translation>上周</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="112"/>
        <source>Last Year</source>
        <translation>去年</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="97"/>
        <source>Last Two Months</source>
        <translation>最近两个月</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="290"/>
        <source>Total Time in Apnea</source>
        <translation>呼吸暂停总时间</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="290"/>
        <source>Total Time in Apnea
(Minutes)</source>
        <translation>呼吸暂停总时间
（分钟）</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="353"/>
        <source>How you felt
(1-10)</source>
        <translation>感觉如何
(1-10)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="127"/>
        <source>Snapshot</source>
        <translation>快照</translation>
    </message>
    <message>
        <location filename="../oscar/overview.h" line="203"/>
        <source>Hide All Graphs</source>
        <translation>隐藏所有图表</translation>
    </message>
    <message>
        <location filename="../oscar/overview.h" line="204"/>
        <source>Show All Graphs</source>
        <translation>显示所有图表</translation>
    </message>
</context>
<context>
    <name>OximeterImport</name>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="61"/>
        <location filename="../oscar/oximeterimport.cpp" line="42"/>
        <source>Oximeter Import Wizard</source>
        <translation>血氧仪导入向导</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="444"/>
        <source>Skip this page next time.</source>
        <translation>下次跳过此页面。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="499"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Please note: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;First select your correct oximeter type from the pull-down menu below.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;请注意：&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;请先从下拉菜单中选择正确的血氧仪类型。&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt; </translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="542"/>
        <source>Where would you like to import from?</source>
        <translation>从何处导入数据？</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="586"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:700;&quot;&gt;FIRST Select your Oximeter from these groups:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:700;&quot;&gt;首先从这些组中选择您的血氧仪：&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="829"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you don&apos;t mind a being attached to a running computer overnight, this option provide a useful plethysomogram graph, which gives an indication of heart rhythm, on top of the normal oximetry readings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;如果您不介意整晚连接电脑，此选项将提供一个有用的容积图，可显示心律，并在常规的血氧读数之上显示。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="835"/>
        <source>Record attached to computer overnight (provides plethysomogram)</source>
        <translation>整晚连接电脑记录（提供容积图）</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="868"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import from data files created by software that came with your Pulse Oximeter, such as SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;此选项允许您从随血氧仪软件（如 SpO2Review）创建的数据文件导入数据。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="874"/>
        <source>Import from a datafile saved by another program, like SpO2Review</source>
        <translation>从其他程序（如 SpO2Review）保存的数据文件导入</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="953"/>
        <source>Please connect your oximeter device</source>
        <translation>请连接您的血氧仪设备</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1026"/>
        <source>Press Start to commence recording</source>
        <translation>按开始以开始记录</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1071"/>
        <source>Show Live Graphs</source>
        <translation>显示实时图表</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1101"/>
        <location filename="../oscar/oximeterimport.ui" line="1353"/>
        <source>Duration</source>
        <translation>时长</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1217"/>
        <source>Pulse Rate</source>
        <translation>脉搏率</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1309"/>
        <source>Multiple Sessions Detected</source>
        <translation>检测到多次会话</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1358"/>
        <source>Details</source>
        <translation>详情</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1375"/>
        <source>Import Completed. When did the recording start?</source>
        <translation>导入完成。记录何时开始？</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1445"/>
        <source>Oximeter Starting time</source>
        <translation>血氧仪开始时间</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1457"/>
        <source>I want to use the time reported by my oximeter&apos;s built in clock.</source>
        <translation>我想使用血氧仪内置时钟报告的时间。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1534"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: Syncing to CPAP session starting time will always be more accurate.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;注意：同步到CPAP会话的开始时间会更准确。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1555"/>
        <source>Choose CPAP session to sync to:</source>
        <translation>选择要同步的CPAP会话：</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1675"/>
        <source>You can manually adjust the time here if required:</source>
        <translation>如有需要，您可以在此手动调整时间：</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1696"/>
        <source>HH:mm:ssap</source>
        <translation>小时:分钟:秒</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1774"/>
        <source>Information Page</source>
        <translation>信息页</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1793"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1812"/>
        <source>Retry</source>
        <translation>重试</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1831"/>
        <source>Choose Session</source>
        <translation>选择会话</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1850"/>
        <source>End Recording</source>
        <translation>结束记录</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1869"/>
        <source>Sync and Save</source>
        <translation>同步并保存</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1888"/>
        <source>Save and Finish</source>
        <translation>保存并完成</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1907"/>
        <source>Start</source>
        <translation>开始</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="vanished">&amp;取消</translation>
    </message>
    <message>
        <source>&amp;Information Page</source>
        <translation type="vanished">&amp;信息页</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1473"/>
        <source>I started this oximeter recording at (or near) the same time as a session on my CPAP device.</source>
        <translation>我在 CPAP 设备会话的同时（或接近于）开始使用这个血氧仪进行记录。</translation>
    </message>
    <message>
        <source>&amp;Retry</source>
        <translation type="vanished">&amp;再试一次</translation>
    </message>
    <message>
        <source>&amp;Choose Session</source>
        <translation type="vanished">&amp;选择会话</translation>
    </message>
    <message>
        <source>&amp;End Recording</source>
        <translation type="vanished">&amp;停止记录</translation>
    </message>
    <message>
        <source>&amp;Sync and Save</source>
        <translation type="vanished">&amp;同步并存储</translation>
    </message>
    <message>
        <source>&amp;Save and Finish</source>
        <translation type="vanished">&amp;存储并结束</translation>
    </message>
    <message>
        <source>&amp;Start</source>
        <translation type="vanished">&amp;开始</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="193"/>
        <source>Scanning for compatible oximeters</source>
        <translation>正在扫描兼容的血氧仪</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="225"/>
        <source>Could not detect any connected oximeter devices.</source>
        <translation>未检测到任何连接的血氧仪设备。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="233"/>
        <source>Connecting to %1 Oximeter</source>
        <translation>正在连接到 %1 血氧仪</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="340"/>
        <source>Select upload option on %1</source>
        <translation>在 %1 上选择上传选项</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="372"/>
        <source>%1 device is uploading data...</source>
        <translation>%1 设备正在上传数据...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="373"/>
        <source>Please wait until oximeter upload process completes. Do not unplug your oximeter.</source>
        <translation>请等待血氧仪上传过程完成。不要拔出血氧仪。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="392"/>
        <source>Oximeter import completed..</source>
        <translation>血氧仪导入完成..</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="417"/>
        <source>Select a valid oximetry data file</source>
        <translation>选择一个有效的血氧仪数据文件</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="469"/>
        <source>Oximeter not detected</source>
        <translation>未检测到血氧仪</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="476"/>
        <source>Couldn&apos;t access oximeter</source>
        <translation>无法访问血氧仪</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="490"/>
        <source>Starting up...</source>
        <translation>启动中...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="491"/>
        <source>If you can still read this after a few seconds, cancel and try again</source>
        <translation>如果几秒钟后仍能看到此内容，请取消并重试</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="538"/>
        <source>Live Import Stopped</source>
        <translation>实时导入已停止</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="591"/>
        <source>%1 session(s) on %2, starting at %3</source>
        <translation>%1 会话于 %2，开始时间为 %3</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="595"/>
        <source>No CPAP data available on %1</source>
        <translation>%1 上没有可用的CPAP数据</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="730"/>
        <source>Recording...</source>
        <translation>正在记录...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="737"/>
        <source>Finger not detected</source>
        <translation>未检测到手指</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="837"/>
        <source>I want to use the time my computer recorded for this live oximetry session.</source>
        <translation>我想使用计算机记录的时间作为此次实时血氧测量会话的时间。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="840"/>
        <source>I need to set the time manually, because my oximeter doesn&apos;t have an internal clock.</source>
        <translation>我的血氧仪没有内置时钟，我需要手动设置时间。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="852"/>
        <source>Something went wrong getting session data</source>
        <translation>获取会话数据时出错</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1348"/>
        <source>Start Time</source>
        <translation>开始时间</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="307"/>
        <source>&quot;%1&quot;, session %2</source>
        <translation>&quot;%1&quot;, 会话 %2</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="337"/>
        <source>Waiting for %1 to start</source>
        <translation>等待 %1 开始</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="338"/>
        <source>Waiting for the device to start the upload process...</source>
        <translation>等待设备开始上传过程...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="688"/>
        <source>Set device date/time</source>
        <translation>设置设备日期/时间</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="695"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to enable updating the device identifier next import, which is useful for those who have multiple oximeters lying around.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;勾选以启用在下次导入时更新设备标识符，这对于有多个血氧仪的人来说很有用。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="698"/>
        <source>Set device identifier</source>
        <translation>设置设备标识符</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="761"/>
        <source>Erase session after successful upload</source>
        <translation>上传成功后删除会话</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="793"/>
        <source>Import directly from a recording on a device</source>
        <translation>直接从设备上的记录导入</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="918"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Reminder for CPAP users: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Did you remember to import your CPAP sessions first?&lt;br/&gt;&lt;/span&gt;If you forget, you won&apos;t have a valid time to sync this oximetry session to.&lt;br/&gt;To a ensure good sync between devices, always try to start both at the same time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;提醒CPAP用户：&lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;您是否记得先导入CPAP会话？&lt;br/&gt;&lt;/span&gt;如果忘记，您将无法同步此血氧仪会话的时间。&lt;br/&gt;为了确保设备之间的良好同步，请尽量同时启动两者。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="971"/>
        <source>If you can read this, you likely have your oximeter type set wrong in preferences.</source>
        <translation>如果您能看到此信息，可能是您在偏好设置中设置了错误的血氧仪类型。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="261"/>
        <source>Renaming this oximeter from &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>将此血氧仪从 &apos;%1&apos; 重命名为 &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="264"/>
        <source>Oximeter name is different.. If you only have one and are sharing it between profiles, set the name to the same on both profiles.</source>
    <translation>血氧仪名称不同。如果您只有一个血氧仪并在不同用户之间共享，请将两个用户的血氧仪名称设置为相同。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="332"/>
        <source>Nothing to import</source>
        <translation>没有可导入的数据</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="333"/>
        <source>Your oximeter did not have any valid sessions.</source>
        <translation>您的血氧仪没有任何有效会话。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="334"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="341"/>
        <source>You need to tell your oximeter to begin sending data to the computer.</source>
        <translation>您需要告知血氧仪开始向计算机发送数据。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="342"/>
        <source>Please connect your oximeter, enter it&apos;s menu and select upload to commence data transfer...</source>
        <translation>请连接您的血氧仪，进入菜单并选择上传以开始数据传输...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1141"/>
        <source>Welcome to the Oximeter Import Wizard</source>
        <translation>欢迎使用血氧仪导入向导</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1143"/>
        <source>Pulse Oximeters are medical devices used to measure blood oxygen saturation. During extended Apnea events and abnormal breathing patterns, blood oxygen saturation levels can drop significantly, and can indicate issues that need medical attention.</source>
        <translation>脉搏血氧仪是一种用于测量血氧饱和度的医疗设备。在长时间的呼吸暂停事件和异常呼吸模式期间，血氧饱和度水平会显著下降，这可能表明需要医疗关注的问题。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1149"/>
        <source>You may wish to note, other companies, such as Pulox, simply rebadge Contec CMS50&apos;s under new names, such as the Pulox PO-200, PO-300, PO-400. These should also work.</source>
        <translation>您可能会注意到，其他公司，如Pulox，只是将Contec CMS50重新贴标为新名称，如Pulox PO-200、PO-300、PO-400。这些也应该可以使用。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1152"/>
        <source>It also can read from ChoiceMMed MD300W1 oximeter .dat files.</source>
        <translation>它还可以读取ChoiceMMed MD300W1血氧仪的.dat文件。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1154"/>
        <source>Please remember:</source>
        <translation>请记住：</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1158"/>
        <source>Important Notes:</source>
        <translation>重要提示：</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1161"/>
        <source>Contec CMS50D+ devices do not have an internal clock, and do not record a starting time. If you do not have a CPAP session to link a recording to, you will have to enter the start time manually after the import process is completed.</source>
        <translation>Contec CMS50D+设备没有内部时钟，也不会记录开始时间。如果您没有CPAP会话来链接记录，则必须在导入过程完成后手动输入开始时间。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1163"/>
        <source>Even for devices with an internal clock, it is still recommended to get into the habit of starting oximeter records at the same time as CPAP sessions, because CPAP internal clocks tend to drift over time, and not all can be reset easily.</source>
        <translation>即使是带有内部时钟的设备，仍然建议养成在与CPAP会话同时启动血氧仪记录的习惯，因为CPAP内部时钟随着时间的推移往往会漂移，并且并非所有时钟都能轻松复位。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="417"/>
        <source>Oximetry Files (*.spo *.spor *.spo2 *.SpO2 *.dat)</source>
        <translation>血氧仪文件 (*.spo *.spor *.spo2 *.SpO2 *.dat)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="439"/>
        <source>No Oximetry module could parse the given file:</source>
        <translation>没有血氧仪模块可以解析给定的文件：</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="487"/>
        <source>Live Oximetry Mode</source>
        <translation>实时血氧测量模式</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="539"/>
        <source>Live Oximetry Stopped</source>
        <translation>实时血氧测量已停止</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="540"/>
        <source>Live Oximetry import has been stopped</source>
        <translation>实时血氧测量导入已停止</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1100"/>
        <source>Oximeter Session %1</source>
        <translation>血氧仪会话 %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1156"/>
        <source>If you are trying to sync oximetry and CPAP data, please make sure you imported your CPAP sessions first before proceeding!</source>
        <translation>如果您正在尝试同步血氧测定和CPAP数据，请确保您先导入了CPAP会话再继续！</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="648"/>
        <source>CMS50E/F users, when importing directly, please don&apos;t select upload on your device until OSCAR prompts you to.</source>
        <translation>CMS50E/F用户，在直接导入时，请不要在设备上选择上传，直到OSCAR提示您为止。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="685"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If enabled, OSCAR will automatically reset your CMS50&apos;s internal clock using your computers current time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;如果启用，OSCAR将使用您的计算机当前时间自动重置CMS50的内部时钟。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1325"/>
        <source>Please choose which one you want to import into OSCAR</source>
        <translation>请选择要导入OSCAR的文件</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1399"/>
        <source>Day recording (normally would have) started</source>
        <translation>白天记录（通常会）开始</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1502"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR needs a starting time to know where to save this oximetry session to.&lt;/p&gt;&lt;p&gt;Choose one of the following options:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
    <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR需要一个开始时间来知道将此血氧仪会话保存到哪里。&lt;/p&gt;&lt;p&gt;请选择以下选项之一：&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1145"/>
        <source>OSCAR gives you the ability to track Oximetry data alongside CPAP session data, which can give valuable insight into the effectiveness of CPAP treatment. It will also work standalone with your Pulse Oximeter, allowing you to store, track and review your recorded data.</source>
        <translation>OSCAR使您能够在跟踪CPAP会话数据的同时跟踪血氧测量数据，这可以为CPAP治疗的有效性提供有价值的见解。它还可以与您的脉搏血氧仪独立工作，允许您存储、跟踪和审查记录的数据。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1159"/>
        <source>For OSCAR to be able to locate and read directly from your Oximeter device, you need to ensure the correct device drivers (eg. USB to Serial UART) have been installed on your computer. For more information about this, %1click here%2.</source>
        <translation>为了使OSCAR能够直接从您的血氧仪设备中定位和读取数据，您需要确保在计算机上安装了正确的设备驱动程序（例如USB转串行UART）。有关此的更多信息，%1请点击这里%2。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1147"/>
        <source>OSCAR is currently compatible with Contec CMS50D+, CMS50E, CMS50F and CMS50I serial oximeters.&lt;br/&gt;(Note: Direct importing from bluetooth models is &lt;span style=&quot; font-weight:600;&quot;&gt;probably not&lt;/span&gt; possible yet)</source>
        <translation>OSCAR目前兼容Contec CMS50D+、CMS50E、CMS50F和CMS50I系列血氧仪。&lt;br/&gt;（注意：直接从蓝牙型号导入 &lt;span style=&quot; font-weight:600;&quot;&gt;可能&lt;/span&gt; 还不可行）</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="717"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Here you can enter a 7 character name for this oximeter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;在此处输入该血氧仪的7字符名称。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="758"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will erase the imported session from your oximeter after import has completed. &lt;/p&gt;&lt;p&gt;Use with caution, because if something goes wrong before OSCAR saves your session, you can&apos;t get it back.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;此选项将在导入完成后从您的血氧仪中删除导入的会话。&lt;/p&gt;&lt;p&gt;谨慎使用，因为如果在OSCAR保存会话之前出现问题，您将无法找回它。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="787"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import (via cable) from your oximeters internal recordings.&lt;/p&gt;&lt;p&gt;After selecting on this option, old Contec oximeters will require you to use the device&apos;s menu to initiate the upload.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;此选项允许您从血氧仪的内部记录中（通过线缆）导入。&lt;/p&gt;&lt;p&gt;选择此选项后，旧款Contec血氧仪将要求您使用设备的菜单来启动上传。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1000"/>
        <source>Please connect your oximeter device, turn it on, and enter the menu</source>
        <translation>请连接您的血氧仪设备，打开电源并进入菜单</translation>
    </message>
</context>
<context>
    <name>Oximetry</name>
    <message>
        <location filename="../oscar/oximetry.ui" line="89"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="131"/>
        <source>Reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="245"/>
        <source>Pulse</source>
        <translation>脉搏</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="366"/>
        <source>Open .spo/R File</source>
        <translation>打开 .spo/R 文件</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="385"/>
        <source>Serial Import</source>
        <translation>串行导入</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="398"/>
        <source>Start Live</source>
        <translation>开始实时</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="450"/>
        <source>Rescan Ports</source>
        <translation>重新扫描端口</translation>
    </message>
    <message>
        <source>&amp;Open .spo/R File</source>
        <translation type="vanished">&amp;打开 SPO/R 文件</translation>
    </message>
    <message>
        <source>R&amp;eset</source>
        <translation type="vanished">重&amp;置</translation>
    </message>
    <message>
        <source>Serial &amp;Import</source>
        <translation type="vanished">序列号&amp;导入</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="421"/>
        <source>Serial Port</source>
        <translation>串口</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="102"/>
        <source>d/MM/yy h:mm:ss AP</source>
        <translation>日/月/年 小时:分钟:秒</translation>
    </message>
    <message>
        <source>&amp;Start Live</source>
        <translation type="vanished">&amp;开始</translation>
    </message>
    <message>
        <source>&amp;Rescan Ports</source>
        <translation type="vanished">&amp;重新扫描端口</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="999"/>
        <location filename="../oscar/preferencesdialog.ui" line="1122"/>
        <location filename="../oscar/preferencesdialog.ui" line="1722"/>
        <location filename="../oscar/preferencesdialog.ui" line="1813"/>
        <location filename="../oscar/preferencesdialog.ui" line="1842"/>
        <source>s</source>
        <translation>秒</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation type="vanished">&amp;确定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1712"/>
        <location filename="../oscar/preferencesdialog.ui" line="1742"/>
        <location filename="../oscar/preferencesdialog.ui" line="1826"/>
        <source> bpm</source>
        <translation> bpm</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2518"/>
        <source>Graph Height</source>
        <translation>图表高度</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2899"/>
        <source>For multiple sessions, displays a thin gray line for each session at the top of the Event Flag graph.</source>

        <translation>对于多个会话，在事件标记图的顶部显示每个会话的一条细灰线。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2902"/>
        <source>Enables SessionBar in Event Flags Graph</source>
        <translation>在事件标记图中启用会话栏</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2909"/>
        <source> Enables High Resoluton Mode. Changes take effect when Oscar is restarted.</source>
    <translation> 启用高分辨率模式。更改将在重新启动Oscar时生效。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2912"/>
        <source>Enables High Resolutiom Mode</source>
        <translation>启用高分辨率模式</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2979"/>
        <source>Font</source>
        <translation>字体</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2998"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <source>&amp;CPAP</source>
        <translation type="vanished">&amp;CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2114"/>
        <source>General Settings</source>
        <translation>通用设置</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2732"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This makes scrolling when zoomed in easier on sensitive bidirectional TouchPads&lt;/p&gt;&lt;p&gt;50ms is recommended value.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;在使用双向触摸板放大时，滚动显示更容易&lt;/p&gt;&lt;p&gt;推荐值为50毫秒。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1158"/>
        <source>Event Duration</source>
        <translation>事件时长</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1858"/>
        <source>Pulse</source>
        <translation>脉搏</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2278"/>
        <source>days.</source>
        <translation>天</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="245"/>
        <source>Ignore Short Sessions</source>
        <translation>忽略短时会话</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1098"/>
        <source>Percentage of restriction in airflow from the median value.
A value of 20% works well for detecting apneas. </source>
        <translation>气流限值的中值百分比。
20%的气流限值有助于检测呼吸暂停。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="321"/>
        <source>Sessions starting before this time will go to the previous calendar day.</source>
        <translation>在此之前开始的会话将被计入前一天。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="394"/>
        <source>Session Storage Options</source>
        <translation>会话存储选项</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3177"/>
        <source>Graph Titles</source>
        <translation>图表标题</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1262"/>
        <source>Zero Reset</source>
        <translation>归零</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1058"/>
        <source>Flow Restriction</source>
        <translation>气流限制</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1810"/>
        <source>Minimum duration of drop in oxygen saturation</source>
        <translation>血氧饱和度下降的最短持续时间</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2787"/>
        <source>Overview Linecharts</source>
        <translation>线形图概览</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2869"/>
        <source>Whether to allow changing yAxis scales by double clicking on yAxis labels</source>
        <translation>是否允许通过双击Y轴标签来更改Y轴比例</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2836"/>
        <source>Pixmap caching is an graphics acceleration technique. May cause problems with font drawing in graph display area on your platform.</source>
        <translation>像素映射缓存是一种图形加速技术，可能会在您的平台上导致图表显示区域的字体绘制问题。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="575"/>
        <source>Bypass the login screen and load the most recent User Profile</source>
        <translation>跳过登录界面，加载最近的用户配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="836"/>
        <source>Data Reindex Required</source>
        <translation>需要重建数据索引</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2614"/>
        <source>Scroll Dampening</source>
        <translation>滚动抑制</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2565"/>
        <source>Standard Bars</source>
        <translation>标准条形图</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1420"/>
        <source>99% Percentile</source>
        <translation>99%百分位数</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1719"/>
        <source>Small chunks of oximetry data under this amount will be discarded.</source>
        <translation>小于此数量的血氧测量数据将被丢弃。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1259"/>
        <source>Reset the counter to zero at beginning of each (time) window.</source>
        <translation>在每个时间窗口开始时将计数器归零。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1220"/>
        <source> minutes</source>
        <translation> 分钟</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="170"/>
        <location filename="../oscar/preferencesdialog.ui" line="255"/>
        <location filename="../oscar/preferencesdialog.ui" line="753"/>
        <source>Minutes</source>
        <translation>分钟</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2418"/>
        <source>Graph Settings</source>
        <translation>图形设置</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3017"/>
        <source>Bold </source>
        <translation>粗体 </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1839"/>
        <source>Minimum duration of pulse change event.</source>
        <translation>脉搏变化事件的最短持续时间。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2812"/>
        <source>Anti-Aliasing applies smoothing to graph plots..
Certain plots look more attractive with this on.
This also affects printed reports.

Try it and see if you like it.</source>
        <translation>抗锯齿技术可对图表进行平滑处理。
开启此功能后，某些图表看起来更美观。
这也会影响打印的报告。

试试看，看看你是否喜欢它。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1377"/>
        <source>Median is recommended for ResMed users.</source>
        <translation>建议瑞思迈用户选择中位数。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3039"/>
        <source>Italic</source>
        <translation>斜体</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2139"/>
        <source>Enable Multithreading</source>
        <translation>启用多线程</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1210"/>
        <source>This may not be a good idea</source>
        <translation>这可能不是一个好主意</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1386"/>
        <source>Weighted Average</source>
        <translation>加权平均数</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1381"/>
        <location filename="../oscar/preferencesdialog.ui" line="1444"/>
        <source>Median</source>
        <translation>中位数</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1823"/>
        <source>Sudden change in Pulse Rate of at least this amount</source>
        <translation>脉搏率突然变化的最小值</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1965"/>
        <location filename="../oscar/preferencesdialog.ui" line="2044"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1349"/>
        <source>Middle Calculations</source>
        <translation>中间计算</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2862"/>
        <source>Skip over Empty Days</source>
        <translation>跳过无数据的天数</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2560"/>
        <source>The visual method of displaying waveform overlay flags.
</source>
        <translation>显示波形叠加标记的视觉方法。
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1363"/>
        <source>Upper Percentile</source>
        <translation>上百分位数</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="843"/>
        <source>Restart Required</source>
        <translation>需要重启</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1415"/>
        <source>True Maximum</source>
        <translation>真正的最大值</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1326"/>
        <source>For consistancy, ResMed users should use 95% here,
as this is the only value available on summary-only days.</source>
        <translation>为了保持一致，瑞思迈用户应在此处使用95%，
因为这是摘要日唯一可用的值。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3116"/>
        <source>Graph Text</source>
        <translation>图表文字</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2135"/>
        <source>Allow use of multiple CPU cores where available to improve performance.
Mainly affects the importer.</source>
        <translation>允许在可用时使用多个CPU核心以提高性能。
主要影响导入器。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2697"/>
        <source>Line Chart</source>
        <translation>线形图</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2626"/>
        <source>How long you want the tooltips to stay visible.</source>
        <translation>设置工具提示可见的时间长度。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="190"/>
        <source>Multiple sessions closer together than this value will be kept on the same day.
</source>
        <translation>多个会话间距小于此值将被保存在同一天。
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1119"/>
        <source>Duration of airflow restriction</source>
        <translation>气流限制的持续时间</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1458"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Due to summary design limitations, ResMed devices do not support changing these settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;注意：&lt;/span&gt;由于摘要设计限制，ResMed设备不支持更改这些设置。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2692"/>
        <source>Bar Tops</source>
        <translation>条形图顶端</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2806"/>
        <source>Other Visual Settings</source>
        <translation>其他视觉设置</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="311"/>
        <source>Day Split Time</source>
        <translation>每日分割时间</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3238"/>
        <source>Big Text</source>
        <translation>大字体</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2849"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These features have recently been pruned. They will come back later. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;这些功能最近被裁剪了。它们会在以后回归。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1541"/>
        <source>Regard days with under this usage as &quot;incompliant&quot;. 4 hours is usually considered
 compliant.</source>
        <translation>将低于此使用量的天数视为“不符合要求”。通常认为4小时是符合要求的。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2859"/>
        <source>Daily view navigation buttons will skip over days without data records</source>
        <translation>每日视图导航按钮将跳过没有数据记录的天数</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1216"/>
        <source>Adjusts the amount of data considered for each point in the AHI/Hour graph.
Defaults to 60 minutes.. Highly recommend it&apos;s left at this value.</source>
        <translation>调整AHI/小时图表中每个点考虑的数据量。
默认值为60分钟。强烈建议保留此值。</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="vanished">&amp;取消</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2308"/>
        <source>Last Checked For Updates: </source>
        <translation>上次检查更新： </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3305"/>
        <location filename="../oscar/preferencesdialog.cpp" line="499"/>
        <location filename="../oscar/preferencesdialog.cpp" line="631"/>
        <source>Details</source>
        <translation>详情</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2819"/>
        <source>Use Anti-Aliasing</source>
        <translation>使用抗锯齿</translation>
    </message>
    <message>
        <source>Animations &amp;&amp; Fancy Stuff</source>
        <translation type="vanished">动画 &amp;&amp; 花哨的东西</translation>
    </message>
    <message>
        <source>&amp;Import</source>
        <translation type="vanished">&amp;导入</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1317"/>
        <source>Changes to the following settings needs a restart, but not a recalc.</source>
        <translation>更改以下设置需要重启，但不需要重新计算。</translation>
    </message>
    <message>
        <source>&amp;Appearance</source>
        <translation type="vanished">&amp;外观</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2580"/>
        <source>The pixel thickness of line plots</source>
        <translation>线条图的像素厚度</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="160"/>
        <source>Combine Close Sessions</source>
        <translation>合并接近的会话</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2872"/>
        <source>Allow YAxis Scaling</source>
        <translation>允许Y轴缩放</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2839"/>
        <source>Use Pixmap Caching</source>
        <translation>使用像素映射缓存</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2255"/>
        <source>Check for new version every</source>
        <translation>每隔一定时间检查新版本</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1356"/>
        <source>Maximum Calcs</source>
        <translation>最大计算值</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2604"/>
        <source>Tooltip Timeout</source>
        <translation>工具提示超时</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="29"/>
        <source>Preferences</source>
        <translation>参数设置</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2711"/>
        <source>Default display height of graphs in pixels</source>
        <translation>图表的默认显示高度（以像素为单位）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2534"/>
        <source>Overlay Flags</source>
        <translation>叠加标记</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2826"/>
        <source>Makes certain plots look more &quot;square waved&quot;.</source>
        <translation>使某些图表看起来更像“方波”。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1865"/>
        <source>Percentage drop in oxygen saturation</source>
        <translation>血氧饱和度下降的百分比</translation>
    </message>
    <message>
        <source>&amp;General</source>
        <translation type="vanished">&amp;通用</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="441"/>
        <source>Compress SD Card Backups (slower first import, but makes backups smaller)</source>
        <translation>压缩SD卡备份（首次导入较慢，但备份更小）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1391"/>
        <source>Normal Average</source>
        <translation>普通平均数</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="837"/>
        <source>A data reindexing proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>需要重建数据索引以应用这些更改。此操作可能需要几分钟才能完成。

确定要进行这些更改吗？</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1320"/>
        <source>Preferred Calculation Methods</source>
        <translation>首选计算方法</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2684"/>
        <source>Graph Tooltips</source>
        <translation>图表工具提示</translation>
    </message>
    <message>
        <source>&amp;Oximetry</source>
        <translation type="vanished">&amp;血氧测量</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="706"/>
        <source>CPAP Clock Drift</source>
        <translation>CPAP时钟漂移</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2829"/>
        <source>Square Wave Plots</source>
        <translation>方波图</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2321"/>
        <source>TextLabel</source>
        <translation>文本标签</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3052"/>
        <source>Application</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2544"/>
        <source>Line Thickness</source>
        <translation>线宽</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="502"/>
        <source>Do not import sessions older than:</source>
        <translation>请不要导入早于如下日期的会话：</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="509"/>
        <source>Sessions older than this date will not be imported</source>
        <translation>将不会导入早于此日期的会话</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="535"/>
        <source>dd MMMM yyyy</source>
        <translation>dd MMMM yyyy</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1275"/>
        <source>User definable threshold considered large leak</source>
        <translation>用户自定义的大量漏气阈值</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1242"/>
        <source>Whether to show the leak redline in the leak graph</source>
        <translation>是否在漏气图表中显示漏气限值红线</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1295"/>
        <source>Are you really sure you want to do this?</source>
        <translation>确定进行此操作吗？</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1112"/>
        <source>Show in Event Breakdown Piechart</source>
        <translation>在事件分类饼图中显示</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="484"/>
        <source>Create SD Card Backups during Import (Turn this off at your own peril!)</source>
        <translation>在导入过程中创建SD卡备份（关闭此功能需自行承担风险！）</translation>
    </message>
    <message>
        <source>Reset &amp;Defaults</source>
        <translation type="vanished">恢复&amp;默认设置</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2008"/>
        <location filename="../oscar/preferencesdialog.ui" line="2087"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Just because you can, does not mean it&apos;s good practice.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;警告：&lt;/span&gt;仅仅因为你可以做，并不意味着这是一个好习惯。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2021"/>
        <source>Waveforms</source>
        <translation>波形</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="494"/>
        <location filename="../oscar/preferencesdialog.cpp" line="625"/>
        <source>Name</source>
        <translation>姓名</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="495"/>
        <location filename="../oscar/preferencesdialog.cpp" line="626"/>
        <source>Color</source>
        <translation>颜色</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="498"/>
        <location filename="../oscar/preferencesdialog.cpp" line="630"/>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1942"/>
        <source>Events</source>
        <translation>事件</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1791"/>
        <source>Flag rapid changes in oximetry stats</source>
        <translation>在血氧统计数据中标记快速变化</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1699"/>
        <source>Other oximetry options</source>
        <translation>其他血氧选项</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1732"/>
        <source>Discard segments under</source>
        <translation>丢弃低于以下数值的数据段</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1772"/>
        <source>Flag Pulse Rate Above</source>
        <translation>心率高于标记值</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1762"/>
        <source>Flag Pulse Rate Below</source>
        <translation>心率低于标记值</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="71"/>
        <source>Flag</source>
        <translation>标记</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="72"/>
        <source>Minor Flag</source>
        <translation>次要标记</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="73"/>
        <source>Span</source>
        <translation>范围</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="74"/>
        <source>Always Minor</source>
        <translation>始终次要</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="497"/>
        <source>Flag Type</source>
        <translation>标记类型</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="515"/>
        <source>CPAP Events</source>
        <translation>CPAP事件</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="516"/>
        <source>Oximeter Events</source>
        <translation>血氧仪事件</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="517"/>
        <source>Positional Events</source>
        <translation>位置事件</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="518"/>
        <source>Sleep Stage Events</source>
        <translation>睡眠阶段事件</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="519"/>
        <source>Unknown Events</source>
        <translation>未知事件</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="691"/>
        <source>Double click to change the descriptive name this channel.</source>
        <translation>双击更改此通道的描述名称。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="569"/>
        <location filename="../oscar/preferencesdialog.cpp" line="698"/>
        <source>Double click to change the default color for this channel plot/flag/data.</source>
        <translation>双击更改此通道图表/标记/数据的默认颜色。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="49"/>
        <source>Clinical Mode:</source>
        <translation>临床模式：</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="50"/>
        <source>Reports what is on the data card, all of it including any and all data deselected in the Permissive mode.</source>
        <translation>报告数据卡上的所有数据，包括在宽容模式中未选择的任何和所有数据。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="51"/>
        <source>Basically replicates the reports and data stored on the devices data card.</source>
        <translation>基本上复制存储在设备数据卡上的报告和数据。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="52"/>
        <source>This includes pap devices, oximeters, etc. Compliance reports fall under this mode.</source>
        <translation>这包括PAP设备、血氧仪等。符合性报告属于此模式。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="53"/>
        <source>Compliance reports always include all data within the chosen Compliance period, even if otherwise deselected.</source>
        <translation>符合性报告总是包括所选符合期内的所有数据，即使其他情况下未选择。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="55"/>
        <source>Permissive Mode:</source>
        <translation>宽容模式：</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="56"/>
        <source>Allows user to select which data sets/ sessions to be used for calculations and display.</source>
        <translation>允许用户选择用于计算和显示的数据集/会话。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="57"/>
        <source>Additional charts and calculations may be available that are not available from the vendor data.</source>
        <translation>可能有供应商数据中没有的其他图表和计算。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="58"/>
        <source>Enables Custom UserFlags displayed in the statistics Therapy Efficacy section</source>
        <translation>启用在统计治疗效果部分显示的自定义用户标记</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="87"/>
        <source>No CPAP devices detected</source>
        <translation>未检测到CPAP设备</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="88"/>
        <source>Will you be using a ResMed brand device?</source>
        <translation>您将使用瑞思迈品牌设备吗？</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="95"/>
        <source>&lt;p&gt;&lt;b&gt;Please Note:&lt;/b&gt; OSCAR&apos;s advanced session splitting capabilities are not possible with &lt;b&gt;ResMed&lt;/b&gt; devices due to a limitation in the way their settings and summary data is stored, and therefore they have been disabled for this profile.&lt;/p&gt;&lt;p&gt;On ResMed devices, days will &lt;b&gt;split at noon&lt;/b&gt; like in ResMed&apos;s commercial software.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;请注意：&lt;/b&gt;由于瑞思迈设备的设置和汇总数据存储方式有限，OSCAR 的高级会话拆分功能无法在瑞思迈设备上实现，因此已在此配置文件中禁用。&lt;/p&gt;&lt;p&gt;在瑞思迈设备上，日期将像在瑞思迈的商业软件中一样&lt;b&gt;在中午分割&lt;/b&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="584"/>
        <source>Here you can change the type of flag shown for this event</source>
        <translation>在这里可以更改显示在此事件上的标记类型</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="589"/>
        <location filename="../oscar/preferencesdialog.cpp" line="722"/>
        <source>This is the short-form label to indicate this channel on screen.</source>
        <translation>这是屏幕上显示此通道的简短标签。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="595"/>
        <location filename="../oscar/preferencesdialog.cpp" line="728"/>
        <source>This is a description of what this channel does.</source>
        <translation>这是对该通道功能的描述。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="628"/>
        <source>Lower</source>
        <translation>更低</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="629"/>
        <source>Upper</source>
        <translation>更高</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="648"/>
        <source>CPAP Waveforms</source>
        <translation>CPAP波形</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="649"/>
        <source>Oximeter Waveforms</source>
        <translation>血氧仪波形</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="650"/>
        <source>Positional Waveforms</source>
        <translation>位置波形</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="651"/>
        <source>Sleep Stage Waveforms</source>
        <translation>睡眠阶段波形</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="712"/>
        <source>Here you can set the &lt;b&gt;lower&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>在这里可以为%1波形设置&lt;b&gt;更低的&lt;/b&gt;阈值来进行某些计算</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="717"/>
        <source>Here you can set the &lt;b&gt;upper&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>在这里可以为%1波形设置&lt;b&gt;更高的&lt;/b&gt;阈值来进行某些计算</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1211"/>
        <source>ResMed S9 devices routinely delete certain data from your SD card older than 7 and 30 days (depending on resolution).</source>
        <translation>瑞思迈S9设备常规删除SD卡中分辨率高于7天和30天的某些数据。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2570"/>
        <source>Top Markers</source>
        <translation>置顶标记</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="97"/>
        <source>Session Splitting Settings</source>
        <translation>会话拆分设置</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="357"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;This setting should be used with caution...&lt;/span&gt; Switching it off comes with consequences involving accuracy of summary only days, as certain calculations only work properly provided summary only sessions that came from individual day records are kept together. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed users:&lt;/span&gt; Just because it seems natural to you and I that the 12 noon session restart should be in the previous day, does not mean ResMed&apos;s data agrees with us. The STF.edf summary index format has serious weaknesses that make doing this not a good idea.&lt;/p&gt;&lt;p&gt;This option exists to pacify those who don&apos;t care and want to see this &amp;quot;fixed&amp;quot; no matter the costs, but know it comes with a cost. If you keep your SD card in every night, and import at least once a week, you won&apos;t see problems with this very often.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;设置前请谨慎...&lt;/span&gt;关闭此选项会影响仅汇总天数的准确性，因为某些计算仅在来自单个日期记录的仅汇总会话保持在一起时才能正常工作。&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-weight:600;&quot;&gt;瑞思迈用户：&lt;/span&gt;尽管对你我来说，正午12点之前属于前一天似乎很自然，但这并不意味着瑞思迈的数据同意我们。STF.edf摘要索引格式存在严重缺陷，使得这样做不是一个好主意。&lt;/p&gt;&lt;p&gt;此选项存在的意义在于安抚那些不在意看到什么，只是想看到&quot;固定的数据&quot;，无论代价，但请知晓这会带来代价。如果您每晚都将SD卡保存在设备中，并且至少每周导入一次，您将很少遇到此问题。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="360"/>
        <source>Don&apos;t Split Summary Days (Warning: read the tooltip!)</source>
        <translation>不拆分汇总天数（警告：请阅读工具提示！）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="569"/>
        <source>Memory and Startup Options</source>
        <translation>存储与启动选项</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="611"/>
        <source>Pre-Load all summary data at startup</source>
        <translation>启动时预加载所有汇总数据</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="598"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This setting keeps waveform and event data in memory after use to speed up revisiting days.&lt;/p&gt;&lt;p&gt;This is not really a necessary option, as your operating system caches previously used files too.&lt;/p&gt;&lt;p&gt;Recommendation is to leave it switched off, unless your computer has a ton of memory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;此设置将波形和事件数据保存在内存中，以加快重新访问天数的速度。&lt;/p&gt;&lt;p&gt;这并不是一个必要的选项，因为您的操作系统也会缓存之前使用的文件。&lt;/p&gt;&lt;p&gt;建议将其保持关闭状态，除非您的计算机内存非常大。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="475"/>
        <source>This maintains a backup of SD-card data for ResMed devices,

ResMed S9 series devices delete high resolution data older than 7 days,
and graph data older than 30 days..

OSCAR can keep a copy of this data if you ever need to reinstall.
(Highly recomended, unless your short on disk space or don&apos;t care about the graph data)</source>
        <translation>这会为瑞思迈设备保留SD卡数据的备份，

瑞思迈S9系列设备会删除超过7天的高分辨率数据，
和超过30天的图表数据。

如果您需要重新安装，OSCAR可以保留此数据的副本。
（强烈推荐，除非您的磁盘空间不足或不在意图表数据）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="601"/>
        <source>Keep Waveform/Event data in memory</source>
        <translation>将波形/事件数据保存在内存中</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="625"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cuts down on any unimportant confirmation dialogs during import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;在导入过程中减少任何不重要的确认对话框。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="628"/>
        <source>Import without asking for confirmation</source>
        <translation>无需确认直接导入</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="635"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data from any device model that has not yet been tested by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;当从尚未经过OSCAR开发人员测试的任何设备型号导入数据时提供警报。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt; </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="638"/>
        <source>Warn when importing data from an untested device</source>
        <translation>从未经测试的设备导入数据时发出警告</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="803"/>
        <source>This calculation requires Total Leaks data to be provided by the CPAP device. (Eg, PRS1, but not ResMed, which has these already)

The Unintentional Leak calculations used here are linear, they don&apos;t model the mask vent curve.

If you use a few different masks, pick average values instead. It should still be close enough.</source>
<translation>此计算需要由CPAP设备提供的总漏气数据。（例如，PRS1，但不包括瑞思迈，因为它已经有了这些数据）

这里使用的非故意漏气计算是线性的，它们不模拟面罩通风曲线。

如果您使用几种不同的面罩，请选择平均值。它仍应足够接近。
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="967"/>
        <source>Enable/disable experimental event flagging enhancements.
It allows detecting borderline events, and some the device missed.
This option must be enabled before import, otherwise a purge is required.</source>
<translation>启用/禁用实验性事件标记增强功能。
它允许检测边界事件和一些设备漏掉的事件。
此选项必须在导入之前启用，否则需要清除数据。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1019"/>
        <source>This experimental option attempts to use OSCAR&apos;s event flagging system to improve device detected event positioning.</source>
        <translation>此实验性选项尝试使用OSCAR的事件标记系统来改进设备检测事件的位置。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1022"/>
        <source>Resync Device Detected Events (Experimental)</source>
        <translation>重新同步设备检测事件（实验性）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1079"/>
        <source>Custom flagging is an experimental method of detecting events missed by the device. They are not included in AHI. They are also displayed in the Statistics Tab using the Permissive mode (see Clinical tab).</source>
        <translation>自定义标记是一种实验性方法，用于检测设备漏掉的事件。它们不包含在AHI中。它们还使用宽容模式显示在统计标签中（参见临床标签）。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1145"/>
        <source>Allow duplicates near device events.</source>
        <translation>允许在设备事件附近重复。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1187"/>
        <source>General CPAP and Related Settings</source>
        <translation>通用CPAP及相关设置</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1193"/>
        <source>Show flags for device detected events that haven&apos;t been identified yet.</source>
        <translation>显示尚未识别的设备检测事件的标记。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1196"/>
        <source>Enable Unknown Events Channels</source>
        <translation>启用未知事件通道</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1295"/>
        <source>AHI</source>
        <extracomment>Apnea Hypopnea Index</extracomment>
        <translatorcomment>呼吸暂停低通气指数</translatorcomment>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1300"/>
        <source>RDI</source>
        <extracomment>Respiratory Disturbance Index</extracomment>
        <translatorcomment>呼吸紊乱指数</translatorcomment>
        <translation>RDI</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1203"/>
        <source>AHI/Hour Graph Time Window</source>
        <translation>AHI/小时图表时间窗口</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1252"/>
        <source>Preferred major event index</source>
        <translation>首选主要事件指数</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1544"/>
        <source>Compliance defined as</source>
        <translation>合规性定义为</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1245"/>
        <source>Flag leaks over threshold</source>
        <translation>标记超过阈值的漏气</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="787"/>
        <source>Seconds</source>
        <translation>秒</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="733"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: This is not intended for timezone corrections! Make sure your operating system clock and timezone is set correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;注意: 这不是为了时区校正！请确保您的操作系统时钟和时区设置正确。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="780"/>
        <source>Hours</source>
        <translation>小时</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1411"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;True maximum is the maximum of the data set.&lt;/p&gt;&lt;p&gt;99th percentile filters out the rarest outliers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;真正的最大值是数据集的最大值。&lt;/p&gt;&lt;p&gt;第99百分位数滤除最稀有的离群值。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1429"/>
        <source>Combined Count divided by Total Hours</source>
        <translation>合并计数除以总小时数</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1434"/>
        <source>Time Weighted average of Indice</source>
        <translation>时间加权平均指数</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1439"/>
        <source>Standard average of indice</source>
        <translation>标准平均指数</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="972"/>
        <source>Custom CPAP User Event Flagging</source>
        <translation>自定义CPAP用户事件标记</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2944"/>
        <source>Fonts (Application wide settings)</source>
        <translation>字体（应用程序全局设置）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2456"/>
        <location filename="../oscar/preferencesdialog.ui" line="2495"/>
        <location filename="../oscar/preferencesdialog.cpp" line="496"/>
        <location filename="../oscar/preferencesdialog.cpp" line="627"/>
        <source>Overview</source>
        <translation>概览</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="561"/>
        <source>Double click to change the descriptive name the &apos;%1&apos; channel.</source>
        <translation>双击更改 &apos;%1&apos; 通道的描述名称。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="574"/>
        <source>Whether this flag has a dedicated overview chart.</source>
        <translation>此标志是否有专用的概览图表。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="707"/>
        <source>Whether a breakdown of this waveform displays in overview.</source>
        <translation>是否在概览中显示此波形的细分。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="810"/>
        <source>Calculate Unintentional Leaks When Not Present</source>
        <translation>当不存在时计算无意漏气</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="906"/>
        <source>4 cmH2O</source>
        <translation>4 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="916"/>
        <source>20 cmH2O</source>
        <translation>20 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="948"/>
        <source>Note: A linear calculation method is used. Changing these values requires a recalculation.</source>
        <translation>注意: 使用线性计算方法。更改这些值需要重新计算。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="578"/>
        <source>Auto-Launch CPAP Importer after opening profile</source>
        <translation>打开配置文件后自动启动CPAP导入程序</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="70"/>
        <source>Import</source>
        <translation>导入</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="433"/>
        <source>Compress ResMed (EDF) backups to save disk space.
Backed up EDF files are stored in the .gz format, 
which is common on Mac  Linux platforms..

OSCAR can import from this compressed backup directory natively.. 
To use it with ResScan will require the .gz files to be uncompressed first..</source>
        <translation>压缩ResMed (EDF) 备份以节省磁盘空间。
备份的EDF文件以.gz格式存储，
这在Mac和Linux平台上很常见。

OSCAR可以直接从这个压缩备份目录导入。
要与ResScan一起使用，需要先解压.gz文件。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="618"/>
        <source>Automatically load last used profile on start-up</source>
        <translation>启动时自动加载上次使用的配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="659"/>
        <source>CPAP</source>
        <translation>持续气道正压通气</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1278"/>
        <source>l/min</source>
        <translation>升/分钟</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1370"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cumulative Indices&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;累积指数&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1490"/>
        <source>Clinical</source>
        <translation>临床</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1517"/>
        <source>Clinical Settings</source>
        <translation>临床设置</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1573"/>
        <location filename="../oscar/preferencesdialog.ui" line="1903"/>
        <location filename="../oscar/preferencesdialog.ui" line="1995"/>
        <location filename="../oscar/preferencesdialog.ui" line="2074"/>
        <source>Reset Defaults</source>
        <translation>重置为默认值</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1586"/>
        <source>Select Oscar Operating Mode</source>
        <translation>选择Oscar操作模式</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1592"/>
        <source>Clinical Mode does not allow disabled sessions.\nDisabled Session are not used for graphing or Statistics.</source>
        <translation>临床模式不允许禁用会话。\n禁用的会话不用于绘图或统计。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1595"/>
        <source>Clinical Mode</source>
        <translation>临床模式</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1608"/>
        <source>Permissive Mode allows disabled sessions.\nDisabled Session are used for graphing and Statistics.</source>
        <translation>宽容模式允许禁用会话。\n禁用的会话用于绘图和统计。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1611"/>
        <source>Permissive Mode</source>
        <translation>宽容模式</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1621"/>
        <source> Hours</source>
        <translation> 小时</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1648"/>
        <source>Oximetry</source>
        <translation>血氧测定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1752"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Flag SpO&lt;span style=&quot; vertical-align:sub;&quot;&gt;2&lt;/span&gt; Desaturations Below&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;标记低于SpO&lt;span style=&quot; vertical-align:sub;&quot;&gt;2&lt;/span&gt; 的去饱和&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1925"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Syncing Oximetry and CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data imported from SpO2Review (from .spoR files) or the serial import method do &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; have the correct timestamp needed to sync.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live view mode (using a serial cable) is one way to achieve an accurate sync on CMS50 oximeters, but does not counter for CPAP clock drift.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;If you start your Oximeters recording mode at &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactly &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;the same time you start your CPAP device, you can now also achieve sync. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;The serial import process takes the starting time from last nights first CPAP session. (Remember to import your CPAP data first!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;同步血氧和CPAP数据&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;从SpO2Review（.spoR文件）或串行导入方法导入的CMS50数据&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;不能&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;获得所需的正确时间戳来同步。&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;实时查看模式（使用串行电缆）是实现CMS50血氧计上准确同步的一种方法，但不能解决CPAP时钟漂移问题。&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;如果您在开始CPAP设备时&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;准确地&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;开始您的血氧计的录制模式，您现在也可以实现同步。&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;串行导入过程将从昨晚的第一次CPAP会话中获取开始时间。（记得先导入您的CPAP数据！）&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2100"/>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2346"/>
        <source>I want to be notified of test versions. (Advanced users only please.)</source>
        <translation>我希望收到测试版本的通知。（仅适用于高级用户。）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2389"/>
        <source>Appearance</source>
        <translation>外观</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2424"/>
        <source>On Opening</source>
        <translation>启动时</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2437"/>
        <location filename="../oscar/preferencesdialog.ui" line="2441"/>
        <source>Profile</source>
        <translation>配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2446"/>
        <location filename="../oscar/preferencesdialog.ui" line="2485"/>
        <source>Welcome</source>
        <translation>欢迎</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2451"/>
        <location filename="../oscar/preferencesdialog.ui" line="2490"/>
        <source>Daily</source>
        <translation>日常</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2461"/>
        <location filename="../oscar/preferencesdialog.ui" line="2500"/>
        <source>Statistics</source>
        <translation>统计</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2469"/>
        <source>Switch Tabs</source>
        <translation>切换标签</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2480"/>
        <source>No change</source>
        <translation>无更改</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2508"/>
        <source>After Import</source>
        <translation>导入后</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3337"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3344"/>
        <source>Ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="322"/>
        <source>Never</source>
        <translation>从不</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="827"/>
        <source>Data Processing Required</source>
        <translation>需要数据处理</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="828"/>
        <source>A data re/decompression proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>应用这些更改需要数据重新/解压缩过程。此操作可能需要几分钟才能完成。

确定要进行这些更改吗？</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2179"/>
        <source>Graphics Engine (Requires Restart)</source>
        <translation>图形引擎（需要重启）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="844"/>
        <source>One or more of the changes you have made will require this application to be restarted, in order for
 these changes to come into effect.

Would you like do this now?</source>
        <translation>您所做的一个或多个更改将要求重新启动此应用程序，以使这些更改生效。

现在要进行此操作吗？</translation>
    </message>
    <message>
        <source>Compress ResMed (EDF) backups to save disk space.
Backed up EDF files are stored in the .gz format,
which is common on Mac &amp; Linux platforms..

OSCAR can import from this compressed backup directory natively..
To use it with ResScan will require the .gz files to be uncompressed first..
</source>
        <translation>压缩ResMed (EDF) 备份以节省磁盘空间。
备份的EDF文件以.gz格式存储，
这在Mac和Linux平台上很常见。

OSCAR可以直接从这个压缩备份目录导入。
要与ResScan一起使用，需要先解压.gz文件。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="453"/>
        <source>The following options affect the amount of disk space OSCAR uses, and have an effect on how long import takes.</source>
        <translation>以下选项会影响OSCAR使用的磁盘空间量，并影响导入所需的时间。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="463"/>
        <source>This makes OSCAR&apos;s data take around half as much space.
But it makes import and day changing take longer..
If you&apos;ve got a new computer with a small solid state disk, this is a good option.</source>
        <translation>这使得OSCAR的数据占用大约一半的空间。
但导入和日期切换会需要更长的时间。
如果您有一台配有小型固态硬盘的新计算机，这是一个不错的选择。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="468"/>
        <source>Compress Session Data (makes OSCAR data smaller, but day changing slower.)</source>
        <translation>压缩会话数据（使OSCAR数据更小，但日期切换速度更慢。）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="608"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes starting OSCAR a bit slower, by pre-loading all the summary data in advance, which speeds up overview browsing and a few other calculations later on. &lt;/p&gt;&lt;p&gt;If you have a large amount of data, it might be worth keeping this switched off, but if you typically like to view &lt;span style=&quot; font-style:italic;&quot;&gt;everything&lt;/span&gt; in overview, all the summary data still has to be loaded anyway. &lt;/p&gt;&lt;p&gt;Note this setting doesn&apos;t affect waveform and event data, which is always demand loaded as needed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;通过预加载所有摘要数据，使OSCAR启动速度稍慢，但可以加快浏览概览和稍后的其他一些计算。&lt;/p&gt;&lt;p&gt;如果您有大量数据，建议关闭此选项，但如果您通常喜欢在概览中查看&lt;span style=&quot; font-style:italic;&quot;&gt;所有内容&lt;/span&gt;，所有摘要数据仍然需要加载。&lt;/p&gt;&lt;p&gt;注意：此设置不影响波形和事件数据，它们始终根据需要加载。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1669"/>
        <source>Oximetry Settings - Not Currently Functional</source>
        <translation>血氧测定设置 - 目前不可用</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2146"/>
        <source>Show Remove Card reminder notification on OSCAR shutdown</source>
        <translation>OSCAR关闭时显示移除卡片提醒通知</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2434"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Which tab to open on loading a profile. (Note: It will default to Profile if OSCAR is set to not open a profile on startup)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;加载配置文件时要打开哪个选项卡。（注意：如果OSCAR设置为在启动时不打开配置文件，它将默认打开配置文件选项卡）&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2185"/>
        <source>Try changing this from the default setting (Desktop OpenGL) if you experience rendering problems with OSCAR&apos;s graphs.</source>
        <translation>如果您遇到OSCAR图形渲染问题，请尝试将其从默认设置（桌面OpenGL）更改为其他选项。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1212"/>
        <source>If you ever need to reimport this data again (whether in OSCAR or ResScan) this data won&apos;t come back.</source>
<translation>如果您需要再次重新导入此数据（无论是在OSCAR还是ResScan中），此数据将不会返回。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1213"/>
        <source> If you need to conserve disk space, please remember to carry out manual backups.</source>
        <translation> 如果您需要节省磁盘空间，请记得手动备份。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1214"/>
        <source> Are you sure you want to disable these backups?</source>
        <translation> 确定要禁用这些备份吗？</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1294"/>
        <source>Switching off backups is not a good idea, because OSCAR needs these to rebuild the database if errors are found.

</source>
        <translation>关闭备份不是一个好主意，因为如果发现错误，OSCAR需要这些备份来重建数据库。

</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="423"/>
        <source>Changing SD Backup compression options doesn&apos;t automatically recompress backup data.</source>
        <translation>更改SD备份压缩选项不会自动重新压缩备份数据。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="819"/>
        <source>Your masks vent rate at 20 cmH2O pressure</source>
        <translation>您的面罩在20 cmH2O压力下的通气率</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="875"/>
        <source>Your masks vent rate at 4 cmH2O pressure</source>
        <translation>您的面罩在4 cmH2O压力下的通气率</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2882"/>
        <source>Include Serial Number</source>
        <translation>包括序列号</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="645"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data that is somehow different from anything previously seen by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;当导入的数据与OSCAR开发人员以前见过的任何数据有所不同时提供警报。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="272"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt;&quot;&gt;Sessions shorter in duration than this will not be displayed&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt;&quot;&gt;时长短于此的会话将不显示&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-style:italic;&quot;&gt;。&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="648"/>
        <source>Warn when previously unseen data is encountered</source>
        <translation>当遇到以前未见过的数据时警告</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2153"/>
        <source>Always save screenshots in the OSCAR Data folder</source>
        <translation>始终将屏幕截图保存到OSCAR数据文件夹中</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2207"/>
        <source>Check For Updates</source>
<translation>检查更新</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2222"/>
        <source>You are using a test version of OSCAR. Test versions check for updates automatically at least once every seven days. You may set the interval to less than seven days.</source>
        <translation>您正在使用OSCAR的测试版本。测试版本会每七天自动检查一次更新。您可以将间隔设置为少于七天。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2240"/>
        <source>Automatically check for updates</source>
        <translation>自动检查更新</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2262"/>
        <source>How often OSCAR should check for updates.</source>
        <translation>OSCAR应该多久检查一次更新。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2343"/>
        <source>If you are interested in helping test new features and bugfixes early, click here.</source>
        <translation>如果您有兴趣提前测试新功能和错误修复，请点击这里。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2359"/>
        <source>If you would like to help test early versions of OSCAR, please see the Wiki page about testing OSCAR.  We welcome everyone who would like to test OSCAR, help develop OSCAR, and help with translations to existing or new languages. https://www.sleepfiles.com/OSCAR</source>
        <translation>如果您愿意帮助测试OSCAR的早期版本，请参阅有关测试OSCAR的Wiki页面。我们欢迎所有愿意测试OSCAR、帮助开发OSCAR和帮助翻译现有或新语言的人员。https://www.sleepfiles.com/OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2852"/>
        <source>Animations  Fancy Stuff</source>
    <translation>动画效果</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2879"/>
        <source>Whether to include device serial number on device settings changes report</source>
    <translation>是否在设备设置更改报告中包含设备序列号</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2889"/>
        <source>Print reports in black and white, which can be more legible on non-color printers</source>
    <translation>以黑白打印报告，这在非彩色打印机上更易读</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2892"/>
        <source>Print reports in black and white (monochrome)</source>
        <translation>以黑白（单色）打印报告</translation>
    </message>
</context>
<context>
    <name>ProfileSelector</name>
    <message>
        <location filename="../oscar/profileselector.ui" line="26"/>
        <source>Filter:</source>
        <translation>筛选：</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="198"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <source>&amp;Open Profile</source>
        <translation type="vanished">&amp;打开配置文件</translation>
    </message>
    <message>
        <source>&amp;Edit Profile</source>
        <translation type="vanished">&amp;编辑配置文件</translation>
    </message>
    <message>
        <source>&amp;New Profile</source>
        <translation type="vanished">&amp;新建配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="215"/>
        <source>Open Profile</source>
        <translation>打开配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="226"/>
        <source>Edit Profile</source>
        <translation>编辑配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="240"/>
        <source>New Profile</source>
        <translation>新建配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="258"/>
        <source>Profile: None</source>
        <translation>配置文件：无</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="278"/>
        <source>Please select or create a profile...</source>
        <translation>请选择或创建一个配置文件...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="328"/>
        <source>Destroy Profile</source>
        <translation>删除配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="93"/>
        <source>Profile</source>
        <translation>配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="94"/>
        <source>Ventilator Brand</source>
        <translation>呼吸机品牌</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="95"/>
        <source>Ventilator Model</source>
        <translation>呼吸机型号</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="96"/>
        <source>Other Data</source>
        <translation>其他参数</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="97"/>
        <source>Last Imported</source>
        <translation>最新导入</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="98"/>
        <source>Name</source>
        <translation>姓名</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="236"/>
        <location filename="../oscar/profileselector.cpp" line="370"/>
        <source>Enter Password for %1</source>
        <translation>键入 %1 的密码</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="252"/>
        <location filename="../oscar/profileselector.cpp" line="389"/>
        <source>You entered an incorrect password</source>
        <translation>密码不正确</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="255"/>
        <source>Forgot your password?</source>
        <translation>忘记密码？</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="255"/>
        <source>Ask on the forums how to reset it, it&apos;s actually pretty easy.</source>
        <translation>在论坛上询问如何重置，其实很简单。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="325"/>
        <source>Select a profile first</source>
        <translation>首先选择配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="392"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to either reset it or delete the profile folder manually.</source>
        <translation>如果由于忘记密码而试图删除配置文件，则需要重置密码或手动删除配置文件文件夹。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="402"/>
        <source>You are about to destroy profile &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</source>
        <translation>你将要删除配置文件 &apos;&lt;b&gt;%1&lt;/b&gt;&apos;。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="402"/>
        <source>Think carefully, as this will irretrievably delete the profile along with all &lt;b&gt;backup data&lt;/b&gt; stored under&lt;br/&gt;%2.</source>
        <translation>请注意：这将不可恢复地删除配置文件以及存储在 %2 下的所有备份数据。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="402"/>
        <source>Enter the word &lt;b&gt;DELETE&lt;/b&gt; below (exactly as shown) to confirm.</source>
        <translation>请输入如下图所示的单词 &lt;b&gt;DELETE&lt;/b&gt; 以确认。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="420"/>
        <source>DELETE</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="421"/>
        <source>Sorry</source>
        <translation>抱歉</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="421"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>您需要输入大写字母 DELETE。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="434"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>删除配置文件目录时出错，请手动移除。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="438"/>
        <source>Profile &apos;%1&apos; was successfully deleted</source>
        <translation>配置文件 &apos;%1&apos; 已成功删除</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="482"/>
        <location filename="../oscar/profileselector.cpp" line="522"/>
        <source>Hide disk usage information</source>
        <translation>隐藏磁盘使用信息</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="485"/>
        <source>Show disk usage information</source>
        <translation>显示磁盘使用信息</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="503"/>
        <source>Name: %1, %2</source>
        <translation>名字: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="506"/>
        <source>Phone: %1</source>
        <translation>电话号码: %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="509"/>
        <source>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>电子邮件: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="512"/>
        <source>Address:</source>
        <translation>地址:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="515"/>
        <source>No profile information given</source>
        <translation>未提供配置文件信息</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="518"/>
        <source>Profile: %1</source>
        <translation>配置文件: %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="448"/>
        <source>Bytes</source>
        <translation>字节</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="448"/>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="448"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="448"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="448"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="448"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="468"/>
        <source>Summaries:</source>
        <translation>摘要：</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="469"/>
        <source>Events:</source>
        <translation>事件：</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="470"/>
        <source>Backups:</source>
        <translation>备份：</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="173"/>
        <source>You must create a profile</source>
        <translation>您必须创建一个配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="36"/>
        <source>Reset filter to see all profiles</source>
        <translation>重置筛选以查看所有配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="360"/>
        <source>The selected profile does not appear to contain any data and cannot be removed by OSCAR</source>
        <translation>所选配置文件似乎不包含任何数据，无法被OSCAR删除</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="58"/>
        <source>Abort</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="779"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>A</source>
        <translation>未分类</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="777"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>H</source>
        <translation>低通气</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="791"/>
        <source>P</source>
        <translation>压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="804"/>
        <source>AI</source>
        <translation>呼吸暂停指数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="781"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>CA</source>
        <translation>中枢性</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="785"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="193"/>
        <source>EP</source>
        <translation>呼气压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="782"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>FL</source>
        <translation>气流受限</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="805"/>
        <source>HI</source>
        <translation>低通气指数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="817"/>
        <source>IE</source>
        <translation>呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="784"/>
        <source>LE</source>
        <translation>漏气率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="834"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="187"/>
        <source>LL</source>
        <translation>大量漏气</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="795"/>
        <source>O2</source>
        <translation>氧气</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="778"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>OA</source>
        <translation>阻塞性</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="793"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>NR</source>
        <translation>未响应事件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="813"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="162"/>
        <source>PB</source>
        <translation>周期性呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="796"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2850"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="221"/>
        <source>PC</source>
        <translation>混合面罩</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="214"/>
        <source>Compiler:</source>
        <translation>编译器：</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="690"/>
        <source>in</source>
        <translation>英寸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="694"/>
        <source>kg</source>
        <translation>千克</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="740"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Feeling</source>
        <translation>感觉</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="762"/>
        <source>EEPAP</source>
        <translation>呼气末正压</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="763"/>
        <source>Min EEPAP</source>
        <translation>最小呼气末正压</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="764"/>
        <source>Max EEPAP</source>
        <translation>最大呼气末正压</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="790"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2833"/>
        <source>PP</source>
        <translation>最高压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="801"/>
        <source>PS</source>
        <translation>压力支持</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="853"/>
        <source>Device</source>
        <translation>设备</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="880"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="194"/>
        <source>On</source>
        <translation>开启</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="792"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="176"/>
        <source>RE</source>
        <translation>呼吸作用</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="783"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="196"/>
        <source>SA</source>
        <translation>呼吸暂停</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>SD</source>
        <translation>SD</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="780"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="170"/>
        <source>UA</source>
        <translation>未知暂停</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="786"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <source>VS</source>
        <translation>鼾声指数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="691"/>
        <source>ft</source>
        <translation>英尺</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="692"/>
        <source>lb</source>
        <translation>磅</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="693"/>
        <source>oz</source>
        <translation>盎司</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="802"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="277"/>
        <source>AHI</source>
        <translation>呼吸暂停低通气指数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="771"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2847"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="126"/>
        <source>ASV</source>
        <translation>适应性支持通气模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="738"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>BMI</source>
        <translation>体重指数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="807"/>
        <source>CAI</source>
        <translation>中枢性暂停指数</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="73"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="422"/>
        <source>Apr</source>
        <translation>四月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="74"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="423"/>
        <source>Aug</source>
        <translation>八月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="224"/>
        <location filename="../oscar/SleepLib/common.cpp" line="892"/>
        <source>Avg</source>
        <translation>平均</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="862"/>
        <source>DOB</source>
        <translation>生日</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="811"/>
        <source>EPI</source>
        <translation>呼气压力指数</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="74"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="423"/>
        <source>Dec</source>
        <translation>十二月</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="808"/>
        <source>FLI</source>
        <translation>气流受限指数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="879"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>End</source>
        <translation>结束</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="73"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="422"/>
        <source>Feb</source>
        <translation>二月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="73"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="422"/>
        <source>Jan</source>
        <translation>一月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="74"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="423"/>
        <source>Jul</source>
        <translation>七月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="73"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="422"/>
        <source>Jun</source>
        <translation>六月</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="794"/>
        <source>NRI</source>
        <translation>未响应事件指数</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="73"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="422"/>
        <source>Mar</source>
        <translation>三月</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="887"/>
        <source>Max</source>
        <translation>最大值</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="73"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="422"/>
        <source>May</source>
        <translation>五月</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="888"/>
        <source>Med</source>
        <translation>中位数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="886"/>
        <source>Min</source>
        <translation>最小值</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="74"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="423"/>
        <source>Nov</source>
        <translation>十一月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="74"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="423"/>
        <source>Oct</source>
        <translation>十月</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="881"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="809"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="1018"/>
        <source>Off</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="803"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>RDI</source>
        <translation>呼吸紊乱指数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="810"/>
        <source>REI</source>
        <translation>呼吸作用指数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="806"/>
        <source>UAI</source>
        <translation>未知暂停指数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="797"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="798"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="799"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>UF3</source>
        <translation>UF3</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="74"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="423"/>
        <source>Sep</source>
        <translation>九月</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="788"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <source>VS2</source>
        <translation>鼾声指数2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="707"/>
        <source>bpm</source>
        <translation>次每分钟</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="770"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="120"/>
        <source>APAP</source>
        <translation>全自动正压通气</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="758"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="796"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2842"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="119"/>
        <source>CPAP</source>
        <translation>持续气道正压通气</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="765"/>
        <source>Min EPAP</source>
        <translation>呼气压力最小值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="761"/>
        <source>EPAP</source>
        <translation>呼气压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="867"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="768"/>
        <source>Min IPAP</source>
        <translation>吸气压力最小值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="767"/>
        <source>IPAP</source>
        <translation>吸气压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="877"/>
        <source>Last</source>
        <translation>最后一次</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="831"/>
        <source>Leak</source>
        <translation>漏气率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="848"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="791"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="793"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2837"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2839"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="115"/>
        <source>Mode</source>
        <translation>模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="861"/>
        <source>Name</source>
        <translation>姓名</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="873"/>
        <source>None</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="789"/>
        <source>RERA</source>
        <translation>呼吸努力相关觉醒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="820"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="265"/>
        <source>Resp. Event</source>
        <translation>呼吸事件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="857"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="293"/>
        <source>Inclination</source>
        <translation>侧卧角度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="136"/>
        <source>Therapy Pressure</source>
        <translation>治疗压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="759"/>
        <source>BiPAP</source>
        <translation>双水平气道正压通气</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="850"/>
        <source>Brand</source>
        <translation>品牌</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="746"/>
        <source>Daily</source>
        <translation>日常</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="865"/>
        <source>Email</source>
        <translation>电子邮件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="718"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2921"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="876"/>
        <source>First</source>
        <translation>第一次</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>Ramp Pressure</source>
        <translation>斜坡升压压力</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="377"/>
        <location filename="../oscar/Graphs/gUsageChart.cpp" line="45"/>
        <location filename="../oscar/SleepLib/common.cpp" line="696"/>
        <source>Hours</source>
        <translation>小时</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="832"/>
        <source>Leaks</source>
        <translation>漏气率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="849"/>
        <source>Model</source>
        <translation>型号</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="863"/>
        <source>Phone</source>
        <translation>电话号码</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="874"/>
        <source>Ready</source>
        <translation>就绪</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="222"/>
        <location filename="../oscar/SleepLib/common.cpp" line="893"/>
        <source>W-Avg</source>
        <translation>加权平均</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="830"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2804"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="240"/>
        <source>Snore</source>
        <translation>鼾声</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="878"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="327"/>
        <source>Start</source>
        <translation>开始</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="840"/>
        <source>Usage</source>
        <translation>使用情况</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="695"/>
        <source>cmH2O</source>
        <translation>厘米水柱</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>Pressure Support</source>
        <translation>压力支持</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="713"/>
        <source>ratio</source>
        <translation>比率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="828"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="237"/>
        <source>Tidal Volume</source>
        <translation>潮气量</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="443"/>
        <source>Entire Day</source>
        <translation>整天</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="209"/>
        <source>Heart rate in beats per minute</source>
        <translation>每分钟心跳次数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="824"/>
        <source>Pat. Trig. Breath</source>
        <translation>患者触发呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>Ramp Delay Period</source>
        <translation>斜坡延迟期间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="839"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="337"/>
        <source>Sleep Stage</source>
        <translation>睡眠阶段</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="827"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Minute Vent.</source>
        <translation>每分钟通气量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="196"/>
        <source>SensAwake feature will reduce pressure when waking is detected.</source>
        <translation>觉醒侦测功能会在侦测到醒来时降低压力.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="293"/>
        <source>Upright angle in degrees</source>
        <translation>垂直角度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="142"/>
        <source>Higher Expiratory Pressure</source>
        <translation>更高的呼气压力</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="295"/>
        <source>NRI=%1 LKI=%2 EPI=%3</source>
        <translation>未响应事件指数=%1 漏气指数=%2 呼气压力指数=%3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <source>A vibratory snore</source>
        <translation>振动鼾声</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Lower Inspiratory Pressure</source>
        <translation>更低的吸气压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="829"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Resp. Rate</source>
        <translation>呼吸频率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="818"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="262"/>
        <source>Insp. Time</source>
        <translation>吸气时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="819"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="259"/>
        <source>Exp. Time</source>
        <translation>呼气时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>A sudden (user definable) drop in blood oxygen saturation</source>
        <translation>血氧饱和度突然下降（用户可定义）</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="45"/>
        <source>There are no graphs visible to print</source>
        <translation>没有可打印的图表</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="826"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="271"/>
        <source>Target Vent.</source>
        <translation>目标通气量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="290"/>
        <source>Sleep position in degrees</source>
        <translation>睡眠位置角度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>Ramp Time</source>
        <translation>斜坡升压时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="836"/>
        <source>Unintentional Leaks</source>
        <translation>无意漏气量</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="60"/>
        <source>Would you like to show bookmarked areas in this report?</source>
        <translation>是否希望在报告中显示书签区域?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>Patient Triggered Breaths</source>
        <translation>患者触发的呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="379"/>
        <source>Events</source>
        <translation>事件</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="394"/>
        <source>(% %1 in events)</source>
        <translation>(% %1 事件)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="820"/>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1249"/>
        <source>No Data</source>
        <translation>无数据</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="568"/>
        <source>Page %1 of %2</source>
        <translation>第 %1 页，共 %2 页</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="891"/>
        <source>Median</source>
        <translation>中位数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="148"/>
        <source>PS Max</source>
        <translation>压力支持最大值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>PS Min</source>
        <translation>压力支持最小值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>End Expiratory Pressure</source>
        <translation>呼气末压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="160"/>
        <source>Cheyne Stokes Respiration (CSR)</source>
        <translation>潮式呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="162"/>
        <source>Periodic Breathing (PB)</source>
        <translation>周期性呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>Clear Airway (CA)</source>
        <translation>气道畅通性呼吸暂停 (CA)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>Obstructive Apnea (OA)</source>
        <translation>阻塞性睡眠呼吸暂停</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>Hypopnea (H)</source>
        <translation>低通气</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="170"/>
        <source>Unclassified Apnea (UA)</source>
        <translation>未分类呼吸暂停</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>Apnea (A)</source>
        <translation>呼吸暂停 (A)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>Flow Limitation (FL)</source>
        <translation>气流限制 (FL)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="176"/>
        <source>RERA (RE)</source>
        <translation>呼吸努力相关觉醒 (RE)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="176"/>
        <source>Respiratory Effort Related Arousal: A restriction in breathing that causes either awakening or sleep disturbance.</source>
        <translation>呼吸努力相关觉醒：呼吸受限导致的觉醒或睡眠中断。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <source>Vibratory Snore (VS)</source>
        <translation>振动鼾声 (VS)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>Leak Flag (LF)</source>
        <translation>漏气标志 (LF)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="187"/>
        <source>A large mask leak affecting device performance.</source>
        <translation>大规模面罩漏气影响设备性能。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="187"/>
        <source>Large Leak (LL)</source>
        <translation>大漏气 (LL)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>Non Responding Event (NR)</source>
        <translation>无响应事件 (NR)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="193"/>
        <source>Expiratory Puff (EP)</source>
        <translation>呼气喷射 (EP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="196"/>
        <source>SensAwake (SA)</source>
        <translation>感应觉醒 (SA)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <source>User Flag #1 (UF1)</source>
        <translation>用户标志#1 (UF1)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <source>User Flag #2 (UF2)</source>
        <translation>用户标志#2 (UF2)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>User Flag #3 (UF3)</source>
        <translation>用户标志#3 (UF3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="221"/>
        <source>Pulse Change (PC)</source>
        <translation>脉搏变化 (PC)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>SpO2 Drop (SD)</source>
        <translation>血氧下降 (SD)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="256"/>
        <source>I/E Value</source>
        <translation>吸气/呼气值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="268"/>
        <source>Flow Limit.</source>
        <translation>气流限制。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="277"/>
        <source>Apnea Hypopnea Index (AHI)</source>
        <translation>呼吸暂停低通气指数 (AHI)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="280"/>
        <source>Detected mask leakage including natural Mask leakages</source>
        <translation>检测到的面罩漏气，包括自然漏气</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="743"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="215"/>
        <source>Plethy</source>
        <translation>体积描记</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="823"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1024"/>
        <source>SensAwake</source>
        <translation>感应觉醒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="773"/>
        <source>ST/ASV</source>
        <translation>自发/定时 ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="283"/>
        <source>Median Leaks</source>
        <translation>漏气中值</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="137"/>
        <source>%1 Report</source>
        <translation>%1 报告</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="842"/>
        <source>Pr. Relief</source>
        <translation>压力释放</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="851"/>
        <source>Serial</source>
        <translation>序列号</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="250"/>
        <source>AHI	%1
</source>
        <translation>呼吸暂停低通气指数（AHI） %1
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="739"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Weight</source>
        <translation>体重</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="858"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="290"/>
        <source>Orientation</source>
        <translation>方向</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="752"/>
        <source>Event Flags</source>
        <translation>事件标志</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="844"/>
        <source>Bookmarks</source>
        <translation>书签</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>An apnea where the airway is open</source>
        <translation>气道开放情况下的呼吸暂停</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="821"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="268"/>
        <source>Flow Limitation</source>
        <translation>气流限制</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="248"/>
        <source>RDI	%1
</source>
        <translation>呼吸紊乱指数（RDI） %1
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="838"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="228"/>
        <source>Flow Rate</source>
        <translation>气流速率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="259"/>
        <source>Time taken to breathe out</source>
        <translation>呼气时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="215"/>
        <source>An optical Photo-plethysomogram showing heart rhythm</source>
        <translation>显示心率的光电体积描记图</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="237"/>
        <source>Amount of air displaced per breath</source>
        <translation>每次呼吸的气量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>Pat. Trig. Breaths</source>
        <translation>患者触发的呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="864"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Leak Rate</source>
        <translation>漏气率</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="366"/>
        <source>Reporting from %1 to %2</source>
        <translation>报告时间段从 %1 到 %2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="137"/>
        <source>Inspiratory Pressure</source>
        <translation>吸气压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2832"/>
        <source>A pulse of pressure &apos;pinged&apos; to detect a closed airway.</source>
        <translation>通过压力脉冲&apos;ping&apos;来检测气道是否闭合。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="283"/>
        <source>Median Leak Rate</source>
        <translation>漏气率中值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Rate of breaths per minute</source>
        <translation>每分钟呼吸次数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="240"/>
        <source>Graph displaying snore volume</source>
        <translation>显示鼾声音量的图表</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="766"/>
        <source>Max EPAP</source>
        <translation>最大呼气正压 (EPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="769"/>
        <source>Max IPAP</source>
        <translation>最大吸气正压 (IPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="869"/>
        <source>Bedtime</source>
        <translation>就寝时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="744"/>
        <source>Pressure</source>
        <translation>压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="890"/>
        <source>Average</source>
        <translation>平均</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="271"/>
        <source>Target Minute Ventilation</source>
        <translation>目标分钟通气量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Amount of air displaced per minute</source>
        <translation>每分钟的换气量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>Percentage of breaths triggered by patient</source>
        <translation>患者触发的呼吸占比</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="154"/>
        <source>Your %1 %2 (%3) generated data that OSCAR has never seen before.</source>
        <translation>您的 %1 %2 (%3) 生成了OSCAR前所未见的数据。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="155"/>
        <source>The imported data may not be entirely accurate, so the developers would like a .zip copy of this device&apos;s SD card and matching clinician .pdf reports to make sure OSCAR is handling the data correctly.</source>
        <translation>导入的数据可能不完全准确，因此开发人员希望获得该设备SD卡的.zip副本以及相应的临床.pdf报告，以确保OSCAR正确处理数据。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="164"/>
        <source>Non Data Capable Device</source>
        <translation>无数据功能的设备</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="165"/>
        <source>Your %1 CPAP Device (Model %2) is unfortunately not a data capable model.</source>
        <translation>很遗憾，您的 %1 CPAP设备 (型号 %2) 不是数据功能型号。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="166"/>
        <source>I&apos;m sorry to report that OSCAR can only track hours of use and very basic settings for this device.</source>
        <translation>很抱歉，OSCAR只能追踪该设备的使用时间和非常基本的设置。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="178"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="491"/>
        <source>Device Untested</source>
        <translation>设备未经测试</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="179"/>
        <source>Your %1 CPAP Device (Model %2) has not been tested yet.</source>
        <translation>您的 %1 CPAP设备 (型号 %2) 尚未经过测试。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="180"/>
        <source>It seems similar enough to other devices that it might work, but the developers would like a .zip copy of this device&apos;s SD card and matching clinician .pdf reports to make sure it works with OSCAR.</source>
        <translation>它看起来与其他设备相似，可能会工作，但开发人员希望获得该设备SD卡的.zip副本以及相应的临床.pdf报告，以确保它与OSCAR兼容。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="188"/>
        <source>Device Unsupported</source>
        <translation>设备不受支持</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="189"/>
        <source>Sorry, your %1 CPAP Device (%2) is not supported yet.</source>
        <translation>抱歉，您的 %1 CPAP设备 (%2) 目前不受支持。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="190"/>
        <source>The developers need a .zip copy of this device&apos;s SD card and matching clinician .pdf reports to make it work with OSCAR.</source>
        <translation>开发人员需要该设备SD卡的.zip副本以及相应的临床.pdf报告，以使其与OSCAR兼容。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="215"/>
        <source>Plethysomogram</source>
        <translation>体积描记图</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>Starting Ramp Pressure</source>
        <translation>起始坡道压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="193"/>
        <source>Intellipap event where you breathe out your mouth.</source>
        <translation>Intellipap检测到的口呼吸事件。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="822"/>
        <source>Flow Limit</source>
        <translation>气流限制</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="293"/>
        <source>UAI=%1 </source>
        <translation>未知暂停指数=%1 </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="741"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="209"/>
        <source>Pulse Rate</source>
        <translation>脉搏率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="277"/>
        <source>Graph showing running AHI for the past hour</source>
        <translation>图表显示过去一小时的AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>Graph showing running RDI for the past hour</source>
        <translation>图表显示过去一小时的RDI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="871"/>
        <source>Mask Time</source>
        <translation>面罩时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="854"/>
        <source>Channel</source>
        <translation>通道</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="274"/>
        <source>Max Leaks</source>
        <translation>最大漏气</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="289"/>
        <source>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</source>
        <translation>呼吸事件指数=%1 鼾声指数=%2 气流受限指数=%3 周期性呼吸/潮湿呼吸=%4%%</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="283"/>
        <source>Median rate of detected mask leakage</source>
        <translation>检测到的面罩漏气中值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="231"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="234"/>
        <source>Mask Pressure</source>
        <translation>面罩压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="265"/>
        <source>Respiratory Event</source>
        <translation>呼吸事件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>A type of respiratory event that won&apos;t respond to a pressure increase.</source>
        <translation>不会响应压力增加的呼吸事件。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="37"/>
        <source>Windows User</source>
        <translation>Windows 用户</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="717"/>
        <source>Question</source>
        <translation>问题</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="139"/>
        <source>Higher Inspiratory Pressure</source>
        <translation>更高的吸气压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="760"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2845"/>
        <source>Bi-Level</source>
        <translation>双水平</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="872"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="131"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="208"/>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="126"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="377"/>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="380"/>
        <source>Duration</source>
        <translation>持续时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="841"/>
        <source>Sessions</source>
        <translation>会话</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="855"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="748"/>
        <source>Overview</source>
        <translation>概述</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="431"/>
        <source>Entire Day&apos;s Flow Waveform</source>
        <translation>全天气流波形</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="558"/>
        <location filename="../oscar/main.cpp" line="611"/>
        <location filename="../oscar/main.cpp" line="626"/>
        <source>Exiting</source>
        <translation>正在退出</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="148"/>
        <source>Pressure Support Maximum</source>
        <translation>压力支持最大值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="268"/>
        <source>Graph showing severity of flow limitations</source>
        <translation>显示气流限制严重程度的图表</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="195"/>
        <source>: %1 hours, %2 minutes, %3 seconds
</source>
        <translation>：%1 小时, %2 分钟, %3 秒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>A partially obstructed airway</source>
        <translation>气道部分阻塞</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>Pressure Support Minimum</source>
        <translation>压力支持最小值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="833"/>
        <source>Large Leak</source>
        <translation>大量漏气</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="870"/>
        <source>Wake-up</source>
        <translation>觉醒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="719"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="894"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="895"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="896"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>Min Pressure</source>
        <translation>最小压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="280"/>
        <source>Total Leak Rate</source>
        <translation>总漏气率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>Max Pressure</source>
        <translation>最大压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="837"/>
        <source>MaskPressure</source>
        <translation>面罩压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="835"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="280"/>
        <source>Total Leaks</source>
        <translation>总漏气量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Minute Ventilation</source>
        <translation>分钟通气率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Rate of detected mask leakage</source>
        <translation>面罩漏气率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="228"/>
        <source>Breathing flow rate waveform</source>
        <translation>呼吸流量波形</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="141"/>
        <source>Lower Expiratory Pressure</source>
        <translation>更低的呼气压力</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="283"/>
        <source>AI=%1 HI=%2 CAI=%3 </source>
        <translation>暂停指数=%1 低通气指数=%2 中枢性暂停指数=%3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="262"/>
        <source>Time taken to breathe in</source>
        <translation>吸气时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>Maximum Therapy Pressure</source>
        <translation>最大治疗压力</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="433"/>
        <source>Current Selection</source>
        <translation>当前选择</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="212"/>
        <source>Blood-oxygen saturation percentage</source>
        <translation>血氧饱和百分比</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="262"/>
        <source>Inspiratory Time</source>
        <translation>吸气时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Respiratory Rate</source>
        <translation>呼吸频率</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="104"/>
        <source>Printing %1 Report</source>
        <translation>正在打印 %1 报告</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="259"/>
        <source>Expiratory Time</source>
        <translation>呼气时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="274"/>
        <source>Maximum Leak</source>
        <translation>最大漏气量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="256"/>
        <source>Ratio between Inspiratory and Expiratory time</source>
        <translation>吸气和呼气时间的比率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>Minimum Therapy Pressure</source>
        <translation>最小治疗压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="221"/>
        <source>A sudden (user definable) change in heart rate</source>
        <translation>心率突变</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="749"/>
        <source>Oximetry</source>
        <translation>血氧测定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="751"/>
        <source>Oximeter</source>
        <translation>血氧仪</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="274"/>
        <source>The maximum rate of mask leakage</source>
        <translation>面罩的最大漏气率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>Expiratory Pressure</source>
        <translation>呼气压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="825"/>
        <source>Tgt. Min. Vent</source>
        <translation>目标最小通气量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2831"/>
        <source>Pressure Pulse</source>
        <translation>压力脉冲</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="775"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2906"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2933"/>
        <source>Humidifier</source>
        <translation>加湿器</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="866"/>
        <source>Patient ID</source>
        <translation>患者编号</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>An apnea caused by airway obstruction</source>
        <translation>气道阻塞状态下的呼吸暂停</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1008"/>
        <source>Days: %1</source>
        <translation>天数：%1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1121"/>
        <source>(Sess: %1)</source>
        <translation>（会话：%1）</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1129"/>
        <source>Bedtime: %1</source>
        <translation>睡眠时间：%1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1131"/>
        <source>Waketime: %1</source>
        <translation>觉醒时间：%1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="697"/>
        <source>Minutes</source>
        <translation>分钟</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="698"/>
        <source>Seconds</source>
        <translation>秒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="699"/>
        <source>milliSeconds</source>
        <translation>毫秒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="704"/>
        <source>Events/hr</source>
        <translation>事件/小时</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="706"/>
        <source>Hz</source>
        <translation>赫兹</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="708"/>
        <source>l/min</source>
        <translation>升/分钟</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="709"/>
        <source>Litres</source>
        <translation>升</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="710"/>
        <source>ml</source>
        <translation>毫升</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="711"/>
        <source>Breaths/min</source>
        <translation>呼吸次数/分钟</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="715"/>
        <source>Degrees</source>
        <translation>度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="720"/>
        <source>Information</source>
        <translation>消息</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="721"/>
        <source>Busy</source>
        <translation>忙</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="722"/>
        <source>Please Note</source>
        <translation>请注意</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="725"/>
        <source>Only Settings and Compliance Data Available</source>
        <translation>只有设置和合规数据可用</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="727"/>
        <source>Summary Data Only</source>
        <translation>只有摘要数据</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="732"/>
        <source>&amp;Yes</source>
        <translation>&amp;是</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="733"/>
        <source>&amp;No</source>
        <translation>&amp;否</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="734"/>
        <source>&amp;Cancel</source>
        <translation>&amp;取消</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="735"/>
        <source>&amp;Destroy</source>
        <translation>&amp;删除</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="736"/>
        <source>&amp;Save</source>
        <translation>&amp;保存</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="724"/>
        <source>No Data Available</source>
        <translation>无可用数据</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="485"/>
        <source>Launching Windows Explorer failed</source>
        <translation>启动Windows资源管理器失败</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="486"/>
        <source>Could not find explorer.exe in path to launch Windows Explorer.</source>
        <translation>未找到explorer.exe，无法启动Windows资源管理器。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="559"/>
        <source>Important:</source>
        <translation>重要提示：</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="577"/>
        <source>This folder currently resides at the following location:</source>
        <translation>此文件夹当前位于以下位置：</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="585"/>
        <source>Rebuilding from %1 Backup</source>
        <translation>正在从 %1 备份重建</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <source>Vibratory Snore (VS2) </source>
        <translation>振动鼾声 (VS2)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <source>A vibratory snore as detected by a System One device</source>
        <translation>由System One设备检测到的振动鼾声</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="299"/>
        <source>Mask On Time</source>
        <translation>面罩佩戴时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="299"/>
        <source>Time started according to str.edf</source>
        <translation>计时时间依据str.edf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="302"/>
        <source>Summary Only</source>
        <translation>仅有摘要信息</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="572"/>
        <source>Are you sure you want to use this folder?</source>
        <translation>您确定要使用这个文件夹吗？</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="458"/>
        <source>There is a lockfile already present for this profile &apos;%1&apos;, claimed on &apos;%2&apos;.</source>
        <translation>此配置文件已存在锁文件 &apos;%1&apos;，创建于 &apos;%2&apos;。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="714"/>
        <source>Severity (0-1)</source>
        <translation>严重程度 (0-1)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="310"/>
        <source>Fixed Bi-Level</source>
        <translation>固定双水平</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="311"/>
        <source>Auto Bi-Level (Fixed PS)</source>
        <translation>自动双水平（固定压力支持）</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="375"/>
        <source>(last night)</source>
        <translation>（昨晚）</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="41"/>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="43"/>
        <source>Contec</source>
        <translation>Contec</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="41"/>
        <source>CMS50</source>
        <translation>CMS50</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="78"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="88"/>
        <source>Fisher &amp; Paykel</source>
        <translation>Fisher &amp; Paykel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="78"/>
        <source>ICON</source>
        <translation>ICON</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="78"/>
        <source>DeVilbiss</source>
        <translation>DeVilbiss</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="78"/>
        <source>Intellipap</source>
        <translation>Intellipap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="41"/>
        <source>ChoiceMMed</source>
        <translation>ChoiceMMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="41"/>
        <source>MD300</source>
        <translation>MD300</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="70"/>
        <source>Respironics</source>
        <translation>Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="70"/>
        <source>M-Series</source>
        <translation>M-Series</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="187"/>
        <source>Philips Respironics</source>
        <translation>Philips Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="187"/>
        <source>System One</source>
        <translation>System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="92"/>
        <source>ResMed</source>
        <translation>ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="93"/>
        <source>S9</source>
        <translation>S9</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="39"/>
        <source>Somnopose</source>
        <translation>Somnopose</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="39"/>
        <source>Somnopose Software</source>
        <translation>Somnopose Software</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="40"/>
        <source>Zeo</source>
        <translation>Zeo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="40"/>
        <source>Personal Sleep Coach</source>
        <translation>个人睡眠教练</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="154"/>
        <source>Ramp Event</source>
        <translation>斜坡启动事件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="211"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1041"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="154"/>
        <source>Ramp</source>
        <translation>斜坡启动</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="249"/>
        <source>Database Outdated
Please Rebuild CPAP Data</source>
        <translation>数据库过期
请重建呼吸机数据</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="852"/>
        <source>Series</source>
        <translation>系列</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="883"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="884"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="312"/>
        <source>Auto Bi-Level (Variable PS)</source>
        <translation>全自动双水平(可变压力支持)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1590"/>
        <source>Fixed %1 (%2)</source>
        <translation>固定 %1 (%2)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1593"/>
        <source>Min %1 Max %2 (%3)</source>
        <translation>最小 %1 最大 %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1597"/>
        <source>EPAP %1 IPAP %2 (%3)</source>
        <translation>呼气压力 %1 吸气压力 %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1601"/>
        <source>PS %1 over %2-%3 (%4)</source>
        <translation>压力支持 %1 超过 %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1606"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1620"/>
        <source>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</source>
        <translation>最小呼气压力 %1 最大吸气压力 %2 压力支持 %3-%4 (%5)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1611"/>
        <source>Min EEPAP %1 Max EEPAP %2 PDIFF %3-%4 (%5)</source>
        <translation>最小呼气终末正压 %1 最大呼气终末正压 %2 压力差 %3-%4 (%5)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1637"/>
        <source>EPAP %1-%2 IPAP %3-%4 (%5)</source>
        <translation>呼气压力 %1-%2 吸气压力 %3-%4 (%5)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2786"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2788"/>
        <source>SmartFlex Mode</source>
        <translation>SmartFlex模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2787"/>
        <source>Intellipap pressure relief mode.</source>
        <translation>Intellipap压力释放模式。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2793"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="137"/>
        <source>Ramp Only</source>
        <translation>仅斜坡</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2794"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="138"/>
        <source>Full Time</source>
        <translation>全时</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2797"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2799"/>
        <source>SmartFlex Level</source>
        <translation>SmartFlex 级别</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2798"/>
        <source>Intellipap pressure relief level.</source>
        <translation>Intellipap压力释放级别。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="86"/>
        <source>SmartFlex Settings</source>
        <translation>SmartFlex设置</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2963"/>
        <source>15mm</source>
        <translation>15mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2962"/>
        <source>22mm</source>
        <translation>22mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2855"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2857"/>
        <source>Flex Mode</source>
        <translation>Flex模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2856"/>
        <source>PRS1 pressure relief mode.</source>
        <translation>PRS1压力释放模式。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2860"/>
        <source>C-Flex</source>
        <translation>C-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2861"/>
        <source>C-Flex+</source>
        <translation>C-Flex+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2862"/>
        <source>A-Flex</source>
        <translation>A-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2864"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2888"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2890"/>
        <source>Rise Time</source>
        <translation>上升时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2865"/>
        <source>Bi-Flex</source>
        <translation>Bi-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2871"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2873"/>
        <source>Flex Level</source>
        <translation>Flex级别</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2872"/>
        <source>PRS1 pressure relief setting.</source>
        <translation>PRS1压力释放设置。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2904"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="155"/>
        <source>Humidifier Status</source>
        <translation>加湿器状态</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2905"/>
        <source>PRS1 humidifier connected?</source>
        <translation>PRS1加湿器已连接?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2908"/>
        <source>Disconnected</source>
        <translation>断开连接</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2909"/>
        <source>Connected</source>
        <translation>已连接</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2958"/>
        <source>Hose Diameter</source>
        <translation>软管直径</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2959"/>
        <source>Diameter of primary CPAP hose</source>
        <translation>主要呼吸机软管直径</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2986"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2988"/>
        <source>Auto On</source>
        <translation>自动开启</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2995"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2997"/>
        <source>Auto Off</source>
        <translation>自动关闭</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3004"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3006"/>
        <source>Mask Alert</source>
        <translation>面罩警报</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3013"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3015"/>
        <source>Show AHI</source>
        <translation>显示AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3088"/>
        <source>Timed Breath</source>
        <translation>定时呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="1008"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3089"/>
        <source>Machine Initiated Breath</source>
        <translation>机器触发呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="1009"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3090"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="134"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1030"/>
        <source>EPR</source>
        <translation>EPR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="134"/>
        <source>ResMed Exhale Pressure Relief</source>
        <translation>瑞思迈呼气压力释放</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="139"/>
        <source>Patient???</source>
        <translation>病人???</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="142"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1036"/>
        <source>EPR Level</source>
        <translation>呼气压力释放级别</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="142"/>
        <source>Exhale Pressure Relief Level</source>
        <translation>呼气压力释放级别</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="126"/>
        <location filename="../oscar/SleepLib/loader_plugins/resvent_loader.h" line="67"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="98"/>
        <source>EPR: </source>
        <translation>呼气压力释放: </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="118"/>
        <source>Weinmann</source>
        <translation>Weinmann</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="118"/>
        <source>SOMNOsoft2</source>
        <translation>SOMNOsoft2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>Pressure Min</source>
        <translation>最小压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>Pressure Max</source>
        <translation>最大压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>An apnea reported by your CPAP device.</source>
        <translation>由您的呼吸机报告的呼吸暂停。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>LF</source>
        <translation>漏气</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="302"/>
        <source>CPAP Session contains summary data only</source>
        <translation>呼吸机会话仅包含摘要数据</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="792"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2838"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="305"/>
        <source>PAP Mode</source>
        <translation>正压通气模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="305"/>
        <source>PAP Device Mode</source>
        <translation>正压通气设备模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="313"/>
        <source>ASV (Fixed EPAP)</source>
        <translation>ASV (固定呼气压力)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="314"/>
        <source>ASV (Variable EPAP)</source>
        <translation>ASV (可变呼气压力)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1247"/>
        <source>Are you sure you want to reset all your channel colors and settings to defaults?</source>
        <translation>确定将所有通道颜色和设置恢复为默认值吗？</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1257"/>
        <source>Are you sure you want to reset all your oximetry settings to defaults?</source>
        <translation>确定将所有血氧测量设置恢复为默认值吗？</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1138"/>
        <source>Duration %1:%2:%3</source>
        <translation>时长 %1:%2:%3</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1139"/>
        <source>AHI %1</source>
        <translation>AHI %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2442"/>
        <source>Hide All Events</source>
        <translation>隐藏所有事件</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2443"/>
        <source>Show All Events</source>
        <translation>显示所有事件</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2784"/>
        <source>Unpin %1 Graph</source>
        <translation>解除固定 %1 图表</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2863"/>
        <source>Pin %1 Graph</source>
        <translation>固定 %1 图表</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="809"/>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1055"/>
        <source>Plots Disabled</source>
        <translation>禁用区块</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1243"/>
        <source>(Summary Only)</source>
        <translation>（仅摘要）</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="155"/>
        <source>Relief: %1</source>
        <translation>压力释放：%1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="161"/>
        <source>Hours: %1h, %2m, %3s</source>
        <translation>时长：%1小时，%2分钟，%3秒</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="264"/>
        <source>Machine Information</source>
        <translation>设备信息</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="726"/>
        <source>Graphs Switched Off</source>
        <translation>图表已关闭</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="728"/>
        <source>Sessions Switched Off</source>
        <translation>会话已关闭</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="29"/>
        <source>Journal Data</source>
        <translation>日志数据</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="60"/>
        <source>If your old data is missing, copy the contents of all the other Journal_XXXXXXX folders to this one manually.</source>
        <translation>如果旧数据丢失，请手动将所有其他 Journal_XXXXXXX 文件夹的内容复制到此文件夹。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="43"/>
        <source>CMS50F3.7</source>
        <translation>CMS50F3.7</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="43"/>
        <source>CMS50F</source>
        <translation>CMS50F</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="218"/>
        <source>Perfusion Index</source>
        <translation>灌注指数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="218"/>
        <source>A relative assessment of the pulse strength at the monitoring site</source>
        <translation>监测部位脉搏强度的相对评估</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="218"/>
        <source>Perf. Index %</source>
        <translation>灌注指数 %</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>Respiratory Disturbance Index (RDI)</source>
        <translation>呼吸紊乱指数（RDI）</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="309"/>
        <source>APAP (Variable)</source>
        <translation>APAP（自动）</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>How you feel (1 = like crap, 10 = unstoppable)</source>
        <translation>体感（1 = 糟糕，10 = 停不下来）</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="785"/>
        <source>Zero</source>
        <translation>零</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="788"/>
        <source>Upper Threshold</source>
        <translation>上限阈值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="791"/>
        <source>Lower Threshold</source>
        <translation>下限阈值</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="429"/>
        <source>Snapshot %1</source>
        <translation>快照 %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="196"/>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="206"/>
        <source>Selection Length</source>
        <translation>选择长度</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="430"/>
        <source> (%2 min, %3 sec)</source>
        <translation>（%2 分钟，%3 秒）</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="432"/>
        <source> (%3 sec)</source>
        <translation>（%3 秒）</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="220"/>
        <source>Med.</source>
        <translation>中位数</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="238"/>
        <source>Min: %1</source>
        <translation>最小：%1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="269"/>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="279"/>
        <source>Min: </source>
        <translation>最小: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="274"/>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="284"/>
        <source>Max: </source>
        <translation>最大: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="303"/>
        <source>Max: %1</source>
        <translation>最大：%1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="309"/>
        <source>%1 (%2 days): </source>
        <translation>%1 (%2 天): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="311"/>
        <source>%1 (%2 day): </source>
        <translation>%1 (%2 天): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="371"/>
        <source>% in %1</source>
        <translation>% 在 %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="383"/>
        <source>Min %1</source>
        <translation>最小 %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gUsageChart.cpp" line="30"/>
        <source>
Length: %1</source>
        <translation>
长度: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gUsageChart.cpp" line="96"/>
        <source>%1 low usage, %2 no usage, out of %3 days (%4% compliant.) Length: %5 / %6 / %7</source>
        <translation>%1 很少使用, %2 不使用, 超过 %3 天 (%4% 符合). 长度: %5 / %6 / %7</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="102"/>
        <source>Sessions: %1 / %2 / %3 Length: %4 / %5 / %6 Longest: %7 / %8 / %9</source>
        <translation>会话: %1 / %2 / %3 长度: %4 / %5 / %6 最长: %7 / %8 / %9</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="222"/>
        <source>%1
Length: %3
Start: %2
</source>
        <translation>%1
长度: %3
开始: %2
</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="224"/>
        <source>Mask On</source>
        <translation>面罩开启</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="224"/>
        <source>Mask Off</source>
        <translation>面罩关闭</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="235"/>
        <source>%1
Length: %3
Start: %2</source>
        <translation>%1
长度: %3
开始: %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gTTIAChart.cpp" line="71"/>
        <source>TTIA:</source>
        <translation>呼吸暂停总时间:</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gTTIAChart.cpp" line="83"/>
        <source>
TTIA: %1</source>
        <translation>
呼吸暂停总时间: %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="280"/>
        <source>CMS50D+</source>
        <translation>CMS50D+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="280"/>
        <source>CMS50E/F</source>
        <translation>CMS50E/F</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="115"/>
        <source>CPAP Mode</source>
        <translation>CPAP模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="125"/>
        <source>VPAPauto</source>
        <translation>VPAP全自动</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="127"/>
        <source>ASVAuto</source>
        <translation>ASV全自动</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="130"/>
        <source>Auto for Her</source>
        <translation>女士专用自动模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="812"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="160"/>
        <source>CSR</source>
        <translation>CSR</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="708"/>
        <source>It is likely that doing this will cause data corruption, are you sure you want to do this?</source>
        <translation>此操作可能会导致数据损坏，确定继续吗?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1336"/>
        <source>Are you sure you want to reset all your waveform channel colors and settings to defaults?</source>
        <translation>确定将所有波形通道颜色和设置重置为默认值吗?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="755"/>
        <source>Default</source>
        <translation>默认</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="772"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="315"/>
        <source>AVAPS</source>
        <translation>AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="160"/>
        <source>An abnormal period of Cheyne Stokes Respiration</source>
        <translation>潮式呼吸的不正常时期</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="162"/>
        <source>An abnormal period of Periodic Breathing</source>
        <translation>周期性呼吸的不正常时期</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="149"/>
        <source>SmartStart</source>
        <translation>自启动</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="149"/>
        <source>Smart Start</source>
        <translation>自启动</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="155"/>
        <source>Humid. Status</source>
        <translation>湿化器状态</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="155"/>
        <source>Humidifier Enabled Status</source>
        <translation>湿化器已启用状态</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2935"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="161"/>
        <source>Humid. Level</source>
        <translation>湿度等级</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="161"/>
        <source>Humidity Level</source>
        <translation>湿度等级</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="174"/>
        <source>Temperature</source>
        <translation>温度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="174"/>
        <source>ClimateLine Temperature</source>
        <translation>加热管路温度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="178"/>
        <source>Temp. Enable</source>
        <translation>温度启用</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="178"/>
        <source>ClimateLine Temperature Enable</source>
        <translation>加热管路温度启用</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="178"/>
        <source>Temperature Enable</source>
        <translation>温度启用</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="185"/>
        <source>AB Filter</source>
        <translation>抗菌过滤器</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="185"/>
        <source>Antibacterial Filter</source>
        <translation>抗菌过滤器</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="191"/>
        <source>Pt. Access</source>
        <translation>患者访问</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="197"/>
        <source>Climate Control</source>
        <translation>恒温控制</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="200"/>
        <source>Manual</source>
        <translation>手动</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="239"/>
        <source>RiseEnable</source>
        <translation>上升启用</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="244"/>
        <source>RiseTime</source>
        <translation>上升时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="247"/>
        <source>Cycle</source>
        <translation>循环</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="255"/>
        <source>Trigger</source>
        <translation>触发</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="263"/>
        <source>TiMax</source>
        <translation>最大吸气时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="267"/>
        <source>TiMin</source>
        <translation>最小吸气时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="492"/>
        <source>Your ResMed CPAP device (Model %1) has not been tested yet.</source>
        <translation>您的ResMed CPAP设备（型号 %1）尚未测试。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="493"/>
        <source>It seems similar enough to other devices that it might work, but the developers would like a .zip copy of this device&apos;s SD card to make sure it works with OSCAR.</source>
        <translation>它看起来与其他设备足够相似，可能可以正常工作，但开发人员希望获取该设备SD卡的.zip副本，以确保其与OSCAR兼容。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="882"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2946"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3037"/>
        <source>Auto</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="203"/>
        <source>Mask</source>
        <translation>面罩</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="203"/>
        <source>ResMed Mask Setting</source>
        <translation>ResMed面罩设置</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="205"/>
        <source>Pillows</source>
        <translation>鼻枕</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="206"/>
        <source>Full Face</source>
        <translation>全脸面罩</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="207"/>
        <source>Nasal</source>
        <translation>鼻面罩</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="211"/>
        <source>Ramp Enable</source>
        <translation>斜坡启用</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="700"/>
        <source>h</source>
        <translation>小时</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="701"/>
        <source>m</source>
        <translation>分钟</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="702"/>
        <source>s</source>
        <translation>秒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="703"/>
        <source>ms</source>
        <translation>毫秒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Height</source>
        <translation>身高</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Physical Height</source>
        <translation>身高</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Notes</source>
        <translation>备注</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Bookmark Notes</source>
        <translation>标记备注</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Body Mass Index</source>
        <translation>体重指数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="327"/>
        <source>Bookmark Start</source>
        <translation>标记开始</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Bookmark End</source>
        <translation>标记结束</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="329"/>
        <source>Last Updated</source>
        <translation>最后更新</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="330"/>
        <source>Journal Notes</source>
        <translation>日志备注</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="330"/>
        <source>Journal</source>
        <translation>日志</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="337"/>
        <source>1=Awake 2=REM 3=Light Sleep 4=Deep Sleep</source>
        <translation>1=清醒 2=快速眼动 3=浅睡眠 4=深睡眠</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Brain Wave</source>
        <translation>脑电波</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>BrainWave</source>
        <translation>脑电波</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="340"/>
        <source>Awakenings</source>
        <translation>觉醒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="340"/>
        <source>Number of Awakenings</source>
        <translation>觉醒次数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="341"/>
        <source>Morning Feel</source>
        <translation>晨起感觉</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="341"/>
        <source>How you felt in the morning</source>
        <translation>早上醒来的感觉</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="342"/>
        <source>Time Awake</source>
        <translation>清醒时长</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="342"/>
        <source>Time spent awake</source>
        <translation>清醒时长</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Time In REM Sleep</source>
        <translation>快速眼动时长</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Time spent in REM Sleep</source>
        <translation>快速眼动时长</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Time in REM Sleep</source>
        <translation>快速眼动时长</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="344"/>
        <source>Time In Light Sleep</source>
        <translation>浅睡眠时长</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="344"/>
        <source>Time spent in light sleep</source>
        <translation>浅睡眠时长</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="344"/>
        <source>Time in Light Sleep</source>
        <translation>浅睡眠时长</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="345"/>
        <source>Time In Deep Sleep</source>
        <translation>深睡眠时长</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="345"/>
        <source>Time spent in deep sleep</source>
        <translation>深睡眠时长</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="345"/>
        <source>Time in Deep Sleep</source>
        <translation>深睡眠时长</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="346"/>
        <source>Time to Sleep</source>
        <translation>入睡时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="346"/>
        <source>Time taken to get to sleep</source>
        <translation>入睡时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="347"/>
        <source>Zeo ZQ</source>
        <translation>Zeo 睡眠质量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="347"/>
        <source>Zeo sleep quality measurement</source>
        <translation>Zeo 睡眠质量测量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="347"/>
        <source>ZEO ZQ</source>
        <translation>ZEP睡商</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="552"/>
        <source>Pop out Graph</source>
        <translation>弹出图表</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1621"/>
        <source>Your machine doesn&apos;t record data to graph in Daily View</source>
        <translation>您的设备没有记录数据，无法在每日视图中生成图表</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2786"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2861"/>
        <source>Popout %1 Graph</source>
        <translation>弹出图表 %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="688"/>
        <source> m</source>
        <translation> m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="689"/>
        <source> cm</source>
        <translation> cm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="747"/>
        <source>Profile</source>
        <translation>配置文件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2694"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="540"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="873"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="225"/>
        <source>Getting Ready...</source>
        <translation>准备就绪...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="552"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="898"/>
        <source>Scanning Files...</source>
        <translation>扫描文件...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="645"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="906"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="780"/>
        <location filename="../oscar/mainwindow.cpp" line="2291"/>
        <source>Importing Sessions...</source>
        <translation>导入会话...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="795"/>
        <source>UNKNOWN</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="797"/>
        <source>APAP (std)</source>
        <translation>APAP（标准）</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="798"/>
        <source>APAP (dyn)</source>
        <translation>APAP（动态）</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="799"/>
        <source>Auto S</source>
        <translation>自动 S</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="800"/>
        <source>Auto S/T</source>
        <translation>自动 S/T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="801"/>
        <source>AcSV</source>
        <translation>AcSV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="805"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="807"/>
        <source>SoftPAP Mode</source>
        <translation>SoftPAP 模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="806"/>
        <source>Pressure relief during exhalation</source>
        <translation>呼气期间的压力缓解</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="810"/>
        <source>Slight</source>
        <translation>轻微</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="815"/>
        <source>Softstart pressure</source>
        <translation>柔性启动压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="816"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="853"/>
        <source>Pressure during soft start period</source>
        <translation>柔性启动期间的压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="817"/>
        <source>PSoft</source>
        <translation>柔性启动压力(PSoft)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="822"/>
        <source>Softstart minimum pressure</source>
        <translation>柔性启动最小压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="823"/>
        <source>Minimum pressure during soft start period</source>
        <translation>柔性启动期间的最小压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="824"/>
        <source>PSoftMin</source>
        <translation>柔性启动最小压力(PSoftMin)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="829"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="831"/>
        <source>Auto start</source>
        <translation>自动启动</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="830"/>
        <source>Automatically turn on the device by breathing</source>
        <translation>通过呼吸自动启动设备</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="838"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="840"/>
        <source>Softstart time</source>
        <translation>柔性启动时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="839"/>
        <source>Length of soft start period</source>
        <translation>柔性启动期间的长度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="845"/>
        <source>Soft start maximum time</source>
        <translation>柔性启动最大时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="846"/>
        <source>Maximum length of soft start period</source>
        <translation>柔性启动期间的最大长度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="847"/>
        <source>Soft start max. time</source>
        <translation>柔性启动最大时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="852"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="854"/>
        <source>Soft start pressure</source>
        <translation>柔性启动压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="874"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Higher End Expiratory Pressure</source>
        <translation>高端呼气压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="880"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="881"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="882"/>
        <source>Humidifier level</source>
        <translation>加湿器等级</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="887"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="888"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="889"/>
        <source>Tube type</source>
        <translation>管类型</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="904"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="906"/>
        <source>Obstruction level</source>
        <translation>阻塞水平</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="905"/>
        <source>Obstruction level in percentage</source>
        <translation>阻塞水平（百分比）</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="913"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="915"/>
        <source>rRMVFluctuation</source>
        <translation>rRMV波动</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="914"/>
        <source>Relative respiratory minute volume fluctuation</source>
        <translation>相对呼吸分钟量波动</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="923"/>
        <source>Relative respiratory minute volume</source>
        <translation>相对呼吸分钟量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="929"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="930"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="931"/>
        <source>Measured pressure</source>
        <translation>测量压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="936"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="937"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="938"/>
        <source>Full flow</source>
        <translation>全流量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="952"/>
        <source>Artefact</source>
        <translation>伪影</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="953"/>
        <source>Irregularity in measured data, that doesn&apos;t represents a breathing event (e.g swallowing, coughing, or speaking)</source>
        <translation>测量数据中的不规则现象，不代表呼吸事件（例如吞咽、咳嗽或说话）</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="991"/>
        <source>Epoch (2 mins) with Flow Limitation</source>
        <translation>有气流限制的2分钟时期</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="998"/>
        <source>Deep Sleep</source>
        <translation>深度睡眠</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="999"/>
        <source>Deep sleep, stable respiration</source>
        <translation>深度睡眠，呼吸稳定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="1007"/>
        <source>Timed breath</source>
        <translation>定时呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="1014"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="1015"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="1016"/>
        <source>BiSoft Mode</source>
        <translation>BiSoft 模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="1019"/>
        <source>BiSoft 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="1020"/>
        <source>BiSoft 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="1021"/>
        <source>TriLevel</source>
        <translation>三水平</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="859"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="860"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="861"/>
        <source>PMaxOA</source>
        <translation>最大阻塞性呼吸暂停压力(PMaxOA)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="866"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="868"/>
        <source>EEPAPMin</source>
        <translation>呼气末正压最小值(EEPAPMin)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="867"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>Lower End Expiratory Pressure</source>
        <translation>低端呼气压力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="873"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="875"/>
        <source>EEPAPMax</source>
        <translation>呼气末正压最大值(EEPAPMax)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="922"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="924"/>
        <source>rRMV</source>
        <translation>相对呼吸分钟量(rRMV)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="954"/>
        <source>ART</source>
        <translation>伪影</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="959"/>
        <source>CriticalLeak</source>
        <translation>关键泄漏</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="960"/>
        <source>Mask leakage is above a critical threshold</source>
        <translation>面罩泄漏超过关键阈值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="961"/>
        <source>CL</source>
        <translation>关键泄漏(CL)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="966"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="968"/>
        <source>eMO</source>
        <translation>轻度阻塞时期(eMO)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="967"/>
        <source>Epoch (2 mins) with Mild Obstruction</source>
        <translation>有轻度阻塞的2分钟时期</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="974"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="976"/>
        <source>eSO</source>
        <translation>严重阻塞时期(eSO)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="975"/>
        <source>Epoch (2 mins) with Severe Obstruction</source>
        <translation>有严重阻塞的2分钟时期</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="982"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="984"/>
        <source>eS</source>
        <translation>打鼾时期(eS)/translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="983"/>
        <source>Epoch (2 mins) with Snoring</source>
        <translation>有打鼾的2分钟时期</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="990"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="992"/>
        <source>eFL</source>
        <translation>气流限制时期(eFL)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="1000"/>
        <source>DS</source>
        <translation>深度睡眠(DS)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2748"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="368"/>
        <location filename="../oscar/mainwindow.cpp" line="705"/>
        <location filename="../oscar/mainwindow.cpp" line="2313"/>
        <source>Finishing up...</source>
        <translation>整理中...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3081"/>
        <source>Breathing Not Detected</source>
        <translation>呼吸未被检测到</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3083"/>
        <source>BND</source>
        <translation>BND</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="584"/>
        <source>Locating STR.edf File(s)...</source>
        <translation>正在查找STR.edf文件...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="745"/>
        <source>Cataloguing EDF Files...</source>
        <translation>正在给EDF文件编目录...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="764"/>
        <source>Queueing Import Tasks...</source>
        <translation>正在排队导入任务...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="793"/>
        <source>Finishing Up...</source>
        <translation>整理中...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="702"/>
        <source>Loading %1 data for %2...</source>
        <translation>正在为 %2 加载 %1 数据...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="713"/>
        <source>Scanning Files</source>
        <translation>正在扫描文件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="747"/>
        <source>Migrating Summary File Location</source>
        <translation>正在迁移摘要文件位置</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="917"/>
        <source>Loading Summaries.xml.gz</source>
        <translation>加载摘要.xml.gz文件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="1049"/>
        <source>Loading Summary Data</source>
        <translation>正在加载摘要数据</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="15"/>
        <source>Please Wait...</source>
        <translation>请稍候...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="472"/>
        <source>Loading profile &quot;%1&quot;...</source>
        <translation>正在加载配置文件 &quot;%1&quot;...</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="374"/>
        <source>Most recent Oximetry data: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </source>
        <translation>最新血氧测定数据：&lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="382"/>
        <source>No oximetry data has been imported yet.</source>
        <translation>尚未导入血氧测定数据。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="684"/>
        <source>Software Engine</source>
        <translation>软件引擎</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="685"/>
        <source>ANGLE / OpenGLES</source>
        <translation>ANGLE / OpenGLES</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="686"/>
        <source>Desktop OpenGL</source>
        <translation>桌面OpenGL</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1624"/>
        <source>There is no data to graph</source>
        <translation>没有数据可供生成图表</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="47"/>
        <source>OSCAR found an old Journal folder, but it looks like it&apos;s been renamed:</source>
        <translation>OSCAR找到旧的日志文件夹，但看起来它已被重命名：</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="49"/>
        <source>OSCAR will not touch this folder, and will create a new one instead.</source>
        <translation>OSCAR不会更改此文件夹，而是会创建一个新文件夹。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="50"/>
        <source>Please be careful when playing in OSCAR&apos;s profile folders :-P</source>
        <translation>请谨慎在OSCAR配置文件夹中操作:-P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="57"/>
        <source>For some reason, OSCAR couldn&apos;t find a journal object record in your profile, but did find multiple Journal data folders.

</source>
        <translation>出于某种原因，OSCAR在您的配置文件中找不到日志对象记录，但找到了多个日志数据文件夹。

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="58"/>
        <source>OSCAR picked only the first one of these, and will use it in future:

</source>
        <translation>OSCAR将会使用其中的第一个，并将在未来使用它：

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="551"/>
        <source>&lt;b&gt;OSCAR maintains a backup of your devices data card that it uses for this purpose.&lt;/b&gt;</source>
        <translation>&lt;b&gt;OSCAR保留了设备数据卡的备份&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="552"/>
        <source>&lt;i&gt;Your old device data should be regenerated provided this backup feature has not been disabled in preferences during a previous data import.&lt;/i&gt;</source>
        <translation>&lt;i&gt;如果在之前的数据导入过程中未在首选项中禁用此备份功能，则应重新生成您的旧设备数据。&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="555"/>
        <source>OSCAR does not yet have any automatic card backups stored for this device.</source>
        <translation>OSCAR尚未为此设备存储任何自动备份。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="556"/>
        <source>This means you will need to import this device data again afterwards from your own backups or data card.</source>
        <translation>这意味着您需要稍后从自己的备份或数据卡中再次导入此设备数据。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="560"/>
        <source>If you are concerned, click No to exit, and backup your profile manually, before starting OSCAR again.</source>
        <translation>如果您担心，请点击“否”退出，并在再次启动OSCAR之前手动备份您的配置文件。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="561"/>
        <source>Are you ready to upgrade, so you can run the new version of OSCAR?</source>
        <translation>准备升级，以便运行新版本的OSCAR？</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="564"/>
        <source>Device Database Changes</source>
        <translation>设备数据库更改</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="575"/>
        <source>Sorry, the purge operation failed, which means this version of OSCAR can&apos;t start.</source>
        <translation>抱歉，清除操作失败，这意味着此版本的OSCAR无法启动。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="576"/>
        <source>The device data folder needs to be removed manually.</source>
        <translation>需要手动删除设备数据文件夹。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="594"/>
        <source>Would you like to switch on automatic backups, so next time a new version of OSCAR needs to do so, it can rebuild from these?</source>
        <translation>是否要打开自动备份，以便下次OSCAR新版本需要时，可以从这些备份中重建？</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="601"/>
        <source>OSCAR will now start the import wizard so you can reinstall your %1 data.</source>
        <translation>OSCAR现在将启动导入向导，以便您可以重新安装%1数据。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="611"/>
        <source>OSCAR will now exit, then (attempt to) launch your computers file manager so you can manually back your profile up:</source>
        <translation>OSCAR现在将退出，然后（尝试）启动您的计算机文件管理器，以便您可以手动备份配置文件：</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="613"/>
        <source>Use your file manager to make a copy of your profile directory, then afterwards, restart OSCAR and complete the upgrade process.</source>
        <translation>使用文件管理器复制配置文件目录，然后重新启动OSCAR并完成升级过程。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>A user definable event detected by OSCAR&apos;s flow waveform processor.</source>
        <translation>由OSCAR的流量波形处理器检测到的用户自定义事件。</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="559"/>
        <source>As you did not select a data folder, OSCAR will exit.</source>
        <translation>由于您未选择数据文件夹，OSCAR将退出。</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="571"/>
        <source>The folder you chose is not empty, nor does it already contain valid OSCAR data.</source>
        <translation>您选择的文件夹不是空的，也不包含有效的OSCAR数据。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="273"/>
        <source>OSCAR Reminder</source>
        <translation>OSCAR提醒</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="273"/>
        <source>Don&apos;t forget to place your datacard back in your CPAP device</source>
        <translation>别忘了将数据卡放回您的CPAP设备</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="459"/>
        <source>You can only work with one instance of an individual OSCAR profile at a time.</source>
        <translation>一次只能处理单个OSCAR配置文件的一个实例。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="460"/>
        <source>If you are using cloud storage, make sure OSCAR is closed and syncing has completed first on the other computer before proceeding.</source>
        <translation>如果您使用云存储，请确保OSCAR已关闭，并且在继续操作之前已在另一台计算机上完成同步。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="203"/>
        <source>You must run the OSCAR Migration Tool</source>
        <translation>您必须运行OSCAR迁移工具</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="202"/>
        <source>Using </source>
        <translation>使用</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="202"/>
        <source>, found SleepyHead -
</source>
        <translation>, 找到 SleepyHead -
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="170"/>
        <source>An apnea that couldn&apos;t be determined as Central or Obstructive.</source>
        <translation>无法确定为中枢性或阻塞性的呼吸暂停。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>A restriction in breathing from normal, causing a flattening of the flow waveform.</source>
        <translation>正常呼吸的限制，导致流量波形变平。</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="199"/>
        <source>or CANCEL to skip migration.</source>
        <translation>或取消以跳过迁移。</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="214"/>
        <source>You cannot use this folder:</source>
        <translation>您不能使用此文件夹：</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="229"/>
        <source>Migrating </source>
        <translation>正在迁移</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="229"/>
        <source> files</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="230"/>
        <source>from </source>
        <translation>从</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="230"/>
        <source>to </source>
        <translation>到</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="545"/>
        <source>OSCAR will set up a folder for your data.</source>
        <translation>OSCAR将为您的数据设置一个文件夹。</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="548"/>
        <source>We suggest you use this folder: </source>
        <translation>我们建议您使用此文件夹：</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="549"/>
        <source>Click Ok to accept this, or No if you want to use a different folder.</source>
        <translation>单击确定接受此选项，如果您想使用其他文件夹，请单击否。</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="555"/>
        <source>Choose or create a new folder for OSCAR data</source>
        <translation>选择或创建一个新的OSCAR数据文件夹</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="560"/>
        <source>Next time you run OSCAR, you will be asked again.</source>
        <translation>下次运行OSCAR时，系统会再次询问您。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="221"/>
        <source>App key:</source>
        <translation>应用密钥：</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="209"/>
        <source>Operating system:</source>
        <translation>操作系统：</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="210"/>
        <source>Graphics Engine:</source>
        <translation>图形引擎：</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="211"/>
        <source>Graphics Engine type:</source>
        <translation>图形引擎类型：</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="592"/>
        <source>Data directory:</source>
        <translation>数据目录：</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="283"/>
        <source>Permissive Mode</source>
        <translation>宽容模式</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="286"/>
        <source>Total disabled sessions: %1, found in %2 days</source>
        <translation>总禁用会话：%1，发现于%2天</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="288"/>
        <source>Total disabled sessions: %1</source>
        <translation>总禁用会话：%1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="291"/>
        <source>Duration of longest disabled session: %1 minutes, Total duration of all disabled sessions: %2 minutes.</source>
        <translation>最长禁用会话时长：%1分钟，所有禁用会话的总时长：%2分钟。</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="294"/>
        <source>The reporting period is %1 days between %2 and %3</source>
        <translation>报告期为%1天，介于%2和%3之间</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="304"/>
        <source>Updating Statistics cache</source>
        <translation>更新统计缓存</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="908"/>
        <source>Usage Statistics</source>
        <translation>使用统计</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1097"/>
        <source>Everything</source>
        <translation>所有</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1771"/>
        <source>d MMM yyyy [ %1 - %2 ]</source>
        <translation>d MMM yyyy [ %1 - %2 ]</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1616"/>
        <source>EPAP %1 PS %2-%3 (%4)</source>
        <translation>EPAP %1 PS %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>Pressure Set</source>
        <translation>压力设置</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>Pressure Setting</source>
        <translation>压力设置</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>IPAP Set</source>
        <translation>IPAP设置</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>IPAP Setting</source>
        <translation>IPAP设置</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>EPAP Set</source>
        <translation>EPAP设置</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>EPAP Setting</source>
        <translation>EPAP设置</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="752"/>
        <source>Loading summaries</source>
        <translation>正在加载摘要</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="207"/>
        <source>Built with Qt %1 on %2</source>
        <translation>基于Qt %1构建于%2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="859"/>
        <source>Motion</source>
        <translation>运动</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1573"/>
        <source>n/a</source>
        <translation>不可用</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/dreem_loader.h" line="37"/>
        <source>Dreem</source>
        <translation>Dreem</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="153"/>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="92"/>
        <source>Untested Data</source>
        <translation>未测试的数据</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2863"/>
        <source>P-Flex</source>
        <translation>P-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2913"/>
        <source>Humidification Mode</source>
        <translation>加湿模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2914"/>
        <source>PRS1 Humidification Mode</source>
        <translation>PRS1加湿模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2915"/>
        <source>Humid. Mode</source>
        <translation>加湿模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2917"/>
        <source>Fixed (Classic)</source>
        <translation>固定（经典）</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2918"/>
        <source>Adaptive (System One)</source>
        <translation>自适应（System One）</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2919"/>
        <source>Heated Tube</source>
        <translation>加热管</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2925"/>
        <source>Tube Temperature</source>
        <translation>管道温度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2926"/>
        <source>PRS1 Heated Tube Temperature</source>
        <translation>PRS1加热管温度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2927"/>
        <source>Tube Temp.</source>
        <translation>管道温度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2934"/>
        <source>PRS1 Humidifier Setting</source>
        <translation>PRS1加湿器设置</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2964"/>
        <source>12mm</source>
        <translation>12毫米</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="93"/>
        <source>Your Viatom device generated data that OSCAR has never seen before.</source>
        <translation>您的Viatom设备生成的数据是OSCAR从未见过的。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="94"/>
        <source>The imported data may not be entirely accurate, so the developers would like a copy of your Viatom files to make sure OSCAR is handling the data correctly.</source>
        <translation>导入的数据可能不完全准确，因此开发人员希望获得您的Viatom文件副本，以确保OSCAR正确处理数据。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="40"/>
        <source>Viatom</source>
        <translation>Viatom</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="40"/>
        <source>Viatom Software</source>
        <translation>Viatom软件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="538"/>
        <source>OSCAR %1 needs to upgrade its database for %2 %3 %4</source>
        <translation>OSCAR %1需要升级其%2 %3 %4的数据库</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="296"/>
        <source>Movement</source>
        <translation>运动</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="296"/>
        <source>Movement detector</source>
        <translation>运动检测器</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="695"/>
        <source>Version &quot;%1&quot; is invalid, cannot continue!</source>
        <translation>版本“%1”无效，无法继续！</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="704"/>
        <source>The version of OSCAR you are running (%1) is OLDER than the one used to create this data (%2).</source>
        <translation>您运行的OSCAR版本(%1)比创建此数据时使用的版本(%2)旧。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2682"/>
        <source>Please select a location for your zip other than the data card itself!</source>
        <translation>请选择一个位置存放压缩文件，而不是数据卡本身！</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2729"/>
        <location filename="../oscar/mainwindow.cpp" line="2779"/>
        <location filename="../oscar/mainwindow.cpp" line="2838"/>
        <source>Unable to create zip!</source>
        <translation>无法创建压缩文件！</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1262"/>
        <source>Parsing STR.edf records...</source>
        <translation>解析STR.edf记录...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="234"/>
        <source>Mask Pressure (High frequency)</source>
        <translation>面罩压力（高频）</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="265"/>
        <source>A ResMed data item: Trigger Cycle Event</source>
        <translation>ResMed数据项：触发循环事件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="543"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="889"/>
        <source>Backing Up Files...</source>
        <translation>正在备份文件...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="358"/>
        <source>Debugging channel #1</source>
        <translation>调试通道#1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="358"/>
        <source>Test #1</source>
        <translation>测试#1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="359"/>
        <source>Debugging channel #2</source>
        <translation>调试通道#2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="359"/>
        <source>Test #2</source>
        <translation>测试#2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1631"/>
        <source>EPAP %1 IPAP %2-%3 (%4)</source>
        <translation>呼气压力 %1 吸气压力 %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2841"/>
        <source>CPAP-Check</source>
        <translation>CPAP-检查</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2843"/>
        <source>AutoCPAP</source>
        <translation>自动CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2844"/>
        <source>Auto-Trial</source>
        <translation>自动试用</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2846"/>
        <source>AutoBiLevel</source>
        <translation>自动双水平</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2848"/>
        <source>S</source>
        <translation>模式S</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2849"/>
        <source>S/T</source>
        <translation>模式S/T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2851"/>
        <source>S/T - AVAPS</source>
        <translation>模式S/T - AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2852"/>
        <source>PC - AVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2879"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2881"/>
        <source>Flex Lock</source>
        <translation>灵活锁定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2880"/>
        <source>Whether Flex settings are available to you.</source>
        <translation>是否可以使用Flex设置。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2889"/>
        <source>Amount of time it takes to transition from EPAP to IPAP, the higher the number the slower the transition</source>
        <translation>从EPAP过渡到IPAP所需的时间，数字越大，过渡越慢</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2895"/>
        <source>Rise Time Lock</source>
        <translation>上升时间锁定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2896"/>
        <source>Whether Rise Time settings are available to you.</source>
        <translation>是否可以使用上升时间设置。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2897"/>
        <source>Rise Lock</source>
        <translation>上升锁定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2950"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2951"/>
        <source>Mask Resistance Setting</source>
        <translation>面罩阻力设置</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2952"/>
        <source>Mask Resist.</source>
        <translation>面罩阻力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2960"/>
        <source>Hose Diam.</source>
        <translation>软管直径</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2968"/>
        <source>Tubing Type Lock</source>
        <translation>管类型锁定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2969"/>
        <source>Whether tubing type settings are available to you.</source>
        <translation>是否可以使用管类型设置。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2970"/>
        <source>Tube Lock</source>
        <translation>管锁定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2977"/>
        <source>Mask Resistance Lock</source>
        <translation>面罩阻力锁定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2978"/>
        <source>Whether mask resistance settings are available to you.</source>
        <translation>是否可以使用面罩阻力设置。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2979"/>
        <source>Mask Res. Lock</source>
        <translation>面罩阻力锁定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3022"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3024"/>
        <source>Ramp Type</source>
        <translation>坡道类型</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3023"/>
        <source>Type of ramp curve to use.</source>
        <translation>使用的坡道曲线类型。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3026"/>
        <source>Linear</source>
        <translation>线性</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3027"/>
        <source>SmartRamp</source>
        <translation>智能坡道</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3028"/>
        <source>Ramp+</source>
        <translation>增强坡道</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3032"/>
        <source>Backup Breath Mode</source>
        <translation>备用呼吸模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3033"/>
        <source>The kind of backup breath rate in use: none (off), automatic, or fixed</source>
        <translation>使用的备用呼吸率类型：无（关闭）、自动或固定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3034"/>
        <source>Breath Rate</source>
        <translation>呼吸率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3038"/>
        <source>Fixed</source>
        <translation>固定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3042"/>
        <source>Fixed Backup Breath BPM</source>
        <translation>固定备用呼吸BPM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3043"/>
        <source>Minimum breaths per minute (BPM) below which a timed breath will be initiated</source>
        <translation>每分钟（BPM）最小呼吸次数，低于此值将启动定时呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3044"/>
        <source>Breath BPM</source>
        <translation>呼吸BPM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3049"/>
        <source>Timed Inspiration</source>
        <translation>定时吸气</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3050"/>
        <source>The time that a timed breath will provide IPAP before transitioning to EPAP</source>
        <translation>定时呼吸提供IPAP到过渡到EPAP的时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3051"/>
        <source>Timed Insp.</source>
        <translation>定时吸气</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3056"/>
        <source>Auto-Trial Duration</source>
        <translation>自动试用期</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3058"/>
        <source>Auto-Trial Dur.</source>
        <translation>自动试用期</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3063"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3065"/>
        <source>EZ-Start</source>
        <translation>EZ-启动</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3064"/>
        <source>Whether or not EZ-Start is enabled</source>
        <translation>是否启用EZ-启动</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3072"/>
        <source>Variable Breathing</source>
        <translation>可变呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3073"/>
        <source>UNCONFIRMED: Possibly variable breathing, which are periods of high deviation from the peak inspiratory flow trend</source>
        <translation>未经确认：可能是可变呼吸，即从峰值吸气流趋势高度偏离的时期</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="559"/>
        <source>Once you upgrade, you &lt;font size=+1&gt;cannot&lt;/font&gt; use this profile with the previous version anymore.</source>
        <translation>一旦升级，您将无法再使用此配置文件与先前版本。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2920"/>
        <source>Passover</source>
        <translation>旁通</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2987"/>
        <source>A few breaths automatically starts device</source>
        <translation>几次呼吸自动启动设备</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2996"/>
        <source>Device automatically switches off</source>
        <translation>设备自动关闭</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3005"/>
        <source>Whether or not device allows Mask checking.</source>
        <translation>设备是否允许面罩检查。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3014"/>
        <source>Whether or not device shows AHI via built-in display.</source>
        <translation>设备是否通过内置显示屏显示AHI。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3057"/>
        <source>The number of days in the Auto-CPAP trial period, after which the device will revert to CPAP</source>
        <translation>Auto-CPAP试用期的天数，之后设备将恢复到CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3082"/>
        <source>A period during a session where the device could not detect flow.</source>
        <translation>会话期间设备无法检测到流量的时间段。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3096"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3098"/>
        <source>Peak Flow</source>
        <translation>峰流量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3097"/>
        <source>Peak flow during a 2-minute interval</source>
        <translation>2分钟间隔内的峰流量</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2095"/>
        <source>Recompressing Session Files</source>
        <translation>重新压缩会话文件</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="385"/>
        <source>OSCAR crashed due to an incompatibility with your graphics hardware.</source>
        <translation>OSCAR因与您的图形硬件不兼容而崩溃。</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="386"/>
        <source>To resolve this, OSCAR has reverted to a slower but more compatible method of drawing.</source>
        <translation>为解决此问题，OSCAR已恢复到一种较慢但更兼容的绘图方法。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="123"/>
        <source>Couldn&apos;t parse Channels.xml, OSCAR cannot continue and is exiting.</source>
        <translation>无法解析Channels.xml，OSCAR无法继续并正在退出。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="376"/>
        <source>(1 day ago)</source>
        <translation>（1天前）</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="377"/>
        <source>(%2 days ago)</source>
        <translation>（%2天前）</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="152"/>
        <source>New versions file improperly formed</source>
        <translation>新版本文件格式不正确</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="175"/>
        <source>A more recent version of OSCAR is available</source>
        <translation>有可用的更新版本OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="170"/>
        <source>release</source>
        <translation>发布</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="170"/>
        <source>test version</source>
        <translation>测试版</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="171"/>
        <source>You are running the latest %1 of OSCAR</source>
        <translation>您正在运行最新的%1版OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="172"/>
        <location filename="../oscar/checkupdates.cpp" line="176"/>
        <source>You are running OSCAR %1</source>
        <translation>您正在运行OSCAR %1</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="178"/>
        <source>OSCAR %1 is available &lt;a href=&apos;%2&apos;&gt;here&lt;/a&gt;.</source>
        <translation>OSCAR %1 可在 &lt;a href=&apos;%2&apos;&gt;这里&lt;/a&gt; 获取。</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="181"/>
        <source>Information about more recent test version %1 is available at &lt;a href=&apos;%2&apos;&gt;%2&lt;/a&gt;</source>
        <translation>关于最新测试版%1的信息，请访问 &lt;a href=&apos;%2&apos;&gt;%2&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="209"/>
        <source>Check for OSCAR Updates</source>
        <translation>检查OSCAR更新</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="612"/>
        <source>Unable to create the OSCAR data folder at</source>
        <translation>无法在以下位置创建OSCAR数据文件夹</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="634"/>
        <source>The popout window is full. You should capture the existing
popout window, delete it, then pop out this graph again.</source>
        <translation>弹出窗口已满。您应捕获现有的弹出窗口，删除它，然后再次弹出此图表。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="191"/>
        <source>Essentials</source>
        <translation>基本设置</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="193"/>
        <source>Plus</source>
        <translation>增强</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="622"/>
        <source>Unable to write to OSCAR data directory</source>
        <translation>无法写入OSCAR数据目录</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="623"/>
        <source>Error code</source>
        <translation>错误代码</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="624"/>
        <source>OSCAR cannot continue and is exiting.</source>
        <translation>OSCAR无法继续并正在退出。</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="635"/>
        <source>Unable to write to debug log. You can still use the debug pane (Help/Troubleshooting/Show Debug Pane) but the debug log will not be written to disk.</source>
        <translation>无法写入调试日志。您仍然可以使用调试窗格（帮助/故障排除/显示调试窗格），但调试日志将不会写入磁盘。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="358"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="359"/>
        <source>For internal use only</source>
        <translation>仅供内部使用</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="198"/>
        <source>Choose the SleepyHead or OSCAR data folder to migrate</source>
        <translation>选择要迁移的SleepyHead或OSCAR数据文件夹</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="213"/>
        <source>The folder you chose does not contain valid SleepyHead or OSCAR data.</source>
        <translation>您选择的文件夹不包含有效的SleepyHead或OSCAR数据。</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="546"/>
        <source>If you have been using SleepyHead or an older version of OSCAR,</source>
        <translation>如果您一直在使用SleepyHead或较旧版本的OSCAR，</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="547"/>
        <source>OSCAR can copy your old data to this folder later.</source>
        <translation>OSCAR稍后可以将您的旧数据复制到此文件夹。</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="601"/>
        <source>Migrate SleepyHead or OSCAR Data?</source>
        <translation>迁移SleepyHead或OSCAR数据？</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="602"/>
        <source>On the next screen OSCAR will ask you to select a folder with SleepyHead or OSCAR data</source>
        <translation>在下一个屏幕上，OSCAR将要求您选择一个包含SleepyHead或OSCAR数据的文件夹</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="603"/>
        <source>Click [OK] to go to the next screen or [No] if you do not wish to use any SleepyHead or OSCAR data.</source>
        <translation>点击[确定]进入下一个屏幕，或点击[否]如果您不希望使用任何SleepyHead或OSCAR数据。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="919"/>
        <source>Chromebook file system detected, but no removable device found
</source>
        <translation>检测到Chromebook文件系统，但未找到可移动设备</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="920"/>
        <source>You must share your SD card with Linux using the ChromeOS Files program</source>
        <translation>您必须使用ChromeOS文件程序将您的SD卡与Linux共享</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2867"/>
        <source>Flex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="276"/>
        <source>Unable to check for updates. Please try again later.</source>
        <translation>无法检查更新。请稍后再试。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2941"/>
        <source>Target Time</source>
        <translation>目标时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2942"/>
        <source>PRS1 Humidifier Target Time</source>
        <translation>PRS1加湿器目标时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2943"/>
        <source>Hum. Tgt Time</source>
        <translation>加湿器目标时间</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1539"/>
        <source>varies</source>
        <translation>变化</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2717"/>
        <source>Backing up files...</source>
        <translation>正在备份文件...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2724"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="249"/>
        <source>Reading data files...</source>
        <translation>正在读取数据文件...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2805"/>
        <source>Snoring event.</source>
        <translation>打鼾事件。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2806"/>
        <source>SN</source>
        <translation>打鼾</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="964"/>
        <source>model %1</source>
        <translation>型号 %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="967"/>
        <source>unknown model</source>
        <translation>未知型号</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="128"/>
        <source>iVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="221"/>
        <source>Soft</source>
        <translation>柔和</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="811"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="220"/>
        <source>Standard</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="121"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="124"/>
        <source>BiPAP-T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="122"/>
        <source>BiPAP-S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="123"/>
        <source>BiPAP-S/T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="129"/>
        <source>PAC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="149"/>
        <source>Device auto starts by breathing</source>
        <translation>设备通过呼吸自动启动</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="224"/>
        <source>SmartStop</source>
        <translation>智能停止</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="224"/>
        <source>Smart Stop</source>
        <translation>智能停止</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="224"/>
        <source>Device auto stops by breathing</source>
        <translation>设备通过呼吸自动停止</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="233"/>
        <source>Simple</source>
        <translation>简单</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="232"/>
        <source>Advanced</source>
        <translation>高级</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1048"/>
        <source>Humidity</source>
        <translation>湿度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="88"/>
        <source>SleepStyle</source>
        <translation>睡眠风格</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="298"/>
        <source>AI=%1 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1022"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1023"/>
        <source>SensAwake level</source>
        <translation>SensAwake 级别</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1030"/>
        <source>Expiratory Relief</source>
        <translation>呼气缓解</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1036"/>
        <source>Expiratory Relief Level</source>
        <translation>呼气缓解级别</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="218"/>
        <source>Response</source>
        <translation>响应</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="230"/>
        <source>Patient View</source>
        <translation>患者视图</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="80"/>
        <source>This page in other languages:</source>
        <translation>此页面的其他语言版本：</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2602"/>
        <location filename="../oscar/overview.cpp" line="471"/>
        <source>%1 Graphs</source>
        <translation>%1 图表</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2606"/>
        <location filename="../oscar/overview.cpp" line="475"/>
        <source>%1 of %2 Graphs</source>
        <translation>%2 个图表中的 %1 个</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2633"/>
        <source>%1 Event Types</source>
        <translation>%1 事件类型</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2637"/>
        <source>%1 of %2 Event Types</source>
        <translation>%2 种事件类型中的 %1 种</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.h" line="232"/>
        <source>Löwenstein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.h" line="232"/>
        <source>Prisma Smart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resvent_loader.h" line="58"/>
        <source>Resvent/Hoffrichter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resvent_loader.h" line="58"/>
        <source>iBreeze/Point3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/highresolution.cpp" line="71"/>
        <source>High Resolution Mode change will take effect when OSCAR is restarted.</source>
        <translation>高分辨率模式更改将在重新启动 OSCAR 后生效。</translation>
    </message>
    <message>
        <location filename="../oscar/highresolution.cpp" line="78"/>
        <source>High Resolution Mode is Enabled (Experimental)</source>
        <translation>高分辨率模式已启用（实验性）</translation>
    </message>
    <message>
        <location filename="../oscar/highresolution.cpp" line="80"/>
        <source>The High Resolution Mode will be Enabled after Oscar is restarted.</source>
        <translation>重新启动 OSCAR 后将启用高分辨率模式。</translation>
    </message>
    <message>
        <location filename="../oscar/highresolution.cpp" line="85"/>
        <source>High Resolution Mode is Disabled (Experimental)</source>
        <translation>高分辨率模式已禁用（实验性）</translation>
    </message>
    <message>
        <location filename="../oscar/highresolution.cpp" line="87"/>
        <source>High Resolution Mode will be Disabled after Oscar is restarted.</source>
        <translation>重新启动 OSCAR 后将禁用高分辨率模式。</translation>
    </message>
</context>
<context>
    <name>SaveGraphLayoutSettings</name>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="169"/>
        <source>Manage Save Layout Settings</source>
        <translation>管理保存布局设置</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="178"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="179"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="341"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="178"/>
        <source>Add Feature inhibited. The maximum number of Items has been exceeded.</source>
        <translation>添加功能被禁用。项目数量已超过最大限制。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="179"/>
        <source>creates new copy of current settings.</source>
        <translation>创建当前设置的新副本。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="180"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="356"/>
        <source>Restore</source>
        <translation>恢复</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="180"/>
        <source>Restores saved settings from selection.</source>
        <translation>从选择中恢复已保存的设置。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="181"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="360"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="181"/>
        <source>Renames the selection. Must edit existing name then press enter.</source>
        <translation>重命名所选项目。必须编辑现有名称，然后按 Enter 键。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="182"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="364"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="182"/>
        <source>Updates the selection with current settings.</source>
        <translation>使用当前设置更新所选项目。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="183"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="370"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="183"/>
        <source>Deletes the selection.</source>
        <translation>删除所选项目。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="184"/>
        <source>Expanded Help menu.</source>
        <translation>扩展帮助菜单。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="227"/>
        <source>Exits the dialog menu.</source>
        <translation>退出对话菜单。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="331"/>
        <source>This feature manages the saving and restoring of Layout Settings.</source>
        <translation>此功能管理布局设置的保存和恢复。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="333"/>
        <source>Layout Settings control the layout of a graph or chart.</source>
        <translation>布局设置控制图表的布局。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="335"/>
        <source>Different Layouts Settings can be saved and later restored.</source>
        <translation>不同的布局设置可以保存，并在以后恢复。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="337"/>
        <source>Button</source>
        <translation>按钮</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="283"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="339"/>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="237"/>
        <source>Help Menu - Manage Layout Settings</source>
        <translation>帮助菜单 - 管理布局设置</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="280"/>
        <source>Basic Hints</source>
        <translation>基本提示</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="283"/>
        <source>Key Sequence</source>
        <translation>按键顺序</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="284"/>
        <source>MouseWheel</source>
        <translation>鼠标滚轮</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="284"/>
        <source>Scrolls unpinned Graphs</source>
        <translation>滚动未固定的图表</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="285"/>
        <source>Ctrl + MouseWheel</source>
        <translation>Ctrl + 鼠标滚轮</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="285"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="288"/>
        <source>Zooms Time Selection</source>
        <translation>缩放时间选择</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="286"/>
        <source>LeftMouse dragDrop</source>
        <translation>左键拖放</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="286"/>
        <source>Defines Time Selection</source>
        <translation>定义时间选择</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="287"/>
        <source>RightMouse dragDrop</source>
        <translation>右键拖放</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="287"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="289"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="291"/>
        <source>Moves Time Selection</source>
        <translation>移动时间选择</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="288"/>
        <source>Ctrl + (right/left)MouseClick</source>
        <translation>Ctrl + (右/左)键单击</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="289"/>
        <source> (right/left)MouseClick</source>
        <translation>(右/左)键单击</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="291"/>
        <source>(right/left) Arrow (Ctrl =&gt; faster)</source>
        <translation>(右/左)箭头 (Ctrl => 更快)</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="292"/>
        <source>Up/Down Arrow</source>
        <translation>上/下箭头</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="292"/>
        <source>Scrolls graphs</source>
        <translation>滚动图表</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="293"/>
        <source>Up/Down Arrow+Focus</source>
        <translation>上/下箭头+焦点</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="293"/>
        <source>Zooms graphs</source>
        <translation>缩放图表</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="296"/>
        <source>Graph Layout Hints</source>
        <translation>图表布局提示</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="299"/>
        <source>Double Click Graph Title</source>
        <translation>双击图表标题</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="299"/>
        <source>Toggles Pinning</source>
        <translation>切换固定</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="300"/>
        <source>Daily:Double Click Y-axis label</source>
        <translation>每日：双击Y轴标签</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="300"/>
        <source>Toggle Time Selection Auto Zoom</source>
        <translation>切换时间选择自动缩放</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="301"/>
        <source>DragDrop Graph Title</source>
        <translation>拖放图表标题</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="301"/>
        <source>Reorders Graph layout</source>
        <translation>重新排序图表布局</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="302"/>
        <source>DragDrop graph’s bottom line</source>
        <translation>拖放图表的底线</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="302"/>
        <source>Changes Size of Graphs</source>
        <translation>更改图表大小</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="303"/>
        <source>Layout Button (next to Graph Button)</source>
        <translation>布局按钮（图表按钮旁）</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="303"/>
        <source>Save / Restore Graph Layouts</source>
        <translation>保存/恢复图表布局</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="306"/>
        <source>Daily Graph Hints</source>
        <translation>每日图表提示</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="309"/>
        <source>Click on date</source>
        <translation>点击日期</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="309"/>
        <source>Toggle Calendar on/off</source>
        <translation>切换日历开/关</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="310"/>
        <source>Detailed: Click on colored event</source>
        <translation>详细：点击彩色事件</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="310"/>
        <source>Jump to event tab with event opened</source>
        <translation>跳转到打开事件的事件标签</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="311"/>
        <source>Detailed: Click on a session (at bottom)</source>
        <translation>详细：点击一个会话（底部）</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="311"/>
        <source>Toggle session disable / enable session</source>
        <translation>切换会话禁用/启用会话</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="312"/>
        <source>Event: Click on an event</source>
        <translation>事件：点击一个事件</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="312"/>
        <source>Time Selection 3 min before event 20 sec after</source>
        <translation>事件前 3 分钟选择时间，事件后 20 秒</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="313"/>
        <source>Bookmark</source>
        <translation>书签</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="313"/>
        <source>Save current Time Selection</source>
        <translation>保存当前时间选择</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="314"/>
        <source>Search Tab</source>
        <translation>搜索标签</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="314"/>
        <source>Search data base</source>
        <translation>搜索数据库</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="317"/>
        <source>Miscellaneous Hints</source>
        <translation>各种提示</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="320"/>
        <source>OverView: Shift Click on a date</source>
        <translation>概览：按住Shift点击日期</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="320"/>
        <source>Jumps to date in the Daily Tab</source>
        <translation>跳转到每日选项卡中的日期</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="321"/>
        <source>Daily: Event (bottom left corner) </source>
        <translation>每日：事件（左下角）</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="321"/>
        <source>Select Events to view</source>
        <translation>选择查看的事件</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="322"/>
        <source>Graph / Chart (bottom right corner)</source>
        <translation>图表（右下角）</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="322"/>
        <source>Selects graphs to view</source>
        <translation>选择查看的图表</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="343"/>
        <source>Creates a copy of the current Layout Settings.</source>
        <translation>创建当前布局设置的副本。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="345"/>
        <source>The default description is the current date.</source>
        <translation>默认描述为当前日期。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="347"/>
        <source>The description may be changed.</source>
        <translation>描述可以更改。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="349"/>
        <source>The Add button will be greyed out when maximum number is reached.</source>
        <translation>当达到最大数量时，添加按钮将变为灰色。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="352"/>
        <source>Other Buttons</source>
        <translation>其他按钮</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="354"/>
        <source>Greyed out when there are no selections</source>
        <translation>没有选择时按钮变灰</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="358"/>
        <source>Loads the Layout Settings from the selection. Stays Open</source>
        <translation>从选择中加载布局设置。保持打开状态</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="362"/>
        <source>Modify the description of the selection. Same as a double click.</source>
        <translation>修改选择的描述。相当于双击。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="876"/>
        <source>Graph Short-Cuts Help</source>
        <translation>图表快捷键帮助</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="366"/>
        <source>Saves the current Layout Settings to the selection.</source>
        <translation>将当前布局设置保存到选择中。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="368"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="374"/>
        <source>Prompts for confirmation.</source>
        <translation>提示确认。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="372"/>
        <source>Deletes the selecton.</source>
        <translation>删除选择。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="376"/>
        <source>Control</source>
        <translation>控制</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="378"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="380"/>
        <source>(Red circle with a white &quot;X&quot;.) Returns to OSCAR menu.</source>
        <translation>（红色圆圈内有一个白色的“X”）返回到OSCAR菜单。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="382"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="384"/>
        <source>Next to Exit icon. Only in Help Menu. Returns to Layout menu.</source>
        <translation>在退出图标旁边。仅在帮助菜单中。返回到布局菜单。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="386"/>
        <source>Escape Key</source>
        <translation>Esc键</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="388"/>
        <source>Exit the Help or Layout menu.</source>
        <translation>退出帮助或布局菜单。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="390"/>
        <source>Layout Settings</source>
        <translation>布局设置</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="392"/>
        <source>* Name</source>
        <translation>* 名称</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="394"/>
        <source>* Pinning</source>
        <translation>* 置顶</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="396"/>
        <source>* Plots Enabled</source>
        <translation>* 启用图表</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="398"/>
        <source>* Height</source>
        <translation>* 高度</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="400"/>
        <source>* Order</source>
        <translation>* 顺序</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="402"/>
        <source>* Event Flags</source>
        <translation>* 事件标记</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="404"/>
        <source>* Dotted Lines</source>
        <translation>* 虚线</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="406"/>
        <source>* Height Options</source>
        <translation>* 高度选项</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="408"/>
        <source>General Information</source>
        <translation>一般信息</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="410"/>
        <source>Maximum description size = 80 characters.	</source>
        <translation>最大描述长度 = 80 字符。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="412"/>
        <source>Maximum Saved Layout Settings = 30.	</source>
        <translation>最大保存布局设置数量 = 30。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="414"/>
        <source>Saved Layout Settings can be accessed by all profiles.</source>
        <translation>所有配置文件都可以访问保存的布局设置。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="416"/>
        <source>Layout Settings only control the layout of a graph or chart.</source>
        <translation>布局设置仅控制图表的布局。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="418"/>
        <source>They do not contain any other data.</source>
        <translation>它们不包含任何其他数据。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="420"/>
        <source>They do not control if a graph is displayed or not.</source>
        <translation>它们不控制图表是否显示。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="422"/>
        <source>Layout Settings for daily and overview are managed independantly.</source>
        <translation>每日和概览的布局设置独立管理。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="563"/>
        <source>Maximum number of Items exceeded.</source>
        <translation>超过项目的最大数量。</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="569"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="578"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="587"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="616"/>
        <source>No Item Selected</source>
        <translation>未选择任何项目</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="570"/>
        <source>Ok to Update?</source>
        <translation>确定更新？</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="617"/>
        <source>Ok To Delete?</source>
        <translation>确定删除？</translation>
    </message>
</context>
<context>
    <name>SessionBar</name>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="290"/>
        <source>No Sessions Present</source>
        <translation>没有会话</translation>
    </message>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="247"/>
        <source>%1h %2m</source>
        <translation>%1小时 %2分钟</translation>
    </message>
</context>
<context>
    <name>SleepStyleLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="213"/>
        <source>Import Error</source>
        <translation>导入错误</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="214"/>
        <source>This device Record cannot be imported in this profile.</source>
        <translation>此设备记录无法导入到此配置文件中。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="214"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>当天的记录与已存在的内容重叠。</translation>
    </message>
</context>
<context>
    <name>Statistics</name>
    <message>
        <location filename="../oscar/statistics.cpp" line="1218"/>
        <source>Days</source>
        <translation>天数</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="777"/>
        <source>Oximeter Statistics</source>
        <translation>血氧仪统计</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="709"/>
        <location filename="../oscar/statistics.cpp" line="1671"/>
        <source>CPAP Usage</source>
        <translation>CPAP使用情况</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="781"/>
        <source>Blood Oxygen Saturation</source>
        <translation>血氧饱和度</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="804"/>
        <source>% of time in %1</source>
        <translation>% 的时间在 %1 中</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1423"/>
        <source>Last 30 Days</source>
        <translation>过去三十天</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="803"/>
        <source>%1 Index</source>
        <translation>%1 指数</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="802"/>
        <source>Max %1</source>
        <translation>最大 %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="797"/>
        <source>%1 Median</source>
        <translation>%1 中值</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="801"/>
        <source>Min %1</source>
        <translation>最小 %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1421"/>
        <source>Most Recent</source>
        <translation>最近</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1224"/>
        <source>Pressure Settings</source>
        <translation>压力设置</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="752"/>
        <source>Pressure Statistics</source>
        <translation>压力统计</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1424"/>
        <source>Last 6 Months</source>
        <translation>过去六个月</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="798"/>
        <location filename="../oscar/statistics.cpp" line="799"/>
        <source>Average %1</source>
        <translation>平均 %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1156"/>
        <source>Last Use</source>
        <translation>最后一次使用</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="786"/>
        <source>Pulse Rate</source>
        <translation>脉搏率</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1155"/>
        <source>First Use</source>
        <translation>首次使用</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1422"/>
        <source>Last Week</source>
        <translation>上周</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1426"/>
        <source>Last Year</source>
        <translation>去年</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1469"/>
        <source>Details</source>
        <translation>详情</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1392"/>
        <source>Last Session</source>
        <translation>上一个会话</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="694"/>
        <source>CPAP Statistics</source>
        <translation>CPAP统计</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="747"/>
        <source>Leak Statistics</source>
        <translation>漏气统计</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="721"/>
        <source>Average Hours per Night</source>
        <translation>平均每晚的小时数</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="805"/>
        <source>% of time above %1 threshold</source>
        <translation>% 的时间高于 %1 阈值</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="806"/>
        <source>% of time below %1 threshold</source>
        <translation>% 的时间低于 %1 阈值</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1222"/>
        <source>Pressure Relief</source>
        <translation>压力释放</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="724"/>
        <source>Therapy Efficacy</source>
        <translation>疗效</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="711"/>
        <source>Total Days</source>
        <translation>总天数</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="712"/>
        <source>Used Days</source>
        <translation>使用天数</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="713"/>
        <location filename="../oscar/statistics.cpp" line="718"/>
        <source>Used Days %1%2 hrs/day</source>
        <translation>使用天数 %1%2 小时/天</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="715"/>
        <source>Percent Total Days %1%2 hrs/day</source>
        <translation>总天数的百分比 %1%2 小时/天</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="716"/>
        <source>Percent Used Days %1%2 hrs/day</source>
        <translation>使用天数的百分比 %1%2 小时/天</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="719"/>
        <source>Days Not Used</source>
        <translation>未使用天数</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="722"/>
        <source>Median Hours per Night</source>
        <translation>每晚的中位数小时</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="731"/>
        <source>AHI Median</source>
        <translation>AHI 中位数</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="828"/>
        <source>Name: %1, %2</source>
        <translation>名字: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="830"/>
        <source>DOB: %1</source>
        <translation>出生日期: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="833"/>
        <source>Phone: %1</source>
        <translation>电话号码: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="836"/>
        <source>Email: %1</source>
        <translation>电子邮箱: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="839"/>
        <source>Address:</source>
        <translation>地址:</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1149"/>
        <source>Device Information</source>
        <translation>设备信息</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1210"/>
        <source>Changes to Device Settings</source>
        <translation>设备设置的更改</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1428"/>
        <source>Everything</source>
        <translation>所有内容</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1484"/>
        <source>Database has No %1 data available.</source>
        <translation>数据库中没有可用的%1数据。</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1487"/>
        <source>Database has %1 day of %2 Data on %3</source>
        <translation>数据库中有%1天的%2数据在%3。</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1493"/>
        <source>Database has %1 days of %2 Data, between %3 and %4</source>
        <translation>数据库中有%1天的%2数据，日期在%3和%4之间。</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1674"/>
        <source>Total Days: %1</source>
        <translation>总天数: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1675"/>
        <source>Days Not Used: %1</source>
        <translation>未使用的天数: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1677"/>
        <source>Days Used: %1</source>
        <translation>使用的天数: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1703"/>
        <source>Days AHI of 5 or greater: %1</source>
            <translation>AHI大于5的天数: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1710"/>
        <source>Best AHI</source>
        <translation>最佳AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1714"/>
        <location filename="../oscar/statistics.cpp" line="1726"/>
        <source>Date: %1 AHI: %2</source>
        <translation>日期: %1 AHI: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1720"/>
        <source>Worst AHI</source>
        <translation>最差AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1757"/>
        <source>Best Flow Limitation</source>
        <translation>最佳流量限制</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1761"/>
        <location filename="../oscar/statistics.cpp" line="1774"/>
        <source>Date: %1 FL: %2</source>
            <translation>日期: %1 FL: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1767"/>
        <source>Worst Flow Limitation</source>
        <translation>最差流量限制</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1779"/>
        <source>No Flow Limitation on record</source>
        <translation>无流量限制记录</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1800"/>
        <source>Worst Large Leaks</source>
        <translation>最严重的大量漏气</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1808"/>
        <source>Date: %1 Leak: %2%</source>
        <translation>日期: %1 漏气: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1814"/>
        <source>No Large Leaks on record</source>
        <translation>无大量漏气记录</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1837"/>
        <source>Worst CSR</source>
        <translation>最差的潮式呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1845"/>
        <source>Date: %1 CSR: %2%</source>
        <translation>日期: %1 CSR: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1850"/>
        <source>No CSR on record</source>
        <translation>无潮式呼吸记录</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1867"/>
        <source>Worst PB</source>
        <translation>最差周期性呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1875"/>
        <source>Date: %1 PB: %2%</source>
        <translation>日期: %1 PB: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1880"/>
        <source>No PB on record</source>
        <translation>无周期性呼吸数据</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1888"/>
        <source>Want more information?</source>
        <translation>需要更多信息？</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1889"/>
        <source>OSCAR needs all summary data loaded to calculate best/worst data for individual days.</source>
        <translation>OSCAR需要加载所有摘要数据来计算单天的最佳/最差数据。</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1890"/>
        <source>Please enable Pre-Load Summaries checkbox in preferences to make sure this data is available.</source>
        <translation>请在首选项中启用预加载摘要复选框以确保这些数据可用。</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1921"/>
        <source>Worst Device Setting</source>
        <translation>最差设备设置</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1912"/>
        <location filename="../oscar/statistics.cpp" line="1924"/>
        <source>Date: %1 - %2</source>
        <translation>日期: %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="931"/>
        <source>OSCAR is free open-source CPAP report software</source>
        <translation>OSCAR是免费开源的CPAP报告软件</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1293"/>
        <source>Oscar has no data to report :(</source>
        <translation>Oscar没有可报告的数据 :(</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1291"/>
        <source>No data found?!?</source>
        <translation>未找到数据?!?</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1678"/>
        <source>Days %1 %2 %3%</source>
        <translation>天数 %1 %2 %3%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1679"/>
        <location filename="../oscar/statistics.cpp" line="1680"/>
        <source>Days %1 %2 Hours: %3</source>
        <translation>天数 %1 %2 小时: %3</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1909"/>
        <source>Best Device Setting</source>
        <translation>最佳设备设置</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1915"/>
        <location filename="../oscar/statistics.cpp" line="1927"/>
        <source>AHI: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1916"/>
        <location filename="../oscar/statistics.cpp" line="1928"/>
        <source>Total Hours: %1</source>
        <translation>总小时数: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="928"/>
        <source>This report was prepared on %1 by OSCAR %2</source>
        <translation>此报告由OSCAR %2于%1准备</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../oscar/welcome.ui" line="142"/>
        <source>What would you like to do?</source>
        <translation>你想做什么?</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="185"/>
        <source>CPAP Importer</source>
        <translation>CPAP导入器</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="223"/>
        <source>Oximetry Wizard</source>
        <translation>血氧测量向导</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="261"/>
        <source>Daily View</source>
        <translation>每日视图</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="299"/>
        <source>Overview</source>
        <translation>总览</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="337"/>
        <source>Statistics</source>
        <translation>统计</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="580"/>
        <source>&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;ResMed S9 SDCards need to be locked &lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;before inserting into your computer.&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;Some operating systems write index files to the card without asking, which can render your card unreadable by your cpap device.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;span style=&quot; font-weight:600;&quot;&gt;警告: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;ResMed S9 SD卡在插入电脑前需要锁定 &lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;。&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;一些操作系统会在未询问的情况下向卡写入索引文件，这可能会导致您的CPAP设备无法读取您的卡。&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="166"/>
        <source>Note that some preferences are forced when a ResMed device is detected</source>
        <translation>请注意，检测到ResMed设备时某些首选项将被强制应用</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="167"/>
        <source>First import can take a few minutes.</source>
        <translation>第一次导入可能需要几分钟。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="179"/>
        <source>The last time you used your %1...</source>
        <translation>您上次使用您的%1...</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="183"/>
        <source>last night</source>
        <translation>昨晚</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="185"/>
        <source>today</source>
        <translation>今天</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="186"/>
        <source>%2 days ago</source>
        <translation>%2天前</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="188"/>
        <source>was %1 (on %2)</source>
        <translation>是 %1 (在 %2)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="196"/>
        <source>%1 hours, %2 minutes and %3 seconds</source>
        <translation>%1小时，%2分钟和%3秒</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="199"/>
        <source>Your device was on for %1.</source>
        <translation>您的设备开启了%1。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="264"/>
        <source>Your CPAP device used a constant %1 %2 of air</source>
        <translation>您的CPAP设备使用了恒定的%1 %2空气</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="277"/>
        <source>Your device used a constant %1-%2 %3 of air.</source>
        <translation>您的设备使用了恒定的%1-%2 %3空气。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="284"/>
        <source>Your device was under %1-%2 %3 for %4% of the time.</source>
        <translation>您的设备在%1-%2 %3的时间内占用了%4%。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="200"/>
        <source>&lt;font color = red&gt;You only had the mask on for %1.&lt;/font&gt;</source>
        <translation>&lt;font color = red&gt;您仅佩戴了面罩%1。&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="213"/>
        <source>under</source>
        <translation>低于</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="214"/>
        <source>over</source>
        <translation>高于</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="215"/>
        <source>reasonably close to</source>
        <translation>合理接近</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="216"/>
        <source>equal to</source>
        <translation>等于</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="230"/>
        <source>You had an AHI of %1, which is %2 your %3 day average of %4.</source>
        <translation>您的AHI为%1，这%2您的%3天平均值为%4。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="309"/>
        <source>Your EEPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>您的EEPAP压力在%3%的时间内低于%1 %2。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="330"/>
        <source>Your average leaks were %1 %2, which is %3 your %4 day average of %5.</source>
        <translation>您的平均漏气为%1 %2，这%3您的%4天平均值为%5。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="336"/>
        <source>No CPAP data has been imported yet.</source>
        <translation>尚未导入任何CPAP数据。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="164"/>
        <source>It would be a good idea to check File-&gt;Preferences first,</source>
        <translation>建议先检查文件-&gt;首选项,</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="165"/>
        <source>as there are some options that affect import.</source>
        <translation>因为有些选项会影响导入。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="127"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>欢迎使用开源CPAP分析报告器</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="269"/>
        <source>Your pressure was under %1 %2 for %3% of the time.</source>
        <translation>您的压力在%3%的时间内低于%1 %2。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="292"/>
        <source>Your EPAP pressure fixed at %1 %2.</source>
        <translation>您的EPAP压力固定在%1 %2。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="295"/>
        <location filename="../oscar/welcome.cpp" line="304"/>
        <location filename="../oscar/welcome.cpp" line="310"/>
        <source>Your IPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>您的IPAP压力在%3%的时间内低于%1 %2。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="303"/>
        <source>Your EPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>您的EPAP压力在%3%的时间内低于%1 %2。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="184"/>
        <source>1 day ago</source>
        <translation>1天前</translation>
    </message>
</context>
<context>
    <name>gGraph</name>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="651"/>
        <source>Double click Y-axis: Return to AUTO-FIT Scaling</source>
        <translation>双击Y轴: 返回自动缩放</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="653"/>
        <source>Double click Y-axis: Return to DEFAULT Scaling</source>
        <translation>双击Y轴: 返回默认缩放</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="655"/>
        <source>Double click Y-axis: Return to OVERRIDE Scaling</source>
        <translation>双击Y轴: 返回覆盖缩放</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="658"/>
        <source>Double click Y-axis: For Dynamic Scaling</source>
        <translation>双击Y轴: 动态缩放</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="662"/>
        <source>Double click Y-axis: Select DEFAULT Scaling</source>
        <translation>双击Y轴: 选择默认缩放</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="664"/>
        <source>Double click Y-axis: Select AUTO-FIT Scaling</source>
        <translation>双击Y轴: 选择自动缩放</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="923"/>
        <source>%1 days</source>
        <translation>%1天</translation>
    </message>
</context>
<context>
    <name>gGraphView</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="557"/>
        <source>100% zoom level</source>
        <translation>100% 缩放级别</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="563"/>
        <source>Reset Graph Layout</source>
        <translation>重置图表布局</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="568"/>
        <source>Plots</source>
        <translation>图块</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="573"/>
        <source>CPAP Overlays</source>
        <translation>CPAP覆盖</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="576"/>
        <source>Oximeter Overlays</source>
        <translation>血氧仪覆盖</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="579"/>
        <source>Dotted Lines</source>
        <translation>虚线</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="564"/>
        <source>Resets all graphs to a uniform height and default order.</source>
        <translation>将所有图表重置为统一高度和默认顺序。</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="567"/>
        <source>Y-Axis</source>
        <translation>Y轴</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2266"/>
        <source>Remove Clone</source>
        <translation>删除副本</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2270"/>
        <source>Clone %1 Graph</source>
        <translation>复制%1图表</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1967"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2020"/>
        <source>Double click title to pin / unpin
Click and drag to reorder graphs</source>
        <translation>双击标题以固定/取消固定
点击并拖动以重新排列图表</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="559"/>
        <source>Restore X-axis zoom to 100% to view entire selected period.</source>
        <translation>将X轴缩放恢复到100%以查看整个选择的时间段。</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="561"/>
        <source>Restore X-axis zoom to 100% to view entire day&apos;s data.</source>
        <translation>将X轴缩放恢复到100%以查看整天的数据。</translation>
    </message>
</context>
</TS>
